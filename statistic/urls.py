from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index, name="statistic"),
    path('me/', views.my_statistic, name="my_statistic"),
    path('smm/', views.smm_statistic, name="smm_statistic"),
]
