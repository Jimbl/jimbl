from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class month_egegament_rate(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    year = models.IntegerField(blank=True, null=True)
    month = models.IntegerField(blank=True, null=True)
    views = models.CharField(max_length=200, blank=True, null=True)
    readers = models.CharField(max_length=200, blank=True, null=True)

class month_activites_rate(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    year = models.IntegerField(blank=True, null=True)
    month = models.IntegerField(blank=True, null=True)
    views = models.CharField(max_length=200, blank=True, null=True)
    likes = models.CharField(max_length=200, blank=True, null=True)
    comments = models.CharField(max_length=200, blank=True, null=True)
    reposts = models.CharField(max_length=200, blank=True, null=True)
    rating = models.CharField(max_length=200, blank=True, null=True)
