from django.shortcuts import render
from App.models import Post, Comment, Profile, Blog, User, Repost, Select, Report
from .models import month_egegament_rate, month_activites_rate
from App.views import update_month_egagement_rate, update_month_activites_rate
from django.http import HttpResponse
from django.template.context import Context
from django.template.base import Template
from django.template import loader
from django.contrib.auth.decorators import login_required
import datetime

@login_required(login_url='/accounts/login/')
def smm_statistic(request):
    user = request.user
    average_rating = 0
    month_post_cnt = 0
    posts = Post.objects.filter(user = user).all()
    for post in posts:
        average_rating += post.rate
        if int(post.date.strftime('%Y')) == int(datetime.date.today().strftime('%Y')) and int(post.date.strftime('%m')) == int(datetime.date.today().strftime('%m')):
            month_post_cnt += 1

    try:
        average_rating = average_rating / posts.count()
    except ZeroDivisionError:
        average_rating = 0
    frequency = month_post_cnt / int(datetime.date.today().strftime('%d'))

    total_score = 0

    if user.profile.status_profile == 0:
        total_score += 15

    try:
        if user.profile.image_header.url != '':
            total_score += 10
    except Exception:
        pass

    try:
        if user.profile.image_profile.url != '':
            total_score += 10
    except Exception:
        pass

    if user.profile.discription_profile != '':
        total_score += 10

    if user.profile.accept_send_messages == 0:
        total_score += 10
    elif user.profile.accept_send_messages == 1:
        total_score += 5

    if user.profile.accept_comment_posts == 0:
        total_score += 10
    elif user.profile.accept_comment_posts == 1:
        total_score += 5

    if user.profile.vk != '' or user.profile.facebook != '' or user.profile.twitter != '' or user.profile.instagram != '':
        total_score += 5

    if average_rating > 20:
        total_score += 15

    if round(frequency, 1) >= 0.3:
        total_score += 15

    context = {
        'arp': int(average_rating),
        'frequency': round(frequency, 1),
        'total_score': total_score,
    }
    return render(request, 'statistic/about.html', context)

@login_required(login_url='/accounts/login/')
def my_statistic(request):
    now_day = int(datetime.date.today().strftime('%d'))
    range_month = range(1, now_day)
    month_likes = []
    now_month = int(datetime.date.today().strftime('%m'))
    now_year = int(datetime.date.today().strftime('%Y'))
    posts = Post.objects.filter(user = request.user).all()

    update_month_egagement_rate(request.user) # инициализация и обновление статистики
    update_month_activites_rate(request.user)

    month_egegament_rating = month_egegament_rate.objects.get(user = request.user, month = now_month, year = int(datetime.date.today().strftime('%Y')))
    month_activites_rating = month_activites_rate.objects.get(user = request.user, month = now_month, year = int(datetime.date.today().strftime('%Y')))

    try:
        month_erv = int(sum(map(int, month_egegament_rating.readers.split(','))) / sum(map(int, month_egegament_rating.views.split(',')))*100)
    except ZeroDivisionError:
        month_erv = 0

    context = {
        'blogs_cnt': Blog.objects.filter(user = request.user).count(),
        'posts_cnt': Post.objects.filter(user = request.user).count(),
        'now_date': datetime.date.today(),
        'month_egegament_rating_size': range(1, int(datetime.date.today().strftime('%d'))+1),
        'month_egegament_rating_views': month_egegament_rating.views.split(','),
        'month_egegament_rating_readers': month_egegament_rating.readers.split(','),
        'month_erv': month_erv,
        'month_activites_likes': month_activites_rating.likes.split(','),
        'month_activites_comments': month_activites_rating.comments.split(','),
        'month_activites_reposts': month_activites_rating.reposts.split(','),
        'month_activites_epr': month_activites_rating.rating.split(','),
    }
    return render(request, 'statistic/my_statistic.html', context)

@login_required(login_url='/accounts/login/')
def index(request):

    best_bloggers = Profile.objects.all().order_by('-month_rate')

    context = {
        'now_date': datetime.date.today(),
        'best_blogers': best_bloggers,
    }
    return render(request, 'statistic/index.html', context)
