$.ajaxSetup({
 beforeSend: function(xhr, settings) {
     function getCookie(name) {
         var cookieValue = null;
         if (document.cookie && document.cookie != '') {
             var cookies = document.cookie.split(';');
             for (var i = 0; i < cookies.length; i++) {
                 var cookie = jQuery.trim(cookies[i]);
                 // Does this cookie string begin with the name we want?
             if (cookie.substring(0, name.length + 1) == (name + '=')) {
                 cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                 break;
             }
         }
     }
     return cookieValue;
     }
     if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
         // Only send the token to relative URLs i.e. locally.
         xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
     }
 }
});

function pre_edit_message(){
  $('#write-input-desktop').val($('.copy-message').val())
  $('#write-input-desktop').attr('data-edit', 'true');
  $('#write-input-desktop').attr('data-edit-id', sessionStorage.getItem('active_message_id'))
}

function unset_message(){
  sessionStorage.removeItem('active_message_id')
  $('.message').removeClass('set')
  $('#main-chat-top').removeClass('hidden')
  $('#tools-chat-top').addClass('hidden')

  $('.tools.fa-trash-alt').attr('onclick', '')
  $('.tools.fa-star').attr('onclick', '')
  $('.copy-message').attr('value', '')
  $('.copy-message').attr('id', '')
  $('.clipboard-trigger').attr('data-clipboard-target', '')

  $('#write-input-desktop').val('')
  $('#write-input-desktop').attr('data-edit', '');
  $('#write-input-desktop').attr('data-edit-id', '')
}

function tools_message(message, message_item, chat_profile, content){
  chat_profile = "'"+chat_profile+"'"
  if ($(message_item).hasClass('set')) {
    disable_tools_message()
    sessionStorage.removeItem('active_message_id')
    $(message_item).removeClass('set');
    $('#main-chat-top').removeClass('hidden')
    $('#tools-chat-top').addClass('hidden')

    $('.tools.fa-trash-alt').attr('onclick', '')
    $('.tools.fa-star').attr('onclick', '')
    $('.copy-message').attr('value', '')
    $('.copy-message').attr('id', '')
    $('.clipboard-trigger').attr('data-clipboard-target', '')

  }
  else{
    active_tools_message()
    $('.tools.fa-star').toggleClass('fas far')
    $('.tools.fa-star').removeClass('set')
    $('.message').removeClass('set')
    sessionStorage.setItem('active_message_id', message)
    $(message_item).addClass('set');
    $('#main-chat-top').addClass('hidden')
    $('#tools-chat-top').removeClass('hidden')

    $('.tools.fa-trash-alt').attr('onclick', 'delete_message('+sessionStorage.getItem('active_message_id')+', '+chat_profile+')')
    $('.tools.fa-star').attr('onclick', 'add_message_to_favorite('+sessionStorage.getItem('active_message_id')+', '+chat_profile+', this)')
    $('.copy-message').attr('value', content)
    $('.copy-message').attr('id', 'copy_message_'+sessionStorage.getItem('active_message_id'))
    $('.clipboard-trigger').attr('data-clipboard-target', '#copy_message_'+sessionStorage.getItem('active_message_id'))

    if($(message_item).attr('data-important')=='true'){
      $('.tools.fa-star').removeClass('far')
      $('.tools.fa-star').addClass('fas')
      $('.tools.fa-star').addClass('set')
    }
  }

}

$('#search-desktop-input').on("input", function() {
  $('.search-load').removeClass('hidden')
  if(!$('#search-desktop-list').hasClass('show')){
    $('#search-desktop-list').collapse('show');
  }
  search = $(this).val();
  $.ajax({
     url : '/',
     type : 'POST',
     data : {
       is_search: '1',
       search: search,
     },
     success : function (response) {
      $('.search-load').addClass('hidden')
      $('#search-desktop-list').html(response)
      fix()
     },
 });
});

$('#search-important-input').on("input", function() {

  search = $(this).val();
  $.ajax({
     url : '/chat/important/',
     type : 'POST',
     data : {
       is_search: '1',
       search: search,
     },
     success : function (response) {
       $('#important-messages-desktop').html(response)
     },
 });
});

function render_messages(chat_profile){
  $.ajax({
     url : '/chat/' + chat_profile + '/',
     type : 'POST',
     data : {
       get_messages: '1'
     },
     success : function (response) {
      var prev_cnt_comment = $('table.message').length;
      $('#message-list').html(response);
      $('.message[id='+sessionStorage.getItem('active_message_id')+']').addClass('set');
      var now_cnt_comment = $('table.message').length;

      if($('#message-list').scrollTop() + $('#message-list').innerHeight() + $('#message-list > table.message:last-child').height() >= $('#message-list')[0].scrollHeight) {
        $('#message-list').scrollTop(1e10);
      }
      else{
        if(prev_cnt_comment != now_cnt_comment){
          $('#new_message_mobile').removeClass('hidden');
        }
      }
     },
 });
}

function render_messages_desktop(chat_profile){
  $.ajax({
     url : '/chat/' + chat_profile + '/',
     type : 'POST',
     data : {
       get_messages: '1',
       is_desktop: '1',
     },
     success : function (response) {
      var prev_cnt_comment = $('table.message').length;
      $('#message-list-desktop').html(response);
      $('.message[id='+sessionStorage.getItem('active_message_id')+']').addClass('set');
      var now_cnt_comment = $('table.message').length;
      $('#dropdown-message-'+sessionStorage.getItem('selected_message')).addClass('show');

      if($('body').scrollTop() + $('body').innerHeight() + $('#message-list-desktop > .btn-group:last-child').height() >= $('#message-list-desktop')[0].scrollHeight && prev_cnt_comment != now_cnt_comment) {
        $('body').scrollTop(1e10);
      }
      else{
        if(prev_cnt_comment != now_cnt_comment){
          $('#new_message_desktop').removeClass('hidden');
        }
      }
     },
 });
}


function active_tools_message(){
  $('#main-nav-mobile').addClass('hidden')
  $('#tools-message-mobile').removeClass('hidden')
}

function disable_tools_message(){
  $('#main-nav-mobile').removeClass('hidden')
  $('#tools-message-mobile').addClass('hidden')
}

function edit_message(){
  message = $('#write-input-desktop').val()
  if(message){
    $.ajax({
      url: '/chat/'+sessionStorage.getItem('chat_profile')+'/',
      type: 'post',
      data: {
        edit_message: '1',
        message_id: $('#write-input-desktop').attr('data-edit-id'),
        new_data: message
      },
      success: function(response){
        render_messages_desktop(sessionStorage.getItem('chat_profile'))
      }
    })
  }
}

$('#write-input-desktop').keypress(function (e) {
  if (e.which == 13) {
    if($('#write-input-desktop').attr('data-edit') === 'true'){
      edit_message()
      return false;
    }
    else{
      write_message_desktop($('input[name=chat_profile_name]').val());
      return false;
    }
  }
});

$('#search-input').on("change paste keyup", function() {
   if(!$('#collapseExample').hasClass('show')){
     $('#collapseExample').collapse('show');
   }
   search = $(this).val();
   $.ajax({
      url : '/chat/',
      type : 'POST',
      data : {
        is_search: '1',
        search: search,
      },
      success : function (response) {
       $('.search-main').html(response);
       fix()
      },
  });
});

$('#search-chat-desktop-input').on("change paste keyup", function() {
   var search = $(this).val();
   $.ajax({
      url : '/chat/',
      type : 'POST',
      data : {
        is_search: '1',
        search: search,
      },
      success : function (response) {
       $('#chat-list-desktop').html(response);
       fix()
      },
  });
});

$('#search-input-chat').on("change paste keyup", function() {
   var search = $(this).val();
   $.ajax({
      url : '/chat/',
      type : 'POST',
      data : {
        is_search: '1',
        is_mobile: '1',
        search: search,
      },
      success : function (response) {
       $('#chat-list-mobile').html(response);
       fix()
      },
  });
});

function write_message(chat_profile){
  var message = $('#write-input').val();
  if(message === '') return;
  $.ajax({
     url : '/chat/' + chat_profile + '/',
     type : 'POST',
     data : {
       write_message: '1',
       message: message,
     },
     success : function (response) {
      $('#message-list').html(response);
      $('#write-input').val('');
      $('#message-list').scrollTop(1e10);
     },
 });
}

function add_emoji(emoji){
  var text = $('#write-input-desktop').val()
  emoji = emoji.replace(/\s+/g, '');
  text += emoji
  $('#write-input-desktop').val(text)
}

function write_message_desktop(chat_profile){
  var message = $('#write-input-desktop').val();
  if(message === '') return;
  $.ajax({
     url : '/chat/' + chat_profile + '/',
     type : 'POST',
     data : {
       write_message: '1',
       message: message,
       is_desktop: '1',
     },
     success : function (response) {
      $('#message-list-desktop').html(response);
      $('#write-input-desktop').val('');
      $('body').scrollTop(1e10);
     },
 });
}

$(document).on('click', function(){
  if($(this) != $('.dropdown-menu')){
    sessionStorage.setItem('selected_message', '')
    $('.dropdown-menu').removeClass('show');
  }
});

function delete_message(id_message, chat_profile){
  if ($(window).width() < 1200){
    is_desktop = false;
  }
  else{
    is_desktop = true
  }
  $.ajax({
     url : '/chat/' + chat_profile + '/',
     type : 'POST',
     data : {
       delete_message: '1',
       id_message: id_message,
       is_desktop: is_desktop,
     },
     success : function (data) {
      $('#message-list-desktop').html(data);
     },
 });
}

function add_message_to_favorite(id_message, chat_profile, item){
  if ($(window).width() < 1200){
    is_desktop = false;
  }
  else{
    is_desktop = true
  }
  if($(item).hasClass('set')){
    $(item).removeClass('set')
    $(item).removeClass('fas')
    $(item).addClass('far')
  }
  else{
    $(item).addClass('set')
    $(item).addClass('fas')
    $(item).removeClass('far')
  }
  $.ajax({
     url : '/chat/' + chat_profile + '/',
     type : 'POST',
     data : {
       add_message_to_favorite: '1',
       id_message: id_message,
       is_desktop: is_desktop,
     },
 });
}

$('#message-list').scroll(function(){
  if($('#message-list').scrollTop() + $('#message-list').innerHeight() >= $('#message-list')[0].scrollHeight){
    $('#new_message_mobile').addClass('hidden');
  }
});

$(document).scroll(function(){
  if($('#message-list-desktop').height() - $(document).scrollTop() === 806){
    $('#new_message_desktop').addClass('hidden');
  }
});

$('.timeout').on('show.bs.modal', function (event) {
  setTimeout(function(){
    $('.modal-backdrop.show').css('opacity', '0');
    $('body').removeClass('modal-open')
    $('body').css('padding-right', '0')
    $('.navbar').css('padding-right', '0')
  }, 10)
})

$('.modal.timeout').on('show.bs.modal', function (event) {
  setTimeout(function(){$('.modal.timeout').modal('hide')}, 1500)
})

function clear_chat(chat){
  $.ajax({
     url : '/chat/'+chat+'/',
     type : 'POST',
     data : {
       'clear_chat': 1,
     },
     success : function (response) {
      $('#message-list').html(response);
      $('#clear_chat_modal').modal('hide');
     },
 });
}

function leave_chat(chat){
  $.ajax({
     url : '/chat/'+chat+'/',
     type : 'POST',
     data : {
       'leave_chat': 1,
     },
     success : function (response) {
       $('#leave_chat_modal').modal('hide');
     },
 });
}

function delete_chat(event, chat){
  event.preventDefault();

  $.ajax({
     url : '/chat/'+chat+'/',
     type : 'POST',
     data : {
       'delete_chat': 1,
     },
     success : function (response) {

     },
 });
}

$(function () {
    $(".dialogs-sticky").sticky({
        topSpacing: 60,
        zIndex: 2
    });
});

function fix(){
  $('#chat-list-desktop').css('min-height', $(document).height() - 150 + 'px');
  $('#message-list-desktop').css('min-height', $(window).height() - 150 + 'px');
  $('#important-messages-desktop').css('min-height', $(window).height() - 150 + 'px');
  $('#message-list-desktop-muted').css('min-height', $(window).height() - 150 + 'px');
  $('.search-message').css('max-width', $('#chat-list-desktop').width() - 100)
  if(url === 'important'){
    $('[id=message-content]').css('max-width', $('#important-messages-desktop').width() - 80 + 'px');
  }
  else{
    $('[id=message-content]').css('max-width', $('#chat-list-desktop').width() - 80 + 'px');
  }
}

$(document).ready(function(){
  url = document.location.href.split('/')[4]
  sessionStorage.removeItem('active_message_id')
  if($(document).width() < 1200){
    $('#message-list').css('height', $(window).height() - $('#main-nav-mobile').height() - 45 - $('#bottom-menu').height() + 'px');
    $('p#content').css('max-width', $(window).width() - 75 - 20 + 'px');
    if(url != '' && url != 'important'){
      setInterval("render_messages('"+sessionStorage.getItem('chat_profile')+"')", 2000);
    }
    $('#message-list').scrollTop(1e10);
  }
  else{
      $.ajax({
         url : '/chat/',
         type : 'POST',
         data : {
           'load_chats': 1,
         },
         success : function (response) {
          $('#chat-list-desktop').html(response);

         },
     });
    fix()
    $('body').scrollTop(1e10);
    if(url != '' && url != 'important'){
      setInterval("render_messages_desktop('"+sessionStorage.getItem('chat_profile')+"')", 2000)
    }
    $('.fixed-chat').css('width', $('#chat-main-body').width())
    $('#dropdown-message-'+sessionStorage.getItem('selected_message')).addClass('show');
    $('#left_menu_sticky').sticky({topSpacing: 50});
    $('#right_menu_sticky').sticky({topSpacing: $('#main-nav').height() + 16});
    $('#chat-nav').sticky({topSpacing: $('#main-nav').height() + 1});
    var h = $('#chat-main-body').height()
    $('#chat-main-body').css('min-height', h + 38)
  }
});
