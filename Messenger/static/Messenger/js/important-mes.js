$(document).ready(function(){
  $.ajax({
     url : '/chat/important/',
     type : 'POST',
     data : {
       'load_messages': 1,
       'important_message_cnt': $('.important-message').length,
     },
     success : function (response) {
      $('#important-messages-desktop').html(response);
      $('load-important-wrapp').addClass('hidden')
     },
 });
})
