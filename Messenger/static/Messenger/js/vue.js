$.ajaxSetup({
 beforeSend: function(xhr, settings) {
     function getCookie(name) {
         var cookieValue = null;
         if (document.cookie && document.cookie != '') {
             var cookies = document.cookie.split(';');
             for (var i = 0; i < cookies.length; i++) {
                 var cookie = jQuery.trim(cookies[i]);
                 // Does this cookie string begin with the name we want?
             if (cookie.substring(0, name.length + 1) == (name + '=')) {
                 cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                 break;
             }
         }
     }
     return cookieValue;
     }
     if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
         // Only send the token to relative URLs i.e. locally.
         xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
     }
 }
});

var search_input = new Vue({
  el: '#vue-chat-search',
  methods: {
    searchf: function () {
      if($(window).width() > 1200){
        var search = $('#vue-chat-search-input').val()
        $.ajax({
           url : '/chat/'+sessionStorage.getItem('chat_profile')+'/',
           type : 'POST',
           data : {
             search_chats: '1',
             search: search,
           },
           success : function (response) {
            $('#chat-search').html(response)
           },
       });
     }
    }
  }
})
