from django.shortcuts import render, redirect
from App.models import Profile, User, Emoji
from django.template import loader
from django.http import HttpResponse
from .models import Chat, Message
from django.contrib.auth.decorators import login_required
from itertools import groupby

# Create your views here.

@login_required(login_url='/accounts/login/')
def important(request):
    user = request.user

    if request.is_ajax():
        if request.POST.get('load_messages') == '1':
            important_messages = user.profile.important_messages.all()

            template = loader.get_template('Messenger/important-message/include/important_template.html')

            context = {
                'important_messages': important_messages[: int(request.POST.get('important_message_cnt')) + 50]
            }

            resp = template.render(context, request)
            return HttpResponse(resp)

        if request.POST.get('is_search') == '1':
            search = request.POST.get('search')
            user = request.user
            search_important = []

            important_messages = user.profile.important_messages.all()

            for message in important_messages:
                if str(search) in message.content:
                    search_important.append(message)

            template = loader.get_template('Messenger/important-message/include/important_template.html')

            context = {
                'important_messages': search_important,
                'search': '1'
            }

            resp = template.render(context, request)
            return HttpResponse(resp)

    important_messages = user.profile.important_messages.all()

    context = {
        'important_messages': important_messages[:50]
    }
    return render(request, 'Messenger/important-message/index.html', context)

@login_required(login_url='/accounts/login/')
def index(request):
    user = request.user

    if request.is_ajax():

        if(request.POST.get('load_chats') == '1'):
            user = request.user
            chats =  []

            for chat in Chat.objects.filter(root_user = user).all():
                chats.append(chat)

            for chat in Chat.objects.filter(users = user).all():
                chats.append(chat)

            chats = [el for el, _ in groupby(chats)]

            for chat in chats:
                if chat.messages.count() == 0:
                    chats.remove(chat)

            template = loader.get_template('Messenger/include/chats.html')

            context = {
                'chats': chats
            }

            resp = template.render(context, request)
            return HttpResponse(resp)

        if(request.POST.get('is_search') == '1'):
            search = request.POST.get('search')
            user = request.user
            user_messages = Message.objects.filter(user = user).all()
            chats =  []

            for chat in Chat.objects.filter(root_user = user).all():
                chats.append(chat)

            for chat in Chat.objects.filter(users = user).all():
                chats.append(chat)

            chats = [el for el, _ in groupby(chats)]

            if(not search or search == ''):
                if(request.POST.get('is_mobile') == '1'):
                    template = loader.get_template('Messenger/mobile/last_chats.html')
                else:
                    template = loader.get_template('Messenger/include/chats.html')

                context = {
                    'chats': chats
                }

                resp = template.render(context, request)
                return HttpResponse(resp)

            search_blogers = []
            search_messages = []
            search_all_users = []

            for message in user_messages:
                if(str(search) in str(message.content) and search != ''):
                    search_messages.append(message)                           #поиск сообщений

            for chat in chats:
                if(str(search) in str(chat.root_user.username) and search != ''):
                    search_blogers.append(chat.root_user)

                for bloger in chat.users.all():
                    if(str(search) in str(bloger.username) and search != ''):
                        search_blogers.append(bloger)                          #поиск подписчиков

            for bloger in request.user.profile.read.all():
                if(str(search) in str(bloger.username) and search != ''):
                    search_blogers.append(bloger)

            for bloger in User.objects.all():
                if(str(search) in str(bloger.username) and search != ''):
                    search_all_users.append(bloger)                            #поиск всех пользователей

            search_blogers = [el for el, _ in groupby(search_blogers)]
            search_messages = [el for el, _ in groupby(search_messages)]
            search_all_users = [el for el, _ in groupby(search_all_users)]

            if(request.POST.get('is_mobile') == '1'):
                template = loader.get_template('Messenger/search/mobile.html')
            else:
                template = loader.get_template('Messenger/search/desktop.html')

            context = {
                'search': search,
                'search_blogers': search_blogers,
                'search_all_users': search_all_users,
                'search_messages': search_messages,
            }

            resp = template.render(context, request)
            return HttpResponse(resp)

    chats =  []

    for chat in Chat.objects.filter(root_user = user).all():
        chats.append(chat)

    for chat in Chat.objects.filter(users = user).all():
        chats.append(chat)

    context = {
        'chats': chats
    }
    return render(request, 'Messenger/index.html', context)

@login_required(login_url='/accounts/login/')
def chat(request, username):
    user = request.user
    try:
        chat_profile = User.objects.filter(username = username).get()
    except Exception:
        return important(request)

    if(Chat.objects.filter(root_user = user, users = chat_profile).count() == 0 and Chat.objects.filter(root_user = chat_profile, users = user).count() == 0):
        chat = Chat.objects.create(root_user = user)

        chat.users.add(chat_profile)
        chat.save()
    else:
        if(Chat.objects.filter(root_user = user, users = chat_profile).count() > 0 and Chat.objects.filter(root_user = chat_profile, users = user).count() == 0):
            chat = Chat.objects.filter(root_user = user, users = chat_profile).get()
        else:
            chat = Chat.objects.filter(root_user = chat_profile, users = user).get()

    if request.is_ajax():
        if(request.POST.get('search_chats') == '1'):
            search = request.POST.get('search')
            template = loader.get_template('Messenger/include/chat-search.html')
            user = request.user

            if(not search or search == ''):
                dialogs =  []

                for chat in Chat.objects.filter(root_user = request.user).all():
                    dialogs.append(chat)

                for chat in Chat.objects.filter(users = request.user).all():
                    dialogs.append(chat)

                context = {
                    'dialogs': dialogs
                }

                resp = template.render(context, request)
                return HttpResponse(resp)

            dialogs =  []
            search_dialogs = []

            for chat in Chat.objects.filter(root_user = user).all():
                dialogs.append(chat)

            for chat in Chat.objects.filter(users = user).all():
                dialogs.append(chat)

            for chat in dialogs:
                if(str(search) in str(chat.root_user.username) and search != ''):
                    search_dialogs.append(chat)

                for bloger in chat.users.all():
                    if(str(search) in str(bloger.username) and search != ''):
                        search_dialogs.append(chat)

            context = {
                'dialogs': search_dialogs
            }

            resp = template.render(context, request)
            return HttpResponse(resp)


        if(request.POST.get('edit_message') == '1'):
            message = Message.objects.get(id = request.POST.get('message_id'))
            new_data = request.POST.get('new_data')

            if message.user == request.user:
                message.content = new_data
                message.save()

            if(request.POST.get('is_desktop') == 'true'):
                template = loader.get_template('Messenger/Chat/desktop-message.html')
            else:
                template = loader.get_template('Messenger/Chat/message.html')

            context = {
                'chat_profile': chat_profile,
                'chat': chat,
            }

            resp = template.render(context, request)
            return HttpResponse(resp)

        if(request.POST.get('delete_chat') == '1'):

            chat.delete()
            return HttpResponse("document.location.href='/chat'", content_type="application/x-javascript")

        if(request.POST.get('leave_chat') == '1'):

            if(chat.root_user == user):
                return HttpResponse("$('#leave_error_modal').modal('show')", content_type="application/x-javascript")
            else:
                chat.users.remove(user)

                return redirect('/chat/')

        if(request.POST.get('clear_chat') == '1'):
            chat.messages.clear()
            chat.save()

            if(request.POST.get('is_desktop') == 'true'):
                template = loader.get_template('Messenger/Chat/desktop-message.html')
            else:
                template = loader.get_template('Messenger/Chat/message.html')

            context = {
                'chat_profile': chat_profile,
                'chat': chat,
            }

            resp = template.render(context, request)
            return HttpResponse(resp)

        if(request.POST.get('add_message_to_favorite') == '1'):
            user = request.user

            message = Message.objects.get(id = request.POST.get('id_message'))

            if(not message in user.profile.important_messages.all()):
                user.profile.important_messages.add(message)
            else:
                user.profile.important_messages.remove(message)

            if(request.POST.get('is_desktop') == 'true'):
                template = loader.get_template('Messenger/Chat/desktop-message.html')
            else:
                template = loader.get_template('Messenger/Chat/message.html')

            context = {
                'chat_profile': chat_profile,
                'chat': chat,
            }

            resp = template.render(context, request)
            return HttpResponse(resp)

        if(request.POST.get('delete_message') == '1'):

            message = Message.objects.get(id = request.POST.get('id_message'))

            message.delete()

            if(request.POST.get('is_desktop') == 'true'):
                template = loader.get_template('Messenger/Chat/desktop-message.html')
            else:
                template = loader.get_template('Messenger/Chat/message.html')

            context = {
                'chat_profile': chat_profile,
                'chat': chat,
            }

            resp = template.render(context, request)
            return HttpResponse(resp)

        if(request.POST.get('get_messages') == '1'):

            if(request.POST.get('is_desktop') == '1'):
                template = loader.get_template('Messenger/Chat/desktop-message.html')
            else:
                template = loader.get_template('Messenger/Chat/message.html')

            context = {
                'chat_profile': chat_profile,
                'chat': chat,
            }

            resp = template.render(context, request)
            return HttpResponse(resp)

        if(request.POST.get('write_message') == '1'):
            Message_text = request.POST.get('message')

            message = Message.objects.create(user = user, content = Message_text, chat = chat)
            message.save()

            chat.messages.add(message)
            chat.save()

            if(request.POST.get('is_desktop') == '1'):
                template = loader.get_template('Messenger/Chat/desktop-message.html')
            else:
                template = loader.get_template('Messenger/Chat/message.html')

            context = {
                'chat_profile': chat_profile,
                'chat': chat,
            }

            resp = template.render(context, request)
            return HttpResponse(resp)

    dialogs =  []

    for chat in Chat.objects.filter(root_user = request.user).all():
        dialogs.append(chat)

    for chat in Chat.objects.filter(users = request.user).all():
        dialogs.append(chat)

    context = {
        'chat_profile': chat_profile,
        'chat': chat,
        'dialogs': dialogs,
        'emoji_list': Emoji.objects.get(id = 1).emoji
    }

    return render(request, 'Messenger/Chat/chat.html', context)
