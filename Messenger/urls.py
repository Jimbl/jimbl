from django.urls import path
from django.conf.urls import url
from . import views

urlpatterns = [
    path('', views.index, name="index"),
    path('/important', views.important, name="important"),
    url(r'^(?P<username>\w+)/$', views.chat, name='chat_it'),
]
