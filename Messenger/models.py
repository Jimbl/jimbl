from django.db import models
from django.contrib.auth.models import User
import datetime

# Create your models here.

def get_time():
    return datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S');

class Message(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    content = models.TextField(max_length=1000, blank=True)
    date = models.DateTimeField(default=get_time, blank=True)
    chat = models.ForeignKey("Chat", on_delete=models.CASCADE, related_name='chat_list', blank=True, null=True)

class Chat(models.Model):
    messages = models.ManyToManyField(Message, blank=True, related_name='messages', null=True)
    root_user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, related_name='root_user')
    admin_users = models.ManyToManyField(User, blank=True, related_name='admins', null=True)
    users = models.ManyToManyField(User, blank=True, related_name='users_chat', null=True)
