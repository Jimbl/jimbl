from django.shortcuts import render, redirect
from django.core.mail import EmailMultiAlternatives
from django.contrib import auth
from App.models import Profile
from django.http import HttpResponse
from django.template import loader
# Create your views here.

def index(request):
    if request.is_ajax():
        user = request.user
        email_confirm_key = user.profile.email_confirm_key
        if request.POST.get('enter_email'):
            email = request.POST.get('email')
            if( Profile.objects.filter(email = email).all().count() == 0 ):
                send_mail('Подтверждение Email', 'Jimbl', request.POST.get('email'), email_confirm_key)
                template = loader.get_template('confirm_email/input-mail.html')
                context = {
                    'error': '',
                }
                resp = template.render(context, request)
                return HttpResponse(resp)
            else:
                template = loader.get_template('confirm_email/input-mail.html')
                context = {
                    'error': 'Такой Email уже используется!',
                }
                resp = template.render(context, request)
                return HttpResponse(resp)

        if request.POST.get('enter_email_key') == '1':
            user_object = Profile.objects.get(user = user)
            user_email_key = request.POST.get('email_key')
            if str(email_confirm_key) == str(user_email_key):
                user_object.email_confirm_status = True
                print(request.POST.get('user_email'))
                user_object.email = request.POST.get('user_email')
                user_object.user.email = request.POST.get('user_email')

                user_object.save()
                return HttpResponse('')
            else:
                template = loader.get_template('confirm_email/input.html')
                context = {
                    'error': 'Код введён не верно!',
                }
                resp = template.render(context, request)
                return HttpResponse(resp)


    return render(request, 'confirm_email/index.html')
