$.ajaxSetup({
 beforeSend: function(xhr, settings) {
     function getCookie(name) {
         var cookieValue = null;
         if (document.cookie && document.cookie != '') {
             var cookies = document.cookie.split(';');
             for (var i = 0; i < cookies.length; i++) {
                 var cookie = jQuery.trim(cookies[i]);
                 // Does this cookie string begin with the name we want?
             if (cookie.substring(0, name.length + 1) == (name + '=')) {
                 cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                 break;
             }
         }
     }
     return cookieValue;
     }
     if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
         // Only send the token to relative URLs i.e. locally.
         xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
     }
 }
});

$('input.form-control').on('focus', function(){
  $('bottom').addClass('hidden')
})

$('input.form-control').on('blur', function(){
  $('bottom').removeClass('hidden')
})


var inProgress = false;

$('#table-full-height').css('height', $(document).height() - 58 + 'px');

if(sessionStorage.getItem('active_page') === "2"){
  $('#enter_email').addClass('hidden');
  $('#enter_key').removeClass('hidden');
  $('#to_email').text(sessionStorage.getItem('user_email'));
}
else{
  $('#enter_email').removeClass('hidden');
  $('#enter_key').addClass('hidden');
  $('#user_email').val(sessionStorage.getItem('user_email'));
}

$('#next').click(function(){
  email = $('#user_email').val()
  if(email != ''){
    $('#anim_to_key').removeClass('hidden');
  }
  else{
    return false;
  }
  $.ajax({
    url: '/confirm-email/',
    type: 'post',
    data:{
      'enter_email': 1,
      'email': email,
    },
    success: function(data){
      $('#mail-input').html(data)
      sessionStorage.setItem('user_email', email)
      if($('.invalid-feedback').length == 0){
        $('#enter_email').addClass('hidden');
        $('#enter_key').removeClass('hidden');
        $('#email_key').focus();
        $('#to_email').text(sessionStorage.getItem('user_email'));
        $('#id_user_email').val(sessionStorage.getItem('user_email'));
        sessionStorage.setItem('active_page', 2);
     }
     $('#anim_to_key').addClass('hidden');
    }
  })
});

$('#submit_key').click(function(){
  $.ajax({
    url: '/confirm-email/',
    type: 'post',
    data:{
      'enter_email_key': 1,
      'email_key': $('#email_key').val(),
      'user_email': sessionStorage.getItem('user_email')
    },
    success: function(data){
      $('#input').html(data);
      if($('.invalid-feedback').length == 0){
        sessionStorage.setItem('active_page', 1);
        sessionStorage.setItem('user_email', '');
        document.location.href='/'+$('#name_user').val();
      }
    }
  })
});

$('#back').click(function(){
  $('#enter_key').addClass('hidden');
  $('#enter_email').removeClass('hidden');
  sessionStorage.setItem('active_page', 1);
});
