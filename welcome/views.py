from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login
from django.contrib import auth
from App.models import User
from django.http import HttpResponse
from django.template import loader
from django.contrib.auth.hashers import make_password, is_password_usable

def index(request):

    if request.is_ajax():
        if request.POST.get('registration'):
            username = request.POST.get('user')
            password = request.POST.get('pass')

            if len(password) >= 8 and User.objects.filter(username = username).count() == 0:
                try:
                    user = User.objects.create(username=username, password=make_password(password))
                    print(user)
                    login(request, user)

                    return HttpResponse("document.location.href='/"+username+"'", content_type="application/x-javascript")

                except Exception:
                    return HttpResponse()
            else:
                return HttpResponse()

        if request.POST.get('password_validation'):
            if request.POST.get('password_validation') == request.POST.get('password'):
                template = loader.get_template('welcome/requests/password-validation.html')
                context = {
                    'password_validation': True,
                    'password_valid': request.POST.get('password_validation')
                }
                resp = template.render(context, request)
                return HttpResponse(resp)
            else:
                template = loader.get_template('welcome/requests/password-validation.html')
                context = {
                    'password_validation': False,
                    'password_valid': request.POST.get('password_validation')
                }
                resp = template.render(context, request)
                return HttpResponse(resp)

        if request.POST.get('password_check'):
            if len(request.POST.get('password_check')) < 8:
                template = loader.get_template('welcome/requests/password-form.html')
                context = {
                    'password_check': False,
                    'password': request.POST.get('password_check')
                }
                resp = template.render(context, request)
                return HttpResponse(resp)
            else:
                template = loader.get_template('welcome/requests/password-form.html')
                context = {
                    'password_check': True,
                    'password': request.POST.get('password_check')
                }
                resp = template.render(context, request)
                return HttpResponse(resp)

        if request.POST.get('username_check'):
            username = request.POST.get('username_check')

            if User.objects.filter(username = username).count() > 0:
                template = loader.get_template('welcome/requests/username-form.html')
                context = {
                    'username_is_taken': 2,
                    'username_check': username
                }
                resp = template.render(context, request)
                return HttpResponse(resp)
            else:
                template = loader.get_template('welcome/requests/username-form.html')
                context = {
                    'username_is_taken': 3,
                    'username_check': username
                }
                resp = template.render(context, request)
                return HttpResponse(resp)

    if request.method == 'POST':
        form = UserCreationForm(request.POST)

        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']

            user = authenticate(username = username, password = password)
            login(request, user)
            return redirect('/' + auth.get_user(request).username)
    else:
        form = UserCreationForm()

    context = {'form' : form}
    return render(request, 'welcome/index.html', context)
