$(window).scroll(function() {

    if ($(this).scrollTop() > $("html").height()) {
        $('.btn-create-blog').css('display', 'inline-block');
        $('.btn-create-blog').css('position', 'relative');
        $('.btn-create-blog').removeClass('animated fadeOutRight');
        $('.btn-create-blog').addClass('animated fadeInRight');
    }
    else {
      $('.btn-create-blog').removeClass('animated fadeInRight');
      $('.btn-create-blog').addClass('animated fadeOutRight');
      $('.btn-create-blog').css('position', 'absolute');
      $('.btn-create-blog').css('top', 'calc(50% - 1.25em)');
    }

    if($(this).scrollTop() > $("html").height() / 2){
      $('.mouse-table').addClass('animated fadeOut');
    }
    else {
      $('.mouse-table').css('display', 'block');
      $('.mouse-table').removeClass('animated fadeOut');
    }
});

function anim_top_text(){
  $('.discription-text').addClass('animated fadeIn');
  $('.mouse-table').addClass('animated fadeInUp');
}

document.addEventListener("DOMContentLoaded", anim_top_text);
