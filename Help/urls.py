from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name="index"),
    path('register_account/', views.register_account, name='register_account'),
    path('publish_post/', views.publish_post, name='publish_post'),
    path('statistics/', views.statistics, name='statistics'),
    path('messenger/', views.messenger, name='messenger'),
    path('privacy_and_security/', views.privacy_and_security, name='privacy_and_security'),
    path('profile_setup/', views.profile_setup, name='profile_setup'),
    path('account_deletion/', views.account_deletion, name='account_deletion'),
    path('i_forgot_password/', views.i_forgot_password, name='i_forgot_password'),
    path('i_received_an_error_message/', views.i_received_an_error_message, name='i_received_an_error_message'),
    path('blog_creator/', views.blog_creator, name='blog_creator'),
    path('post_editor/', views.post_editor, name='post_editor'),
    path('tape/', views.tape, name='tape')
]
