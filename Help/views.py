from django.shortcuts import render

def index(request):
    return render(request, 'Help/index.html')

def register_account(request):
    return render(request, 'Help/register_account.html')

def statistics(request):
    return render(request, 'Help/statistics.html')

def messenger(request):
    return render(request, 'Help/messenger.html')

def blog_creator(request):
    return render(request, 'Help/blog_creator.html')

def publish_post(request):
    return render(request, 'Help/publish_post.html')

def privacy_and_security(request):
    return render(request, 'Help/privacy_and_security.html')

def profile_setup(request):
    return render(request, 'Help/profile_setup.html')

def account_deletion(request):
    return render(request, 'Help/account_deletion.html')

def i_forgot_password(request):
    return render(request, 'Help/i_forgot_password.html')

def i_received_an_error_message(request):
    return render(request, 'Help/i_received_an_error_message.html')

def post_editor(request):
    return render(request, 'Help/post_editor.html')

def tape(request):
    return render(request, 'Help/tape.html')
