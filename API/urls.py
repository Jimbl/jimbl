from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name="index"),
    path('user.auth', views.auth_user, name="auth_user"),
    path('user.get', views.get_user, name="get_user"),
]
