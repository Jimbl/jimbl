from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.contrib.auth.hashers import make_password, check_password
from django.contrib.auth.models import User
from App.models import Post, Blog
import json

def index(request):
    return render(request, 'API/index.html')

def auth_user(request):
    username = request.GET.get('username')
    password = request.GET.get('password')

    try:
        user = User.objects.get(username = username)
    except Exception:
        return JsonResponse({'Error':'User not found!'})

    if user.check_password(password):
        return JsonResponse({'token': user.profile.token})
    else:
        return JsonResponse({'Error':'Auth error!'})

def user_register(request):
    username = request.GET.get('username')
    password = request.GET.get('password')

    try:
        user = User.objects.create(username = username, password = make_password(password))
    except Exception:
        return JsonResponse({'Error': 'Exception on creating user!'})

    user.save()
    return JsonResponse({'token': user.profile.token})

def user_delete(request):
    username = request.GET.get('username')
    token = request.GET.get('token')

    try:
        user = User.objects.get(username = username)
    except Exception:
        return JsonResponse({'Error': 'User not found!'})

    if token == user.profile.token:
        user.delete()
        return JsonResponse({'Success': 'User success deleted'})
    else:
        return JsonResponse({'Error': 'token not valid'})

def user_follow(request):
    username_host = request.GET.get('username_host') # на кого подписываемся
    username_client = request.GET.get('username_client') # кто подписывается
    token = request.GET.get('token') # токен того кто подписывается

    try:
        user_client = User.objects.get(username = username_client)
    except Exception:
        return JsonResponse({'Error': 'User_client not found'})

    try:
        user_host = User.objects.get(username = username_host)
    except Exception:
        return JsonResponse({'Error': 'User_host not found'})

    if token == user_client.profile.token:
        if user_client not in user_host.profile.readers.all():
            user_host.profile.readers.add(user_client)
            user_client.profile.read.add(user_host)
            user_client.save()
            user_host.save()
            return JsonResponse({'Success': '1'})
        else:
            user_host.profile.readers.remove(user_client)
            user_client.profile.read.remove(user_host)
            user_host.save()
            user_client.save()
            return JsonResponse({'Success': '0'})
    else:
        return JsonResponse({'Error': 'token not valid'})

def user_update(request):
    username = request.GET.get('username')
    token = request.GET.get('token')

    try:
        user = User.objects.get(username = username)
    except Exception:
        return JsonResponse({'Error': 'User not found!'})

    if token == user.profile.token:
        new_username = request.GET.get('new_username')
        first_name = request.GET.get('first_name')
        last_name = request.GET.get('last_name')
        site = request.GET.get('site')
        discription_profile = request.GET.get('discription_profile')

def get_user(request):
    username = request.GET.get('username')
    token = request.GET.get('token')
    fields = request.GET.get('fields').split(',')

    try:
        user = User.objects.get(username = username)
    except Exception:
        return JsonResponse({'Error':'User not found!'})

    if token and token == user.profile.token:
        token_verify = True
    else:
        token_verify = False

    response = {}

    if 'first_name' in fields:
        response['first_name'] = user.first_name

    if 'last_name' in fields:
        response['last_name'] = user.last_name

    if 'image_profile' in fields:
        try:
            response['image_profile'] = user.profile.image_profile.url
        except Exception:
            response['image_profile'] = ''

    if 'image_header' in fields:
        try:
            response['image_header'] = user.profile.image_header.url
        except Exception:
            response['image_header'] = ''

    if 'discription_profile' in fields:
        discription = str(user.profile.discription_profile).replace('\n', ' ')
        discription = discription.replace('\r', ' ')
        response['discription_profile'] = discription

    if 'cnt_blogs' in fields:
        response['cnt_blogs'] = user.profile.blogs

    if 'cnt_posts' in fields:
        response['cnt_posts'] = user.profile.posts

    if 'cnt_readers' in fields:
        response['cnt_readers'] = user.profile.readers.count()

    if 'cnt_read' in fields:
        response['cnt_read'] = user.profile.read.count()

    if 'site' in fields:
        response['site'] = user.profile.site

    if 'month_rate' in fields:
        response['month_rate'] = user.profile.month_rate

    if 'is_verifed' in fields:
        response['is_verifed'] = user.profile.is_verifed

    if 'status_profile' in fields:
        response['status_profile'] = user.profile.status_profile

    if 'accept_send_messages' in fields:
        response['accept_send_messages'] = user.profile.accept_send_messages

    if 'accept_comment_posts' in fields:
        response['accept_comment_posts'] = user.profile.accept_comment_posts

    if 'read' in fields:
        if not user.profile.hide_list_read:
            read = user.profile.read.all()
            read_list = []
            for read in read:
                try:
                    image_profile = read.profile.image_profile.url
                except Exception:
                    image_profile = ''
                read_list.append({
                    'username':read.username,
                    'first_name':read.first_name,
                    'last_name':read.last_name,
                    'image_profile':image_profile,
                    'is_verifed':read.profile.is_verifed
                })

            response['read'] = read_list
        else:
            response['read'] = []

    if 'readers' in fields:
        if not user.profile.hide_list_readers:
            readers = user.profile.readers.all()
            readers_list = []
            for reader in readers:
                try:
                    image_profile = reader.profile.image_profile.url
                except Exception:
                    image_profile = ''
                readers_list.append({
                    'username':reader.username,
                    'first_name':reader.first_name,
                    'last_name':reader.last_name,
                    'image_profile':image_profile,
                    'is_verifed':reader.profile.is_verifed
                })

            response['readers'] = readers_list
        else:
            response['readers'] = []

    if 'date_of_registration' in fields:
        if not user.profile.hide_date_of_registration:
            response['date_of_registration'] = user.profile.Date_registration.strftime('%m %Y')
        else:
            response['date_of_registration'] = ''

    if 'email' in fields:
        if token_verify:
            response['email'] = user.profile.email
        else:
            response['email'] = 'Error: Token verifacation failed!'

    if 'phone' in fields:
        if token_verify:
            response['phone'] = user.profile.phone
        else:
            response['phone'] = 'Error: Token verifacation failed!'

    if 'views' in fields:
        if token_verify:
            response['views'] = user.profile.views
        else:
            response['views'] = 'Error: Token verifacation failed!'

    return HttpResponse(json.dumps(response, ensure_ascii=False), content_type="application/json")

def lenta_get(request):
    username = request.GET.get('username')
    token = request.GET.get('token')
    content = request.GET.get('content')
    filter = request.GET.get('filter')
    size = request.GET.get('size')

    try:
        user = User.objects.get(username = username)
    except Exception:
        return JsonResponse({'Error': 'User not found!'})

    if token == user.profile.token:
        if content == 'all':
            lenta = []
            posts = Post.objects.all().order_by(filter)[:size]
            for post in posts:
                lenta.append(post.id)
            response = {'lenta': lenta}
            return HttpResponse(json.dumps(response, ensure_ascii=False), content_type="application/json")
    else:
        return JsonResponse({'Error': 'Token not valid!'})

def post_get(request):
    username = request.GET.get('username')
    post_id = request.GET.get('post_id')

    try:
        user = User.objects.get(username = username)
    except Exception:
        return JsonResponse({'Error': 'User not found!'})

    try:
        post = Post.objects.get(id = post_id)
    except Exception:
        return JsonResponse({'Error': 'Article not found!'})

    if post.user.profile.status_profile == 0 or post.user.profile.status_profile == 1 and user in post.user.readers.all():
        try:
            user_image = post.user.profile.image_profile.url
        except Exception:
            user_image = ''
        try:
            post_image = post.image.url
        except Exception:
            post_image = ''

        comments = []
        for comment in post.comments.all():
            comments.append(comment.id)

        likes = []
        for user in post.likes.all():
            likes.append(user.id)

        response = {
            'post_id': post.id,
            'user_image': user_image,
            'user_full_name': post.user.first_name + ' ' + post.user.last_name,
            'user_username': post.user.username,
            'post_date': post.date.strftime('%c'),
            'post_image': post_image,
            'post_title': post.title,
            'post_discription': post.discription,
            'likes': likes,
            'comments': comments,
            'reposts': post.share.count(),
            'views': post.views.count(),
        }
        return HttpResponse(json.dumps(response, ensure_ascii=False), content_type="application/json")
    else:
        return JsonResponse({'Error': 'Permission denied!'})

def post_create(request):
    username = request.GET.get('username')
    token = request.GET.get('token')
    title = request.GET.get('title')
    discription = request.GET.get('discription')
    cateregory = request.GET.get('cateregory')
    

    try:
        user = User.objects.get(username = username)
    except Exception:
        return JsonResponse({'Error': 'User not found!'})

    if token == user.profile.token:

        try:
            request_photo = request.GET.get('image', stream = True)
        except Exception:
            return JsonResponse({'Error': 'Image not found!'})

        file_name = photo.split('/')[-1]
        lf = tempfile.NamedTemporaryFile()

        for block in request_photo.iter_content(1024 * 8):
             if not block:
                break
             lf.write(block)

        try:
            post = Post.objects.create(user = user, title = title)
        except Exception:
            pass
    else:
        pass

def post_delete(request):
    post_id = request.GET.get('post_id')
    token = request.GET.get('token')

    try:
        post = Post.objects.get(id = post_id)
    except Exception:
        return JsonResponse({'Error': 'Article not found!'})

    if token == post.user.profile.token:
        post.delete()
        return JsonResponse({'Success': 'Article was been deleted!'})
    else:
        return JsonResponse({'Success': 'Token not valid!'})
