from django.shortcuts import render
from App.models import Blog, Post, Profile
from django.template.base import Template
from django.template import loader
from django.http import HttpResponse

# Create your views here.

def index(request):
    if request.is_ajax():
        if(request.POST.get('is_search') == '1'):
            user = request.user
            search = request.POST.get('search')
            search_blogers = []
            search_posts = []
            users = Profile.objects.all().order_by('-views')
            posts = Post.objects.all().order_by('-views')

            for user in users:
                if(str(search) in str(user.user.username) and search != ''):
                    search_blogers.append(user)

            # for post in posts:
            #     if(str(search) in str(post.title) and search != ''):
            #         search_posts.append(post)

            if(request.POST.get('is_mobile') == '1'):
                template = loader.get_template('search_main/mobile_search/search-result.html')
            else:
                template = loader.get_template('search_main/include/search.html')

            context = {
                'search': search,
                'search_blogers': search_blogers,
                # 'search_posts': search_posts,
            }

            resp = template.render(context, request)
            return HttpResponse(resp)

    best_blogs = Blog.objects.all().order_by('-rate')[:10]

    context = {
        'best_blogs': best_blogs,
        'search': '',
    }
    return render(request, 'search_main/index.html', context)
