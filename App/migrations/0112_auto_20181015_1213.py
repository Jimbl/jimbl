# Generated by Django 2.1 on 2018-10-15 09:13

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('App', '0111_auto_20181010_2227'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='Date_registration',
            field=models.DateField(blank=True, default=datetime.date(2018, 10, 15)),
        ),
        migrations.AlterField(
            model_name='profile',
            name='email_confirm_key',
            field=models.IntegerField(blank=True, default=988902),
        ),
    ]
