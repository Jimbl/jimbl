# Generated by Django 2.1 on 2018-12-19 16:15

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('App', '0170_auto_20181217_0030'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='vk_uid',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AlterField(
            model_name='profile',
            name='Date_registration',
            field=models.DateField(blank=True, default=datetime.date(2018, 12, 19)),
        ),
        migrations.AlterField(
            model_name='profile',
            name='email_confirm_key',
            field=models.IntegerField(blank=True, default=697067),
        ),
    ]
