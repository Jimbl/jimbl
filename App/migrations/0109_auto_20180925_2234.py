# Generated by Django 2.1 on 2018-09-25 19:34

import App.models
import datetime
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('App', '0108_auto_20180924_1939'),
    ]

    operations = [
        migrations.AddField(
            model_name='repost',
            name='type',
            field=models.CharField(blank=True, default='repost', max_length=50),
        ),
        migrations.AlterField(
            model_name='profile',
            name='Date_registration',
            field=models.DateField(blank=True, default=datetime.date(2018, 9, 25)),
        ),
        migrations.AlterField(
            model_name='profile',
            name='email_confirm_key',
            field=models.IntegerField(blank=True, default=763295),
        ),
        migrations.AlterField(
            model_name='profile',
            name='read',
            field=models.ManyToManyField(blank=True, default=App.models.get_username, related_name='Read', to=settings.AUTH_USER_MODEL),
        ),
    ]
