# Generated by Django 2.0.7 on 2018-08-12 12:07

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('App', '0029_auto_20180808_0956'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='Date_registration',
            field=models.DateField(blank=True, default=datetime.date(2018, 8, 12)),
        ),
    ]
