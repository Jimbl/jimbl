# Generated by Django 2.1 on 2018-09-02 20:41

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('App', '0095_auto_20180828_1233'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='html_data',
            field=models.TextField(blank=True, max_length=9999, null=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='Date_registration',
            field=models.DateField(blank=True, default=datetime.date(2018, 9, 2)),
        ),
    ]
