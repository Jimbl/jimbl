# Generated by Django 2.0.7 on 2018-08-07 09:45

import App.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('App', '0021_auto_20180807_1231'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='avatar',
            field=models.ImageField(blank=True, upload_to=App.models.upload_avatar),
        ),
    ]
