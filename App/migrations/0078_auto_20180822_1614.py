# Generated by Django 2.1 on 2018-08-22 13:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('App', '0077_auto_20180822_1612'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='date',
            field=models.DateTimeField(blank=True, default='2018-08-22 16:14:35'),
        ),
    ]
