# Generated by Django 2.1 on 2018-12-13 17:53

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('App', '0164_auto_20181213_2048'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='email_confirm_key',
            field=models.IntegerField(blank=True, default=684786),
        ),
        migrations.AlterField(
            model_name='profile',
            name='views',
            field=models.ManyToManyField(blank=True, related_name='profile_views', to=settings.AUTH_USER_MODEL),
        ),
    ]
