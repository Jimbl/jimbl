# Generated by Django 2.0.7 on 2018-08-08 06:50

import App.models
import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('App', '0026_articles'),
    ]

    operations = [
        migrations.CreateModel(
            name='Avatars',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('img', models.ImageField(blank=True, upload_to=App.models.upload_avatar)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.RemoveField(
            model_name='articles',
            name='user',
        ),
        migrations.RemoveField(
            model_name='profile',
            name='image_profile',
        ),
        migrations.AlterField(
            model_name='profile',
            name='Date_registration',
            field=models.DateField(blank=True, default=datetime.date(2018, 8, 8)),
        ),
        migrations.DeleteModel(
            name='Articles',
        ),
    ]
