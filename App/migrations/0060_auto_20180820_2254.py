# Generated by Django 2.1 on 2018-08-20 19:54

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('App', '0059_auto_20180820_2252'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='Date_registration',
            field=models.DateField(blank=True, default=datetime.date(2018, 8, 20)),
        ),
    ]
