# Generated by Django 2.1 on 2018-11-25 18:09

import datetime
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('App', '0144_auto_20181124_1758'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='Date_registration',
            field=models.DateField(blank=True, default=datetime.date(2018, 11, 25)),
        ),
        migrations.AlterField(
            model_name='profile',
            name='email_confirm_key',
            field=models.IntegerField(blank=True, default=628515),
        ),
        migrations.AlterField(
            model_name='profile',
            name='read',
            field=models.ManyToManyField(blank=True, related_name='Read', to=settings.AUTH_USER_MODEL),
        ),
    ]
