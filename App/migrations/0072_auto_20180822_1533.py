# Generated by Django 2.1 on 2018-08-22 12:33

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('App', '0071_post_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='date',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2018, 8, 22, 12, 33, 0, 435710, tzinfo=utc)),
        ),
    ]
