# Generated by Django 2.1 on 2018-09-05 10:12

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('App', '0099_profile_views_cnt'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='views_cnt',
        ),
        migrations.AddField(
            model_name='profile',
            name='views',
            field=models.ManyToManyField(blank=True, related_name='views_users', to=settings.AUTH_USER_MODEL),
        ),
    ]
