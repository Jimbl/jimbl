# Generated by Django 2.1 on 2018-08-22 14:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('App', '0082_auto_20180822_1658'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='readers',
            field=models.IntegerField(blank=True, default=0),
        ),
    ]
