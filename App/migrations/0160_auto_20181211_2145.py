# Generated by Django 2.1 on 2018-12-11 18:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('App', '0159_auto_20181211_2143'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='month_rate_place',
        ),
        migrations.AlterField(
            model_name='profile',
            name='email_confirm_key',
            field=models.IntegerField(blank=True, default=712862),
        ),
    ]
