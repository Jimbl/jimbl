# Generated by Django 2.0.7 on 2018-08-05 14:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('App', '0018_auto_20180805_1746'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='avatar',
            field=models.FileField(blank=True, upload_to='uploads/<django.db.models.fields.related.OneToOneField>avatar/'),
        ),
    ]
