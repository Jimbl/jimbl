# Generated by Django 2.1 on 2018-12-29 18:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('App', '0181_auto_20181229_2119'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='email_confirm_key',
            field=models.IntegerField(blank=True, default=309622),
        ),
        migrations.AlterField(
            model_name='profile',
            name='token',
            field=models.CharField(blank=True, default='e7nt75kfs2iqj5sg48vgqg92l8pe1ze6ae86u2czq1ex0efdpr9761d9bn43rn0h', max_length=64),
        ),
    ]
