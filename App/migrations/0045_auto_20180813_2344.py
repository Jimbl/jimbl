# Generated by Django 2.0.7 on 2018-08-13 20:44

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('App', '0044_comment_user'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='date',
            field=models.DateTimeField(blank=True, default=datetime.date(2018, 8, 13)),
        ),
    ]
