# Generated by Django 2.0.7 on 2018-08-17 11:41

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('App', '0049_auto_20180815_2219'),
    ]

    operations = [
        migrations.AddField(
            model_name='blog',
            name='share',
            field=models.IntegerField(blank=True, default=0),
        ),
        migrations.AddField(
            model_name='post',
            name='share',
            field=models.IntegerField(blank=True, default=0),
        ),
        migrations.AlterField(
            model_name='comment',
            name='date',
            field=models.DateTimeField(blank=True, default=datetime.date(2018, 8, 17)),
        ),
        migrations.AlterField(
            model_name='post',
            name='date',
            field=models.DateField(blank=True, default=datetime.date(2018, 8, 17)),
        ),
        migrations.AlterField(
            model_name='profile',
            name='Date_registration',
            field=models.DateField(blank=True, default=datetime.date(2018, 8, 17)),
        ),
    ]
