from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from time import gmtime, strftime
import datetime
import random
from Messenger.models import Message
import string
from random import randint, choice

import PIL
from PIL import Image
from imagekit.models.fields import ImageSpecField
from imagekit.processors import ResizeToFit, Adjust,ResizeToFill

def upload_avatar(instance, filename):
    return '%s/avatar/%s' % (instance.user.username, filename)

def upload_image_header(instance, filename):
    return '%s/media_files/%s' % (instance.user.username, filename)

def upload_image_post(instance, filename):
    return '%s/image_post/%s' % (instance.user.username, filename)

def upload_image_blog(instance, filename):
    return '%s/image_blog/%s' % (instance.user.username, filename)

def get_time():
    return datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S');

def confirm_email_key_generate():
    return random.randint(111111, 999999)

def get_username(instance):
    return instance.user.username

class Select(models.Model):
    best_of_travel = models.TextField(max_length=500, blank=True)
    best_of_auto = models.TextField(max_length=500, blank=True)
    best_of_natural = models.TextField(max_length=500, blank=True)
    best_of_lifehacks = models.TextField(max_length=500, blank=True)
    best_of_buisness = models.TextField(max_length=500, blank=True)
    best_of_news = models.TextField(max_length=500, blank=True)
    best_of_food = models.TextField(max_length=500, blank=True)
    best_of_scince = models.TextField(max_length=500, blank=True)
    best_of_sport = models.TextField(max_length=500, blank=True)
    best_of_fashion = models.TextField(max_length=500, blank=True)

class Marker(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    add_name = models.BooleanField(default=False, blank=True)
    add_image = models.BooleanField(default=False, blank=True)
    add_discription = models.BooleanField(default=False, blank=True)
    add_email = models.BooleanField(default=False, blank=True)
    add_followers = models.BooleanField(default=False, blank=True)

class Comment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    data = models.TextField(max_length=500, blank=True)
    likes = models.ManyToManyField(User, blank=True, related_name='Likes_comment')
    date = models.DateTimeField(default=get_time,blank=True)

class Post(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=100, blank=True, null=True)
    image = models.FileField(upload_to=upload_image_post, blank=True, null=True)
    image_preview = ImageSpecField([Adjust(contrast=1.2, sharpness=1.1),
                            ResizeToFill(950, 650)], source='image',
                            format='JPEG', options={'quality': 90})
    image_preview_profile = ImageSpecField([Adjust(contrast=1.2, sharpness=1.1),
                            ResizeToFill(650, 450)], source='image',
                            format='JPEG', options={'quality': 90})
    image_media = ImageSpecField([Adjust(contrast=1.2, sharpness=1.1),
                            ResizeToFill(100, 60)], source='image',
                            format='JPEG', options={'quality': 90})
    image_search = ImageSpecField([Adjust(contrast=1.2, sharpness=1.1),
                            ResizeToFill(800, 600)], source='image',
                            format='JPEG', options={'quality': 90})
    simple_data = models.TextField(max_length=500, blank=True, null=True)
    discription = models.TextField(max_length=500, blank=True, null=True)
    likes = models.ManyToManyField(User, blank=True, related_name='Likes')
    comments = models.ManyToManyField(Comment, blank=True, related_name='Comment')
    html_data = models.TextField(max_length=999999, blank=True, null=True)
    repost = models.IntegerField(default=0, blank=True)
    date = models.DateTimeField(default=get_time,blank=True)
    share = models.ManyToManyField(User, blank=True, related_name='share')
    views = models.ManyToManyField(User, blank=True, related_name='related_views_post')
    cateregory_id = models.IntegerField(default=0, blank=True)
    rate = models.IntegerField(default=0, blank=True)

class Report(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, null=True, blank=True, related_name='report_post')
    blog = models.ForeignKey('Blog', on_delete=models.CASCADE, null=True, blank=True, related_name='report_blog')
    reason = models.CharField(max_length=100, blank=True)

class Blog(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    posts = likes = models.ManyToManyField(Post, blank=True, related_name='Posts')
    name = models.CharField(max_length=50, blank=True)
    discription = models.TextField(max_length=100, blank=True)
    image = models.FileField(upload_to=upload_image_blog, blank=True)
    image_preview = ImageSpecField([Adjust(contrast=1.2, sharpness=1.1),
                            ResizeToFill(750, 450)], source='image',
                            format='JPEG', options={'quality': 90})
    likes = models.IntegerField(default=0, blank=True)
    share = models.IntegerField(default=0, blank=True)
    rate = models.IntegerField(default=0, blank=True)
    related_views = models.ManyToManyField(User, blank=True, related_name='related_views_blogs')

class Repost(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField(default=get_time,blank=True)
    type = models.CharField(max_length=50, default='repost', blank=True)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, null=True, blank=True, related_name='post')

class Emoji(models.Model):
    emoji = models.CharField(max_length=100, blank=True)
    animal = models.CharField(max_length=100, blank=True)
    food = models.CharField(max_length=100, blank=True)
    sport = models.CharField(max_length=100, blank=True)
    car = models.CharField(max_length=100, blank=True)
    flag = models.CharField(max_length=100, blank=True)

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    sex = models.IntegerField(default=0, blank=True)
    birthday_date = models.DateTimeField(blank=True, null=True)
    email = models.CharField(max_length=100, blank=True)
    phone = models.CharField(max_length=50, blank=True)
    discription_profile = models.TextField(max_length=500, blank=True)
    Date_registration = models.DateField(default=datetime.date.today(),blank=True)
    blogs = models.IntegerField(default=0, blank=True)
    posts = models.IntegerField(default=0, blank=True)
    readers = models.ManyToManyField(User, blank=True, related_name='Readers')
    read = models.ManyToManyField(User, blank=True, related_name='Read')
    important_messages = models.ManyToManyField(Message, blank=True, related_name='Read')
    accept_send_notifications = models.BooleanField(default=True, blank=True)
    accept_send_news_of_update = models.BooleanField(default=True, blank=True)
    status_profile = models.IntegerField(default=0, blank=True)
    accept_send_messages = models.IntegerField(default=0, blank=True)
    accept_comment_posts = models.IntegerField(default=0, blank=True)
    hide_date_of_registration = models.BooleanField(default=False, blank=True)
    hide_list_read = models.BooleanField(default=False, blank=True)
    hide_list_readers = models.BooleanField(default=False, blank=True)
    is_verifed = models.BooleanField(default=False, blank=True)
    liked_posts = models.ManyToManyField(Post, blank=True, related_name='Liked_posts')
    liked_comments = models.ManyToManyField(Comment, blank=True, related_name='liked_comments')
    share_posts = models.ManyToManyField(Post, blank=True, related_name='share_posts')
    site = models.CharField(max_length=100, blank=True)
    instagram = models.CharField(max_length=50, blank=True)
    vk = models.CharField(max_length=50, blank=True)
    vk_id = models.CharField(max_length=50, blank=True)
    vk_firstname = models.CharField(max_length=50, blank=True)
    vk_lastname = models.CharField(max_length=50, blank=True)
    vk_image = models.ImageField(upload_to=upload_avatar, blank=True, default='')
    facebook = models.CharField(max_length=50, blank=True)
    twitter = models.CharField(max_length=50, blank=True)
    views = models.IntegerField(default=0, blank=True)
    related_views = models.ManyToManyField(User, blank=True, related_name='related_views')
    month_rate = models.IntegerField(default=0, blank=True)
    email_confirm_status = models.BooleanField(default=False, blank=True)
    email_confirm_key = models.IntegerField(default=confirm_email_key_generate(), blank=True)
    more_post_cat_id = models.IntegerField(default=0, blank=True)
    image_header = models.ImageField(upload_to=upload_image_header, blank=True, default='')
    image_profile = models.ImageField(upload_to=upload_avatar, blank=True, default='')
    image_profile_avatar = ImageSpecField([Adjust(contrast=1.2, sharpness=1.1),
                            ResizeToFill(200, 200)], source='image_profile',
                            format='JPEG', options={'quality': 90})
    image_profile_settings = ImageSpecField([Adjust(contrast=1.2, sharpness=1.1),
                            ResizeToFill(100, 100)], source='image_profile',
                            format='JPEG', options={'quality': 90})
    image_profile_40x40 = ImageSpecField([Adjust(contrast=1.2, sharpness=1.1),
                            ResizeToFill(40, 40)], source='image_profile',
                            format='JPEG', options={'quality': 90})
    image_profile_25x25 = ImageSpecField([Adjust(contrast=1.2, sharpness=1.1),
                            ResizeToFill(25, 25)], source='image_profile',
                            format='JPEG', options={'quality': 90})
    last_search_user = models.ManyToManyField(User, blank=True, related_name='last_search_user', null=True)
    black_list = models.ManyToManyField(User, blank=True, related_name='black_list', null=True)
    recently_emoji = models.CharField(max_length=12, blank=True)
    token = models.CharField(max_length=64, default=''.join(choice(string.ascii_lowercase + string.digits) for _ in range(64)), blank=True)

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
        Marker.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
