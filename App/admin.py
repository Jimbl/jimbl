from django.contrib import admin
from .models import Profile, Post, Blog, Comment, Repost, Select, Report, Marker, Emoji

# Register your models here.

admin.site.register(Profile)
admin.site.register(Post)
admin.site.register(Blog)
admin.site.register(Comment)
admin.site.register(Repost)
admin.site.register(Select)
admin.site.register(Report)
admin.site.register(Marker)
admin.site.register(Emoji)
