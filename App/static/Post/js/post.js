$.ajaxSetup({
 beforeSend: function(xhr, settings) {
     function getCookie(name) {
         var cookieValue = null;
         if (document.cookie && document.cookie != '') {
             var cookies = document.cookie.split(';');
             for (var i = 0; i < cookies.length; i++) {
                 var cookie = jQuery.trim(cookies[i]);
                 // Does this cookie string begin with the name we want?
             if (cookie.substring(0, name.length + 1) == (name + '=')) {
                 cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                 break;
             }
         }
     }
     return cookieValue;
     }
     if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
         // Only send the token to relative URLs i.e. locally.
         xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
     }
 }
});

function toggle_dark_theme(){
  if ($('body').hasClass('dark-theme')){
    $('body').removeClass('dark-theme');
    $('#main-card-post').removeClass('dark-theme');
    $('.scroller').removeClass('dark-theme');
    $('.card-header').removeClass('dark-theme');
    $('textarea').removeClass('dark-theme');
    $('.scroller-img').attr('src', '/static/App/def_image/down-arrow.svg');
    $('.load-more-comments').css('color', 'dark !important')
    $('.load-more-comments').addClass('btn-link')
    $('#main-nav-mobile').removeClass('bg-dark')
    $('#main-nav-mobile').removeClass('border-bottom-dark')
  }
  else{
    $('body').addClass('dark-theme');
    $('#main-card-post').addClass('dark-theme');
    $('.scroller').addClass('dark-theme');
    $('.card-header').addClass('dark-theme');
    $('textarea').addClass('dark-theme');
    $('.scroller-img').attr('src', '/static/App/def_image/down-arrow-white.svg')
    $('.load-more-comments').css('color', '#fafafa !important')
    $('.load-more-comments').removeClass('btn-link')
    $('#main-nav-mobile').addClass('bg-dark')
    $('#main-nav-mobile').addClass('border-bottom-dark')
  }
}

$(document).on('scroll', function(){
    if($(document).scrollTop() > 0){
      $('.scroller-return').addClass('hidden');
      $('.scroller-up').removeClass('hidden');
    }
    else{
      $('.scroller-return').removeClass('hidden');
      $('.scroller-up').addClass('hidden');
    }
});

$('.scroller-up').click(function(){
  var position = $(document).scrollTop();
  sessionStorage.setItem('position', position);
  $(document).scrollTop(0);
  setTimeout(function(){
    $('#full_image_profile').removeClass('hidden');
    $('#short_image_profile').addClass('hidden');
  }, 20)
  $('.scroller-up').addClass('hidden');
  $('.scroller-down').removeClass('hidden');
});

$('.scroller-return').click(function(){
  document.location.href='/'
});

$(document).ready(function(){
  $('textarea.input').each(function() {
      $(this).height($(this).prop('scrollHeight'));
  });

  $('textarea.input').attr('disabled', 'disabled');
  // $('#main-card-post > .card-body').css('min-height', $(window).height() - 200 + 'px');

  $('#bottom-post').removeClass('hidden');
});

var in_Progress = false;

function comment(id){
  $.ajax({
     url : '/',
     type : 'POST',
     data : {
       'write_comment': '1',
       'now_cnt_comments': $('.comments-'+id).length,
       'post_id': id,
       'main_data': $('input[name=comment_data_'+id+']').val()
     },
     success : function (data) {
      $('#post-comments-cnt-'+id).text(parseInt($('#post-comments-cnt-' + id).text()) + 1);
      $('.comments-list-'+id).html(data);
     },
     error: function(response){
       alert('error');
     }
 });
}

function load_more_comments(post_id){
  $.ajax({
       url : '/',
       type : 'POST',
       data : {
         post_id: post_id,
         load_more_comments: 1,
         now_cnt_comments: $('.comments-'+post_id).length
       },
       success : function (data) {
         $('.comments-list-'+post_id).html(data);
       },
  });
}

function hide_more_comments(post_id){
  $.ajax({
       url : '/',
       type : 'POST',
       data : {
         post_id: post_id,
         hide_more_comments: 1,
       },
       success : function (data) {
         $('.comments-list-'+post_id).html(data);
       },
  });
}

$('.load-more-comments').on('click', function(){
  var timer = null;
  var interval = 4000;
  if (timer !== null) return;
  timer = setInterval("render_comments("+$(this).attr('id')+")", interval);
})


function render_comments(post_id){
  $.ajax({
       url : '/',
       type : 'POST',
       data : {
         post_id: post_id,
         render_comments: 1,
         now_cnt_comments: $('.comments-'+post_id).length
       },
       success : function (data) {
         $('.comments-list-'+post_id).html(data);
       },
  });
}

function like_comment(form_like, object, id){
  if($(object).hasClass('liked')){
      $(object).removeClass('liked');
      $('.comment-like-' + id).removeClass('fas');
      $('.comment-like-' + id).addClass('far');
      $('#comment-like-cnt-' + id).text(parseInt($('#comment-like-cnt-' + id).text()) - 1);
  }
  else{
      $(object).addClass('liked');
      $('.comment-like-' + id).removeClass('far');
      $('.comment-like-' + id).addClass('fas');
      $('#comment-like-cnt-' + id).text(parseInt($('#comment-like-cnt-' + id).text()) + 1);
  }
  $.ajax({
     url : '/' + sessionStorage.getItem('logged_user') + '/',
     type : 'POST',
     data : jQuery("#"+form_like).serialize(),
     beforeSend: function(){
       in_Progress = true;
     },
     success : function (response) {
       in_Progress = false;
     },
 });
}

function like(form_like, object, id){
  if($(object).hasClass('liked')){
      $(object).removeClass('liked');
      $('.post-like-' + id).removeClass('fas');
      $('.post-like-' + id).addClass('far');
      $('#post-like-cnt-' + id).text(parseInt($('#post-like-cnt-' + id).text()) - 1);
  }
  else{
      $(object).addClass('liked');
      $('.post-like-' + id).removeClass('far');
      $('.post-like-' + id).addClass('fas');
      $('#post-like-cnt-' + id).text(parseInt($('#post-like-cnt-' + id).text()) + 1);
  }
  if(!in_Progress){
    $.ajax({
       url : '/'+sessionStorage.getItem('logged_user')+'/',
       type : 'POST',
       data : jQuery("#"+form_like).serialize(),
       beforeSend: function(){
         in_Progress = true;
       },
       success : function (response) {
         in_Progress = false;
       },
   });
 }
}

function follow(){
  if($('#follow_btn').hasClass('btn-info')){
    $('#follow_btn').removeClass('btn-info');
    $('#follow_btn').addClass('btn-outline-primary');
    $('#follow_btn').text('Читать');
  }
  else{
    $('#follow_btn').addClass('btn-info');
    $('#follow_btn').removeClass('btn-outline-primary');
    $('#follow_btn').text('Отписаться');
  }
  if(!in_Progress){
  jQuery.ajax({
      url:     '/'+$('#id_post_username').val()+'/',
      type:     "POST",
      dataType: "html",
      data: jQuery("#follow-form").serialize(),
      beforeSend: function(){
        in_Progress = true;
      },
      success: function(response) {
        in_Progress = false;
      },
      error: function(response) {
          alert("2");
      }
   });
 }
}
