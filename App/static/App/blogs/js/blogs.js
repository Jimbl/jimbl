$.ajaxSetup({
 beforeSend: function(xhr, settings) {
     function getCookie(name) {
         var cookieValue = null;
         if (document.cookie && document.cookie != '') {
             var cookies = document.cookie.split(';');
             for (var i = 0; i < cookies.length; i++) {
                 var cookie = jQuery.trim(cookies[i]);
                 // Does this cookie string begin with the name we want?
             if (cookie.substring(0, name.length + 1) == (name + '=')) {
                 cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                 break;
             }
         }
     }
     return cookieValue;
     }
     if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
         // Only send the token to relative URLs i.e. locally.
         xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
     }
 }
});

function fix(){
  if($(window).width() < 1200){
    $('#count_of_recoments').text(5 - $('.card.recoments-mobile').length);
  }
  else{
    $('[id=count_of_recoments]').text(5 - $('.card.recoments').length);
  }
}

$("#id_image_blog").change(function() {
  if (this.files && this.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#blog-preview').attr('src', e.target.result);
    }
    reader.readAsDataURL(this.files[0]);
  }
});

function create_blog(event){
  event.preventDefault();
  if($('input[name=title_blog]').val() == ''){
    $('#empty_title_blog_modal').modal('show');
    return false
  }
  if($('input[name=image_blog]').val() == ''){
    $('#empty_photo_blog_modal').modal('show');
    return false
  }
  var blog_data = new FormData($('#create-blog-form').get(0));
  $.ajax({
     url : '/'+sessionStorage.getItem('logged_user')+'/blogs',
     type : 'POST',
     data : blog_data,
     cache: false,
     processData: false,
     contentType: false,
     success : function (data) {
       $('.post-container').html(data);
       $('#create-blog-modal').modal('hide');
     },
 });
}

$(document).scroll(function(){
  if($(document).scrollTop() != 0){
    sessionStorage.setItem('scroll_position', $(document).scrollTop());
  }
});

$(document).ready(function(){
  $(document).scrollTop(sessionStorage.getItem('scroll_position'));
  fix();
});
