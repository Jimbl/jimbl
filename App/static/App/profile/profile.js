$.ajaxSetup({
 beforeSend: function(xhr, settings) {
     function getCookie(name) {
         var cookieValue = null;
         if (document.cookie && document.cookie != '') {
             var cookies = document.cookie.split(';');
             for (var i = 0; i < cookies.length; i++) {
                 var cookie = jQuery.trim(cookies[i]);
                 // Does this cookie string begin with the name we want?
             if (cookie.substring(0, name.length + 1) == (name + '=')) {
                 cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                 break;
             }
         }
     }
     return cookieValue;
     }
     if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
         // Only send the token to relative URLs i.e. locally.
         xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
     }
 }
});

$('#search-desktop-input').on("input", function() {
  $('.search-load').removeClass('hidden')
  search = $(this).val();
  $.ajax({
     url : '/',
     type : 'POST',
     data : {
       is_search: '1',
       search: search,
     },
     success : function (response) {
       $('.search-load').addClass('hidden')
      $('#search-desktop-list').html(response)
     },
 });
});

//blog views

$('#delete_post_from_blog_modal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var title = button.data('title')
  var id = button.data('id')
  var blog_id = button.data('blog')
  var modal = $(this)
  modal.find('[id=delete_post_from_blog_modal_btn]').attr('onclick', 'delete_post_from_blog('+id+', '+blog_id+')')
})

$('#delete_blog_modal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var title = button.data('title')
  var id = button.data('id')
  var modal = $(this)
  modal.find('.modal-title').text('Удалить блог - '+title)
  modal.find('.btn-danger').attr('onclick', 'delete_blog('+id+')')
})

function delete_share(repost_id){
  $.ajax({
    url: '/'+sessionStorage.getItem('logged_user')+'/',
    type: 'post',
    data: {
      delete_repost: '1',
      repost_id: repost_id,
      count_post: $('.card.post').length,
    },
    success: function(data){
      $('.post-container').html(data);
      fix_width_items()
    }
  })
}

function delete_blog(id){
  $.ajax({
    url: '/'+sessionStorage.getItem('logged_user')+'/blogs',
    type: 'post',
    data: {
      'delete_blog': '1',
      'blog_id': id,
      'count_blogs': $('.blog').length
    },
    success: function(data){
      $('.post-container').html(data);
      $('[id=delete_blog_modal]').modal('hide');
    }
  })
}

function like_media(id, object){
  if($(object).hasClass('liked')){
      $(object).removeClass('liked');
      $('.post-like-' + id).removeClass('fas');
      $('.post-like-' + id).addClass('far');
      $('[id=post-like-cnt-' + id+']').text(parseInt($('#post-like-cnt-' + id).text()) - 1);
  }
  else{
      $(object).addClass('liked');
      $('.post-like-' + id).removeClass('far');
      $('.post-like-' + id).addClass('fas');
      $('[id=post-like-cnt-' + id+']').text(parseInt($('#post-like-cnt-' + id).text()) + 1);
  }
  $.ajax({
    url: '/' + sessionStorage.getItem('logged_user') + '/media',
    type: 'post',
    data: {
      like: 1,
      post_id: id,
      this_image: 1,
    }
  })
}

$('.media').on('click', function(){
  if($(this).hasClass('select')){
      $(this).removeClass('select');
  }
  else{
    $('.media').removeClass('select');
    $(this).addClass('select');
  }
});

function fix_width_items(){
  //$('.recoments-mobile').css('height', h + 'px')
  if($(window).width() < 1200){
    $('[id=count_of_recoments]').text(5 - $('.card.recoments-mobile').length);
  }
  else{
    $('[id=count_of_recoments]').text(5 - $('.card.recoments').length);
  }
  if($(window).width() < 1200){
    $('[id=comment-text]').css('max-width', $('.mobile-row').width() - 98 + 'px');
    $('.img-fluid').css('border-radius', '0');
    $('.card-img-overlay').css('border-radius', '0');
    $('.card-title-wrapper').css('max-width', $('#mobile-post-card').width() - 32 + 'px');
  }
  else{
    $('.card-title-wrapper').css('max-width', $('.card-post').first().width() - 88 + 'px');
  }
  $('[id=comment-text]').css('max-width', $('.card.post').last().width() - 98 + 'px');
  $('[id=comment-text]').parent().css('min-width', $('.card.post').last().width() - 98 + 'px');
}

$('#feedback_area').keyup(function(){
  $('#count_numbers').text($('#feedback_area').val().length)
});

function feedback_submit(){
  if($('#feedback_area').val() != '' && $('#feedback_email').val() != ''){
    $('#anim_to_send_feedback').removeClass('hidden');
    var feedback_data = $('#feedback_form').serialize();
    $.ajax({
       url : '/',
       type : 'POST',
       data : feedback_data,
       success : function (data) {
        $('#feedback_area').val('');
        $('#feedback_modal').modal('hide');
        $('#feedback_success_modal').modal('show');
       },
   });
 }
}

function comment(post_id){
  if($(window).width() < 1200){
    var main_data = $('input#id_comment_data_'+post_id).first().val()
  }
  else{
    var main_data = $('input#comment_input_'+post_id).last().val()
  }
  if(main_data === '') return false;
  $.ajax({
     url : '/',
     type : 'POST',
     data : {
       post_id: post_id,
       write_comment: 1,
       main_data: main_data,
       now_cnt_comments: $('.comments-'+post_id).length,
     },
     success : function (data) {
        $('[id=post-comments-cnt-'+post_id+']').text(parseInt($('#post-comments-cnt-' + post_id).text()) + 1);
        $('.comments-list-'+post_id).html(data);
        fix_width_items()
     },
     error: function(response){
       alert('error');
     }
 });
}

$('.modal').on('hide.bs.modal', function (event) {
  $('#user_media').removeClass('hidden');
  $('#avatar-preview').attr('src', '');

})

$('.timeout').on('show.bs.modal', function (event) {
  setTimeout(function(){
    $('.modal-backdrop.show').last().css('opacity', '0');
    $('body').removeClass('modal-open')
    $('body').css('padding-right', '0')
    $('.navbar').css('padding-right', '0')
  }, 20)
})

$('.modal.timeout').on('show.bs.modal', function (event) {
  setTimeout(function(){$('.modal.timeout').modal('hide')}, 1500)
})

$('#delete_post_modal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var title = button.data('title')
  var id = button.data('id')
  var modal = $(this)
  modal.find('.modal-title').text('Удаление поста - ' + title)
  modal.find('.delete_post_btn').attr('onclick', 'delete_post('+id+')')
})

function delete_post(id){
  $.ajax({
     url : '/'+ sessionStorage.getItem('logged_user') +'/',
     type : 'POST',
     data : {
       post_id: id,
       delete_post: 1,
       count_post: $('.card.post').length,
     },
     success : function (data) {
      $('#post_'+id).html(data);
      $('#delete_post_modal').modal('hide');
      $('.post-container').html(data);
      fix_width_items()
     },
 });
}

function share_post(id){
  if($('#share_icon_'+id).hasClass('reposted')){
    return false;
  }
  $.ajax({
     url : '/' + sessionStorage.getItem('logged_user') + '/',
     type : 'POST',
     data : {
       post_id: id,
       share_post: 1,
       count_post: $('.card.post').length
     },
     success : function (data) {
       $('#share_success_modal').modal('show');
       $('#share_icon_'+id).addClass('reposted');
       $('#share_post_count_'+id).text(parseInt($('#share_post_count_' + id).text()) + 1);
       $('.post-container').html(data);
       fix_width_items()
     }
 });
}

$('#report_post_modal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var title = button.data('title')
  var id = button.data('id')
  var modal = $(this)
  modal.find('.modal-title').text('Пожаловаться на пост - ' + title)
  modal.find('.report_post_btn').attr('onclick', 'report_post('+id+')')
})

$('#report_blog_modal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var title = button.data('title')
  var id = button.data('id')
  var modal = $(this)
  modal.find('.modal-title').text('Пожаловаться на блог - ' + title)
  modal.find('.report_post_btn').attr('onclick', 'report_blog('+id+')')
})

function report_post(id){
  $.ajax({
     url : '/'+ sessionStorage.getItem('logged_user') +'/',
     type : 'POST',
     data : {
       report_post: 1,
       report_data: $('#report_reason_id').val(),
       report_post_id: id,
     },
     success : function (data) {
      $('#report_post_modal').modal('hide');
      $('#report_success_modal').modal('show');
      fix_width_items()
     },
 });
}

function report_blog(id){
  $.ajax({
     url : '/'+ sessionStorage.getItem('logged_user') +'/',
     type : 'POST',
     data : {
       report_blog: 1,
       report_data: $('#report_reason_id').val(),
       report_blog_id: id,
     },
     success : function (data) {
      $('#report_post_modal').modal('hide');
      $('#report_success_modal').modal('show');
      fix_width_items()
     },
 });
}

function toggle_overlay(id){
  if($('.img-overlay-'+id).hasClass('toggled')){
    $('[id=overlay-main-'+id+']').removeClass('hidden');
    $('[id=overlay-share-'+id+']').addClass('hidden');
    $('.img-overlay-'+id).removeClass('toggled')
    $('.img-overlay-'+id).css('background', 'rgba(0,0,0,0.5)');
  }
  else{
    $('.img-overlay-'+id).addClass('toggled');
    $('[id=overlay-main-'+id+']').addClass('hidden');
    $('[id=overlay-share-'+id+']').removeClass('hidden');
    $('.img-overlay-'+id).css('background', 'rgba(0,0,0,0.8)');
  }
}

function toggle_overlay_media(id){
  $('[id=img-share-overlay-'+id+']').toggleClass('hidden')
}

function load_more_comments(post_id){
  $.ajax({
       url : '/' + sessionStorage.getItem('logged_user') + '/',
       type : 'POST',
       data : {
         post_id: post_id,
         load_more_comments: 1,
         window_width: $(window).width(),
         now_cnt_comments: $('.comments-'+post_id).length
       },
       success : function (data) {
         console.log(2);
         $('.comments-list-'+post_id).html(data);
         fix_width_items()
       },
  });
}

function hide_more_comments(post_id){
  $.ajax({
       url : '/',
       type : 'POST',
       data : {
         post_id: post_id,
         hide_more_comments: 1,
         window_width: $(window).width(),
       },
       success : function (data) {
         $('.comments-list-'+post_id).html(data);
         fix_width_items()
       },
  });
}

$(function() {
  var timer = null;
  var interval = 4000;

  $('.load-more-comments').on('click', function(){
    if (timer !== null) return;
    timer = setInterval("render_comments("+$(this).attr('id')+")", interval);
  })

  $('.btn-hide-comments').on('click', function(){
    clearInterval(timer);
    timer = null
  })
});

function render_comments(post_id){
  $.ajax({
       url : '/',
       type : 'POST',
       data : {
         post_id: post_id,
         render_comments: 1,
         window_width: $(window).width(),
         now_cnt_comments: $('.comments-'+post_id).length
       },
       success : function (data) {
         $('.comments-list-'+post_id).html(data);
         fix_width_items()
       },
  });
}

function share_comment(post_id){
  if($(window).width() < 1200){
    var main_data = $('input#id_mobile_share_comment_data_'+post_id).val()
  }
  else{
    var main_data = $('input#id_share_comment_data_'+post_id).last().val()
  }
  $.ajax({
     url : '/',
     type : 'POST',
     data : {
       post_id: post_id,
       write_comment: 1,
       main_data: main_data,
       now_cnt_comments: $('.comments-'+post_id).length,
     },
     success : function (data) {
        $('[id=post-comments-cnt-'+post_id+']').text(parseInt($('#post-comments-cnt-' + post_id).text()) + 1);
        $('.comments-list-'+post_id).html(data);
        fix_width_items()
     },
     error: function(response){
       alert('error');
     }
 });
}

function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#avatar-preview').attr('src', e.target.result);
      $('#user_media').addClass('hidden');
    }
    reader.readAsDataURL(input.files[0]);
  }
}

function readURLblog(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#blog-preview').attr('src', e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
  }
}

$("#id_load_avatar").change(function() {
  readURL(this);
});

$("#id_image_blog").change(function() {
  readURLblog(this);
});

function add_post_to_blog(post_id, blog_id){
  var post_count = $('.card.post').length;
  $.ajax({
     url : '/'+sessionStorage.getItem('logged_user')+'/blogs/'+blog_id,
     type : 'POST',
     data : {
       add_post_to_blog: 1,
       post_id: post_id,
       blog_id: blog_id,
       count_post: post_count,
     },
     success : function (data) {
       $('.post-container').html(data);
       $('#add-post-modal').modal('hide');
       fix_width_items()
       if(post_count != $('.card.post').length){
         $('#top_profile_blog_cnt').text(parseInt($('#top_profile_blog_cnt').text()) + 1);
       }
     },
 });
}

function delete_post_from_blog(post_id, blog_id){
  var post_count = $('.card.post').length;
  $.ajax({
     url : '/'+sessionStorage.getItem('logged_user')+'/blogs/'+blog_id,
     type : 'POST',
     data : {
       delete_post_from_blog: 1,
       post_id: post_id,
       blog_id: blog_id,
       count_post: post_count,
     },
     success : function (data) {
       $('.post-container').html(data);
       $('#delete_post_from_blog_modal').modal('hide');
       fix_width_items()
     },
 });
}

function upload_avatar(event){
  event.preventDefault();

  if($('.media.select').length === 0 && $('#id_load_avatar').val() === ''){
    $('#empty_photo_modal').modal('show');
    return false;
  }

  if($('.media.select').length === 1){
    var url = $('.media.select').attr('src');
    $.ajax({
      url: '/'+sessionStorage.getItem('logged_user')+'/',
      type: 'post',
      data: {
        image_url: url,
        set_avatar_by_url: 1,
      },
      success: function(data){
        document.location.reload()
      }
    })
    return false;
  }

  var avatar_data = new FormData($('#avatar-set-form').get(0));
  jQuery.ajax({
     url : '/'+sessionStorage.getItem('logged_user')+'/',
     type : 'POST',
     data : avatar_data,
     cache: false,
     processData: false,
     contentType: false,
     success : function (data) {
       document.location.reload();
     },
 });
}

$(document).on('scroll', function(){
  if($(window).width() >= 1200){
    if($(document).scrollTop() >= 400){
      $('.scroller').removeClass('hidden');
      $('.scroller-up').removeClass('hidden');
      $('.scroller-down').addClass('hidden');
    }
    else{
     $('.scroller-down').css('padding-top', 400 - $(document).scrollTop() + 'px');
     if($('.scroller-down').hasClass('hidden')){
       $('.scroller').addClass('hidden');
     }
    }
  }
  else{
    if($(document).scrollTop() >= 400){
      $('.user-profile-wrapper').removeClass('hidden');
    }
    else{
      $('.user-profile-wrapper').addClass('hidden');
    }
  }
});

$('.scroller-up').click(function(){
  var position = $(document).scrollTop();
  sessionStorage.setItem('position', position);
  $(document).scrollTop(0);
  setTimeout(function(){
    $('#full_image_profile').removeClass('hidden');
    $('#short_image_profile').addClass('hidden');
  }, 20)
  $('.scroller-up').addClass('hidden');
  $('.scroller-down').removeClass('hidden');
});

$('.scroller-down').click(function(){
  $(document).scrollTop(sessionStorage.getItem('position'));
  $('.scroller-up').removeClass('hidden');
  $('.scroller-down').addClass('hidden');
  setTimeout(function(){
    $('#full_image_profile').addClass('hidden');
    $('#short_image_profile').removeClass('hidden');
  }, 20)
});

$(document).on('load', function(){
  $("#scroll-div").sticky({topSpacing: $('#main-nav').height() + $("#sticky").height() + 400});
})

$("#banner-close").click(
  function(){
    $("#banner").removeClass("animated slideInUp");
    $("#banner").addClass("animated slideOutDown");
  }
);

$("#post-overlay").click(
  function(){
    document.location.href='../';
  }
);

$("#id_simple_data").focus(
  function(){
    $(this).attr('rows', "3");
    $('#add-post-main').removeClass("hidden");
    $('#add-post-hide').removeClass("hidden");
    $('#add-nav-append').addClass('hidden');
  }
);

$("#add-post-hide").click(
  function(){
    $("#id_simple_data").attr('rows', "1");
    $('#add-post-main').addClass("hidden");
    $('#add-post-hide').addClass("hidden");
    $('#add-nav-append').removeClass('hidden');
  }
);

$('.link-scroll-up').click(function(){
  var position = $(document).scrollTop();
  sessionStorage.setItem('position', position);
  $(document).scrollTop(0);
  $('.link-scroll-down').removeClass('hidden');
});

$('.link-scroll-down').click(function(){
  var position = sessionStorage.getItem('position');
  $(document).scrollTop(position);
});

var inProgress = false;
var stop_load = false;

$(document).on('scroll', function(){
  $('#id_count_post').val($('.card.post').length);

  var down_position = $(window).scrollTop() + $(window).height() > $(document).height() - 200;
  if(down_position && !inProgress && !stop_load){
    var data = $('#load-content-form').serialize();
    var page = $('input[name=page]').val();
    var count_post = $('.card.post').length;
    $('.load-wrapp').removeClass('hidden');
    $.ajax({
       url : page,
       type : 'POST',
       data : data,
       beforeSend: function(){
         inProgress = true;
       }}).done(function(data){
           $('.post-container').html(data);
           inProgress = false;
           $('.load-wrapp').addClass('hidden');
           fix_width_items()
           if(count_post === $('.card.post').length){
            stop_load = true;
           }
         })
       }
});


$(document).scroll(function(){
  if($(document).scrollTop() != 0){
    sessionStorage.setItem('scroll_position', $(document).scrollTop());
  }
  if($(document).scrollTop() < $(window).height() / 2){
    $('.link-scroll-up').addClass('hidden');
  }
  else{
    $('.link-scroll-up').removeClass('hidden');
    $('.link-scroll-down').addClass('hidden');
  }
  if($('#top-profile-sticky-sticky-wrapper').hasClass('is-sticky')){
    $('#full_image_profile').addClass('hidden');
    $('#short_image_profile').removeClass('hidden');
    $('#short_image_profile').addClass('animated slideInDown');
  }
  else{
    $('#full_image_profile').removeClass('hidden');
    $('#short_image_profile').addClass('hidden');
  }
});

!function(t){
  t.fn.bcSwipe=function(e){var n={threshold:50};
  return e&&t.extend(n,e),this.each(function(){
    function e(t){
      1==t.touches.length&&(u=t.touches[0].pageX,c=!0,this.addEventListener("touchmove",o,!1))
    }
    function o(e){
      if(c){var o=e.touches[0].pageX,i=u-o;Math.abs(i)>=n.threshold&&(h(),t(this).carousel(i>0?"next":"prev"))}
    }
    function h(){
      this.removeEventListener("touchmove",o),u=null,c=!1
    }
    var u,c=!1;
    "ontouchstart"in document.documentElement&&this.addEventListener("touchstart",e,!1)
  }),this}}(jQuery);

// Swipe functions for Bootstrap Carousel
$('.carousel').bcSwipe({ threshold: 50 });

function follow(profile_view){
  if($('button#follow_btn').hasClass('btn-info')){
    $('button#follow_btn').removeClass('btn-info');
    $('button#follow_btn').addClass('btn-outline-primary');
    $('p#readers').text(parseInt($('#readers').text()) - 1);
    $('button#follow_btn').text('Читать');
  }
  else{
    $('button#follow_btn').addClass('btn-info');
    $('button#follow_btn').removeClass('btn-outline-primary');
    $('button#follow_btn').text('Отписаться');

    var readers = parseInt($('#readers').text());
    $('p#readers').text(readers + 1);
  }
  jQuery.ajax({
      url:     "/"+profile_view+"/",
      type:     "POST",
      dataType: "html",
      data: jQuery("#follow-form").serialize(),
   });
 }

$(document).ready(function(){
  var h = 0
  var elem = $('.recoments-p').toArray()
  for (var i = 0; i < elem.length; i++){
    if (h < $(elem[i]).height()){
      h = $(elem[i]).height()
    }
  }
  $('.recoments-p').css('height', h + 30 + 'px');

  $(function () {
    $("#top-profile-sticky").sticky({
        topSpacing: $('#main-nav').height()
        , zIndex: 2
    });
});
  $('#id_count_post').val($('.card.post').length);
  var data = $('#load-content-form').serialize();
  var page = $('input[name=page]').val();
  var count_post = $('.card.post').length;
  $('.load-start-wrapp').removeClass('hidden');
  $.ajax({
     url : page,
     type : 'POST',
     data : data,
     beforeSend: function(){
       inProgress = true;
     }}).done(function(data){
         $('.post-container').html(data);
         inProgress = false;
         $('.load-start-wrapp').addClass('hidden');
         fix_width_items()
         if(count_post === $('.card.post').length){
          stop_load = true;
         }
       })
  $(document).scrollTop(sessionStorage.getItem('scroll_position'));
  $('#id_window_width').val($(window).width());
  $(".link-scroll-up > div").css('height', $(window).height() - $("#sticky").height() - 420);
  $("#sticky").sticky({topSpacing: $('#main-nav').height()});
  $("#scroll-up").sticky({topSpacing: $('#main-nav').height() + $("#sticky").height()});
  $("#scroll-down").sticky({topSpacing: $('#main-nav').height() + $("#sticky").height()});
  $('#left_menu_sticky').sticky({topSpacing: $('#main-nav').height() + 24});
  fix_width_items();
});

$('.avatar').hover(
  function() {
    $('#img-avatar-overlay').removeClass('hidden');
  },
  function() {
    $('#img-avatar-overlay').addClass('hidden');
  }
)
