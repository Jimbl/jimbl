$('#feedback_area').keyup(function(){
  $('#count_numbers').text($('#feedback_area').val().length)
});

$('#search-desktop-input').on("change paste keyup focus", function() {
  $('.search-load').removeClass('hidden')
  search = $(this).val();
  $.ajax({
     url : '/',
     type : 'POST',
     data : {
       is_search: '1',
       search: search,
     },
     success : function (response) {
      $('.search-load').addClass('hidden')
      $('#search-desktop-list').html(response)
     },
 });
});

var in_Progress = false;

function like(form_like, object, id){
  if($(object).hasClass('liked')){
      $(object).removeClass('liked');
      $('.post-like-' + id).removeClass('fas');
      $('.post-like-' + id).addClass('far');
      $('[id=post-like-cnt-' + id+']').text(parseInt($('#post-like-cnt-' + id).text()) - 1);
  }
  else{
      $(object).addClass('liked');
      $('.post-like-' + id).removeClass('far');
      $('.post-like-' + id).addClass('fas');
      $('[id=post-like-cnt-' + id+']').text(parseInt($('#post-like-cnt-' + id).text()) + 1);
  }
  $.ajax({
     url : '/' + sessionStorage.getItem('logged_user') + '/',
     type : 'POST',
     data : jQuery("#"+form_like).serialize(),
     beforeSend: function(){
      in_Progress = true;
     },
     success : function (response) {
      in_Progress = false;
     },
 });
}

function show_new_posts(id){
  $.ajax({
     url : '/' + sessionStorage.getItem('logged_user') + '/',
     type : 'POST',
     data : jQuery("#delete_post_form_"+id).serialize(),
     success : function (data) {
      $('#post_'+id).html(data);
     },
 });
}

function follow(id){
  console.log(1);
  if($('button#follow_btn_'+id).hasClass('btn-info')){
    $('button#follow_btn_'+id).removeClass('btn-info');
    $('button#follow_btn_'+id).addClass('btn-outline-primary');
    $('button#follow_btn_'+id).text('Читать');
  }
  else{
    $('button#follow_btn_'+id).addClass('btn-info');
    $('button#follow_btn_'+id).removeClass('btn-outline-primary');
    $('button#follow_btn_'+id).text('Отписаться');
  }
  jQuery.ajax({
      url:     '/' + sessionStorage.getItem('logged_user') + '/',
      type:     "POST",
      dataType: "html",
      data: jQuery("#follow_form_"+id).serialize(),
      beforeSend: function(){
        in_Progress = true;
        console.log(2);
      },
      success: function(response) {
        in_Progress = false;
      },
   });
 }

function like_comment(form_like, object, id){
  if($(object).hasClass('liked')){
      $(object).removeClass('liked');
      $('.comment-like-' + id).removeClass('fas');
      $('.comment-like-' + id).addClass('far');
      $('[id=comment-like-cnt-' + id + ']').text(parseInt($('#comment-like-cnt-' + id).text()) - 1);
  }
  else{
      $(object).addClass('liked');
      $('.comment-like-' + id).removeClass('far');
      $('.comment-like-' + id).addClass('fas');
      $('[id=comment-like-cnt-' + id + ']').text(parseInt($('#comment-like-cnt-' + id).text()) + 1);
  }
  if(!in_Progress){
    $.ajax({
       url : '/' + sessionStorage.getItem('logged_user') + '/',
       type : 'POST',
       data : jQuery("#"+form_like).serialize(),
       beforeSend: function(){
         in_Progress = true;
       },
       success : function (response) {
        in_Progress = false;
       },
   });
 }
}

function fix_width_items(){
  $('[id=comment-text]').css('max-width', $('#post-card').width() - 98 + 'px');
  if($(window).width() < 1200){
    $('.card-title-wrapper').css('max-width', $('.post-container').width() - 44 + 'px');
  }
  else{
    $('.card-title-wrapper').css('max-width', $('.post-container').width() - 88 + 'px');
  }
  var h = 0
  var elem = $('.recoments-text-wrapper').toArray()
  for (var i = 0; i < elem.length; i++){
    if (h < $(elem[i]).height()){
      h = $(elem[i]).height()
    }
  }
  $('.recoments-text-wrapper').css('height', h + 15 + 'px');
}

function feedback_submit(){
  if($('#feedback_area').val() != '' && $('#feedback_email').val() != ''){
    $('#anim_to_send_feedback').removeClass('hidden');
    var feedback_data = $('#feedback_form').serialize();
    $.ajax({
       url : '/',
       type : 'POST',
       data : feedback_data,
       success : function (data) {
        $('#feedback_area').val('');
        $('#feedback_modal').modal('hide');
        $('#feedback_success_modal').modal('show');
        $('#anim_to_send_feedback').addClass('hidden');
       },
   });
 }
}

$("#add-post-hide").click(
  function(){
    $("#id_simple_data").attr('rows', "1");
    $('#add-post-main').addClass("hidden");
    $('#add-post-hide').addClass("hidden");
    $('#add-nav-append').removeClass('hidden');
  }
);

$('.link-scroll-up').click(function(){
  var position = $(document).scrollTop();
  sessionStorage.setItem('position', position);
  $(document).scrollTop(0);
  $('.link-scroll-down').removeClass('hidden');
});

$('.link-scroll-down').click(function(){
  var position = sessionStorage.getItem('position');
  $(document).scrollTop(position);
});

var inProgress = false;
var stop_load = false;

$(document).on('scroll', function(){
  $('#id_count_post').val($('.card.post').length);

  var down_position = $(window).scrollTop() + $(window).height() > $(document).height() - 200;
  if(down_position && !inProgress && !stop_load){
    var data = $('#load-content-form').serialize();
    var page = $('input[name=page]').val();
    var count_post = $('.card.post').length;
    $('.load-wrapp').removeClass('hidden');
    $.ajax({
       url : page,
       type : 'POST',
       data : data,
       beforeSend: function(){
         inProgress = true;
       }}).done(function(data){
           $('.post-container').html(data);
           inProgress = false;
           $('.load-wrapp').addClass('hidden');
           fix_width_items();
           if(count_post === $('.card.post').length){
            stop_load = true;
           }
         })
    }
});


$(document).scroll(function(){
  if($(document).scrollTop() != 0){
    sessionStorage.setItem('scroll_position', $(document).scrollTop());
  }
  if($(document).scrollTop() < $(window).height() / 2){
    $('.link-scroll-up').addClass('hidden');
  }
  else{
    $('.link-scroll-up').removeClass('hidden');
    $('.link-scroll-down').addClass('hidden');
  }
  if($('#sticky').parent().hasClass('is-sticky')){
    $('#full_image_profile').addClass('hidden');
    $('#short_image_profile').removeClass('hidden');
    $('#short_image_profile').addClass('animated slideInDown');
  }
  else{
    $('#full_image_profile').removeClass('hidden');
    $('#short_image_profile').addClass('hidden');
  }
});

!function(t){
  t.fn.bcSwipe=function(e){var n={threshold:50};
  return e&&t.extend(n,e),this.each(function(){
    function e(t){
      1==t.touches.length&&(u=t.touches[0].pageX,c=!0,this.addEventListener("touchmove",o,!1))
    }
    function o(e){
      if(c){var o=e.touches[0].pageX,i=u-o;Math.abs(i)>=n.threshold&&(h(),t(this).carousel(i>0?"next":"prev"))}
    }
    function h(){
      this.removeEventListener("touchmove",o),u=null,c=!1
    }
    var u,c=!1;
    "ontouchstart"in document.documentElement&&this.addEventListener("touchstart",e,!1)
  }),this}}(jQuery);

// Swipe functions for Bootstrap Carousel
$('.carousel').bcSwipe({ threshold: 50 });

function check_new_posts(){
  var page = $('input[name=page]').val();
  $.ajax({
     url : page,
     type : 'POST',
     data : {
       check_new_posts: 1,
     },
     success : function (data) {
       $('.post-container').html(data);
     },
     error: function(response){
       alert('error');
     }
 });
}

function comment(post_id){
  $.ajax({
     url : '/',
     type : 'POST',
     data : {
       post_id: post_id,
       write_comment: 1,
       main_data: $('[name=comment_data_'+post_id+']').val(),
       now_cnt_comments: $('.comments-'+post_id).length,
     },
     success : function (data) {
        $('#post-comments-cnt-'+post_id).text(parseInt($('#post-comments-cnt-' + post_id).text()) + 1);
        $('.comments-list-'+post_id).html(data);
        fix_width_items()
     },
     error: function(response){
       alert('error');
     }
 });
}

function load_more_comments(post_id){
  $.ajax({
       url : '/',
       type : 'POST',
       data : {
         post_id: post_id,
         load_more_comments: 1,
         now_cnt_comments: $('.comments-'+post_id).length
       },
       success : function (data) {
         $('.comments-list-'+post_id).html(data);
         fix_width_items()
       },
  });
}

function hide_more_comments(post_id){
  $.ajax({
       url : '/',
       type : 'POST',
       data : {
         post_id: post_id,
         hide_more_comments: 1,
       },
       success : function (data) {
         $('.comments-list-'+post_id).html(data);
         fix_width_items()
       },
  });
}

$('.load-more-comments').on('click', function(){
  var timer = null;
  var interval = 4000;
  if (timer !== null) return;
  timer = setInterval("render_comments("+$(this).attr('id')+")", interval);
})


function render_comments(post_id){
  $.ajax({
       url : '/',
       type : 'POST',
       data : {
         post_id: post_id,
         render_comments: 1,
         now_cnt_comments: $('.comments-'+post_id).length
       },
       success : function (data) {
         $('.comments-list-'+post_id).html(data);
         fix_width_items()
       },
  });
}

function delete_post(id){
  $.ajax({
     url : '/'+ sessionStorage.getItem('logged_user') +'/',
     type : 'POST',
     data : {
       post_id: id,
       delete_post: 1,
       count_post: $('.post').length,
     },
     success : function (data) {
      $('#post_'+id).html(data);
      $('#delete_post_modal').modal('hide');
      fix_width_items()
     },
 });
}

function report_post(id){
  $.ajax({
     url : '/'+ sessionStorage.getItem('logged_user') +'/',
     type : 'POST',
     data : {
       report_post: 1,
       report_data: $('#report_reason_id').val(),
       report_post_id: id,
     },
     success : function (data) {
      $('#report_post_modal').modal('hide');
      $('#report_success_modal').modal('show');
      fix_width_items()
     },
 });
}


$('.timeout').on('show.bs.modal', function (event) {
  setTimeout(function(){
    $('.modal-backdrop.show').css('opacity', '0');
    $('body').removeClass('modal-open')
    $('body').css('padding-right', '0')
    $('.navbar').css('padding-right', '0')
  }, 10)
})

function share_post(id){
  if($('#share_icon_'+id).hasClass('reposted')){
    return false;
  }
  $.ajax({
     url : '/',
     type : 'POST',
     data : {
       share_post: 1,
       post_id: id,
       count_post: $('.post').length,
     },
     success : function (data) {
       $('#share_success_modal').modal('show');
       $('#share_icon_'+id).addClass('reposted');
       $('#share_post_count_'+id).text(parseInt($('#share_post_count_' + id).text()) + 1);
       fix_width_items()
     }
 });
}

$(document).on('click', function(event){
  console.log($(event.target));
  if($(event.target).hasClass('row') || $(event.target).hasClass('card-body') || $(event.target).hasClass('col-xl-3') || $(event.target).hasClass('col-xl-1') || $(event.target).hasClass('media-body')){
    $('.overlay-share').addClass('hidden')
    $('.overlay-main').removeClass('hidden')
    $('.img-overlay').removeClass('toggled')
    $('[id=img-overlay]').css('background', 'rgba(0,0,0,0.5)');
  }
})

function toggle_overlay(id){
  if($('.img-overlay-'+id).hasClass('toggled')){
    $('#overlay-main-'+id).removeClass('hidden');
    $('#overlay-share-'+id).addClass('hidden');
    $('.img-overlay-'+id).removeClass('toggled')
    $('.img-overlay-'+id).css('background', 'rgba(0,0,0,0.5)');
    sessionStorage.setItem('toggled-sharing-post', id)
  }
  else{
    $('.img-overlay-'+id).addClass('toggled');
    $('#overlay-main-'+id).addClass('hidden');
    $('#overlay-share-'+id).removeClass('hidden');
    $('.img-overlay-'+id).css('background', 'rgba(0,0,0,0.8)');
  }
}

$('.modal.timeout').on('show.bs.modal', function (event) {
  setTimeout(function(){$('.modal.timeout').modal('hide')}, 1500)
})

$('#tools_post_modal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var id = button.data('id')
  var title = button.data('title')
  var user = button.data('user')
  var modal = $(this)
  modal.find('.tools_post_edit').attr('href', '/creator/edit-post/'+id)
  modal.find('.tools_post_delete').attr('data-title', title)
  modal.find('.tools_post_delete').attr('data-id', id)
  modal.find('.tools_post_report').attr('data-title', title)
  modal.find('.tools_post_report').attr('data-id', id)
  modal.find('.clipboard-trigger').attr('data-clipboard-target', '#link_post_'+id)
  modal.find('.clipboard-trigger').attr('data-clipboard-target', '#link_post_'+id)
})

$('.modal').on('show.bs.modal', function (event) {
  $('.modal').modal('hide')
})


$('#delete_post_modal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var title = button.data('title')
  var id = button.data('id')
  var modal = $(this)
  modal.find('.modal-title').text('Удаление поста - ' + title)
  modal.find('.delete_post_btn').attr('onclick', 'delete_post('+id+')')
})

$('#report_post_modal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var title = button.data('title')
  var id = button.data('id')
  var modal = $(this)
  modal.find('.modal-title').text('Пожаловаться на пост - ' + title)
  modal.find('.report_post_btn').attr('onclick', 'report_post('+id+')')
})

$(document).ready(function(){

  $(function () {
      $("#left_menu_sticky").sticky({
          topSpacing: $('#main-nav').height()
          , zIndex: 2
      });
  });

  fix_width_items()
  $('.alert').alert()
  $('#id_window_width').val($(window).width());
  $(".link-scroll-up > div").css('height', $(window).height() - $("#sticky").height() - 420);
  $("#sticky").sticky({topSpacing: $('#main-nav').height()});
  $("#scroll-up").sticky({topSpacing: $('#main-nav').height() + $("#sticky").height()});
  $("#scroll-down").sticky({topSpacing: $('#main-nav').height() + $("#sticky").height()});
  var data = $('#load-content-form').serialize();
  var page = $('input[name=page]').val();
  $('.load-start-wrapp').removeClass('hidden');
  $.ajax({
     url : page,
     type : 'POST',
     data : data,
     beforeSend: function(){
       inProgress = true;
     }}).done(function(data){
         $('.post-container').html(data);
         $('.load-start-wrapp').addClass('hidden');
         fix_width_items();
       })
});

$('.avatar').hover(
  function() {
    $('#img-avatar-overlay').removeClass('hidden');
  },
  function() {
    $('#img-avatar-overlay').addClass('hidden');
  }
)
