$('input.form-control').on('focus', function(){
  $('bottom').addClass('hidden')
})

$('input.form-control').on('blur', function(){
  $('bottom').removeClass('hidden')
})

$.ajaxSetup({
 beforeSend: function(xhr, settings) {
     function getCookie(name) {
         var cookieValue = null;
         if (document.cookie && document.cookie != '') {
             var cookies = document.cookie.split(';');
             for (var i = 0; i < cookies.length; i++) {
                 var cookie = jQuery.trim(cookies[i]);
                 // Does this cookie string begin with the name we want?
             if (cookie.substring(0, name.length + 1) == (name + '=')) {
                 cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                 break;
             }
         }
     }
     return cookieValue;
     }
     if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
         // Only send the token to relative URLs i.e. locally.
         xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
     }
 }
});

function username_check(value){
  if(value){
    $.ajax({
      url: '/welcome/',
      type: 'post',
      data: {
        username_check: value
      },
      success: function(response){
        $('[id=username-field]').html(response)
      },
      error: function(){
        console.log(2);
      }
    })
  }
}

function password_check(value){
  if(value){
    $.ajax({
      url: '/welcome/',
      type: 'post',
      data: {
        password_check: value
      },
      success: function(response){
        $('[id=password-field]').html(response)
      },
      error: function(){
        console.log(2);
      }
    })
  }
}

function password_validation(value){
  if(value){
    $.ajax({
      url: '/welcome/',
      type: 'post',
      data: {
        password_validation: value,
        password: $('#pass_1').val()
      },
      success: function(response){
        $('[id=password-validation]').html(response)
      },
      error: function(){
        console.log(2);
      }
    })
  }
}

function registration(){
  if(!$('#accept_check_1').prop("checked")){
    $('.accept-check-box-1').popover('show')
    return false
  }
  var data = $('#registration_form').serialize() + '&registration=1'
  $.ajax({
    url: '/welcome/',
    type: 'post',
    data: data
  })
}

function check_reg(checkbox){
  if ($('.invalid-feedback').length == 0 && $(checkbox).prop("checked")){
    $('.btn-reg').removeClass('disabled')
    $('.btn-reg').attr('onclick', 'registration()')
  }
  else{
    $('.btn-reg').addClass('disabled')
    $('.btn-reg').attr('onclick', '')
  }
}
