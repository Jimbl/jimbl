from django.shortcuts import render, redirect, render_to_response
from django.template import RequestContext
from django.contrib.auth.forms import UserCreationForm
from .models import Post, Comment, Profile, Blog, User, Repost, Select, Report, Emoji
from statistic.models import month_activites_rate, month_egegament_rate
from django.contrib.auth import authenticate, login, logout
from django.contrib import auth
from .forms import BlogForm, PostForm, Avatar_Upload_Form
from django.http import HttpResponse
from django.template.context import Context
from django.template.base import Template
from django.template import loader
from django.db.models import F
from django.http import Http404
from random import shuffle
from django.contrib.auth.decorators import login_required
from creator.models import Image_upload, Video_upload
from django.core.mail import EmailMultiAlternatives
from random import randint, choice
import datetime
import calendar
from urllib import request as r
from urllib import parse
import json
import requests
import ast
import string
from operator import attrgetter
import tempfile
from django.core import files

def err404(request):
    return render(request, 'Errors/404.html')

@login_required(login_url='/accounts/login/')
def statistic(request):

    best_bloggers = Profile.objects.all().order_by('-month_rate')

    context = {
        'now_date': datetime.date.today(),
        'best_blogers': best_bloggers,
    }
    return render(request, 'statistic/index.html', context)

@login_required(login_url='/accounts/login/')
def mobile_search(request):
    user = request.user
    last_search_users = user.profile.last_search_user.all()[:5]

    recomended_posts = Post.objects.filter(cateregory_id = user.profile.more_post_cat_id).all().order_by('-rate')[:22]

    context = {
        'recomended_posts': recomended_posts,
        'last_search_users': last_search_users,
        'search': '',
    }

    return render(request, 'Search/search.html', context)

def licence(request):
    return render(request, 'Licence/licence.html')

def update_month_rate(user_id):
    user = User.objects.get(id = user_id)

    posts = Post.objects.filter(user = user).all()

    access_posts = []
    rating = 0

    now_date = datetime.date.today()
    now_month = now_date.strftime('%m')

    if(now_month[0] == '0'):
        now_month = int(post_month[1])

    now_year = now_date.strftime('%y')

    for post in posts:
        post_month = post.date.strftime('%m')
        post_year = post.date.strftime('%y')

        if(post_month[0] == '0'):
            post_month = post_month[1]

        if(int(now_year) == int(post_year) and int(now_month) == int(post_month)):
            access_posts.append(post)

    for post in access_posts:
        rating += int(post.rate)

    user.profile.month_rate = rating
    user.save()

def update_rating_post(post_id):
    post = Post.objects.get(id = post_id)

    try:
        rating = int((post.likes.count() + post.comments.count() + post.share.count()) / post.views.count() * 100)
    except ZeroDivisionError:
        rating = 0

    post.rate = rating
    post.save()

def update_rating_blog(blog_id):
    blog = Blog.objects.get(id = blog_id)

    sum = 0
    for post in blog.posts.all():
        sum += post.rate

    try:
        rating = int(sum / blog.posts.count())
    except ZeroDivisionError:
        rating = 0

    blog.rate = rating
    blog.save()

def news_followers(request):
    user = request.user
    follow_users = user.profile.read.all()
    news = []
    reposts = []

    for user in follow_users:
        share_posts = Repost.objects.filter(user = user).all() # all user's repost articles
        posts = Post.objects.filter(user = user).all()

        for post in posts:
            news.append(post)

        for share_post in share_posts:
            reposts.append(share_post)

    user_posts = Post.objects.filter(user = request.user).all()
    for post in user_posts:
        news.append(post)

    news.extend(reposts)

    sort_news = sorted(news, key=lambda x: x.date, reverse=True)
    return sort_news

def register(request):

    if request.method == 'GET':
        if request.GET.get('code'):
            code = request.GET.get('code')
            data_dict = {
                'client_id': '6789266',
                'client_secret':'QQZeKIBWXUdt1dw3th4i',
                'redirect_uri':'https://jimbl.ru/registration',
                'code':code
            }
            r = requests.get("https://oauth.vk.com/access_token",  params=data_dict) #send request
            url = r.url #get response
            tmp = r.content.decode('utf-8')
            resp = ast.literal_eval(tmp)
            user_id = resp.get('user_id')
            access_token = resp.get('access_token')

            if Profile.objects.filter(vk_id = user_id).all().count() == 0:
                #create new user with vk
                username = 'vk_' + str(user_id)
                password = ''.join(choice(string.ascii_uppercase + string.digits) for _ in range(8))

                data_dict_2 = {
                    'user_ids': str(user_id),
                    'fields': 'photo_400_orig',
                    'access_token': access_token,
                    'v': '5.92'
                }

                r_2 = requests.get("https://api.vk.com/method/users.get",  params=data_dict_2)
                tmp_2 = r_2.content.decode('utf-8')
                resp_2 = json.loads(tmp_2)
                resp_2_list = resp_2.get('response')

                name = resp_2_list[0].get('first_name')
                last_name = resp_2_list[0].get('last_name')
                photo = resp_2_list[0].get('photo_400_orig')

                request_photo = requests.get(photo, stream=True)

                file_name = photo.split('/')[-1]
                lf = tempfile.NamedTemporaryFile()

                for block in request_photo.iter_content(1024 * 8):
                     if not block:
                        break
                     lf.write(block)

                user = User.objects.create_user(username=username, password=password, first_name = name, last_name = last_name)
                user.profile.vk_id = str(user_id)
                user.profile.image_profile.save(file_name, files.File(lf))
                user.save()

                login(request, user)

                return redirect('/' + auth.get_user(request).username)
            else:
                user_profile = Profile.objects.get(vk_id = user_id)
                user = user_profile.user
                login(request, user)
                return redirect('/' + auth.get_user(request).username)



    if request.method == 'POST':
        form = UserCreationForm(request.POST)

        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']

            user = authenticate(username = username, password = password)
            login(request, user)
            return redirect('/' + auth.get_user(request).username)
    else:
        form = UserCreationForm()

    context = {'form' : form}
    return render(request, 'registration/register.html', context)

@login_required(login_url='/accounts/login/')
def feed(request):

    best_bloggers = Profile.objects.all().order_by('-views')[:3]
    best_blogs = Blog.objects.all().order_by('-rate')[:10]

    recomended_profiles = Profile.objects.all().order_by('-month_rate')[:10]

    if request.is_ajax():
        if(request.POST.get('load_content') == '1'):
            template = loader.get_template('App/include/post_card.html')
            context = {
                'news_list': news_followers(request)[:int(request.POST.get('count_post')) + 22],
                'recomended_profiles': recomended_profiles,
            }
            resp = template.render(context, request)
            return HttpResponse(resp)

    context = {
        'news_list' : news_followers(request)[:20],
        'best_blogs': best_blogs,
        'best_bloggers': best_bloggers,
        'recomended_profiles': recomended_profiles,
    }
    return render(request, 'Feed/feed.html', context)

def interesting(request):

    if request.is_ajax():
        if(request.POST.get('load_content') == '1'):
            template = loader.get_template('App/include/post_card.html')
            context = {
                'news_list': Post.objects.all().order_by('-views')[:int(request.POST.get('count_post')) + 22]
            }
            resp = template.render(context, request)
            return HttpResponse(resp)

    posts = Post.objects.all().order_by('-views')[:20]

    best_bloggers = Profile.objects.all().order_by('-views')[:3]
    best_blogs = Blog.objects.all().order_by('-rate')[:10]

    context = {
        'news_list' : posts,
        'best_blogs': best_blogs,
        'best_bloggers': best_bloggers,
    }
    return render(request, 'interesting/interesting.html', context)

def featured(request):

    if request.is_ajax():
        if(request.POST.get('load_content') == '1'):
            template = loader.get_template('App/include/post_card.html')
            context = {
                'news_list': Post.objects.all().order_by('-likes')[:int(request.POST.get('count_post')) + 22]
            }
            resp = template.render(context, request)
            return HttpResponse(resp)

    posts = Post.objects.all().order_by('-likes')[:20]

    best_bloggers = Profile.objects.all().order_by('-views')[:3]
    best_blogs = Blog.objects.all().order_by('-rate')[:10]

    context = {
        'news_list' : posts,
        'best_blogs': best_blogs,
        'best_bloggers': best_bloggers,
    }
    return render(request, 'featured/featured.html', context)

def show_cateregory(request, cateregory):

    cateregory_list = ['travel', 'auto', 'natural', 'lifehacks', 'buisness', 'news', 'food', 'science', 'sport', 'shopping']

    if cateregory in cateregory_list:
        id = cateregory_list.index(cateregory) + 1
    else:
        return profile_view(request, cateregory)

    if request.is_ajax():
        if(request.POST.get('check_new_posts') == '1'):
            template = loader.get_template('App/include/post_card.html')
            context = {
                'new_post': Post.objects.get(cateregory_id = int(request.POST.get('cateregory_id'))).order_by('-date')
            }
            resp = template.render(context, request)
            return HttpResponse(resp)

        if(request.POST.get('load_content') == '1'):
            if(request.POST.get('cateregory_id')):
                recomended_profiles = Profile.objects.all().order_by('-month_rate')[:6]
                template = loader.get_template('App/include/post_card.html')
                context = {
                    'news_list': Post.objects.filter(cateregory_id = int(request.POST.get('cateregory_id'))).order_by('-date')[:int(request.POST.get('count_post')) + 22],
                    'recomended_profiles': recomended_profiles,
                }
                resp = template.render(context, request)
                return HttpResponse(resp)

    posts = Post.objects.filter(cateregory_id = id).order_by('-date')[:20]

    best_blogs = Blog.objects.all().order_by('-rate')[:10]

    recomended_profiles = Profile.objects.all().order_by('-month_rate')[:6]

    templates = [
        'Tags/travel.html',
        'Tags/auto.html',
        'Tags/natural.html',
        'Tags/lifehacks.html',
        'Tags/buisness.html',
        'Tags/news.html',
        'Tags/food.html',
        'Tags/science.html',
        'Tags/sport.html',
        'Tags/shopping.html',
    ]

    context = {
        'news_list' : posts,
        'best_blogs': best_blogs,
        'recomended_profiles': recomended_profiles,
    }
    return render(request, templates[id - 1], context)

def terms(request):
    return render(request, 'Terms/terms.html')

def random(request):
    if request.is_ajax():
        if(request.POST.get('load_content') == '1'):
            posts = Post.objects.all()[:int(request.POST.get('count_post')) + 22]
            sh_posts = []
            for post in posts:
                sh_posts.append(post)
            shuffle(sh_posts)
            posts = sh_posts
            template = loader.get_template('App/include/post_card.html')
            context = {
                'news_list': posts
            }
            resp = template.render(context, request)
            return HttpResponse(resp)

    posts = Post.objects.all()[:20]

    sh_posts = []

    for post in posts:
        sh_posts.append(post)

    shuffle(sh_posts)

    posts = sh_posts

    best_bloggers = Profile.objects.all().order_by('-views')[:3]

    best_blogs = Blog.objects.all().order_by('-rate')[:10]

    context = {
        'news_list': posts,
        'best_blogs': best_blogs,
        'best_bloggers': best_bloggers,
    }

    return render(request, 'Random/random.html', context)

def send_feedback(subject, from_email, to, username, email, text):
    text_content = ''
    html_content = """
    <div>
        <h1>Получен feedback!</h1>
    </div>
    <div>
        <h1>От кого: """ + username + """</h1>
        <h1>Email: """ + email + """</h1>
        <h1>Feedback: </h1><p>""" + text + """</p>
    </div>
    """
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()

def send_feedback_success(subject, from_email, to, text):
    text_content = ''
    html_content = """
    <div>
        <h1>Запрос в техническую поддержку.</h1>
    </div>
    <div>
        <p>
            Ваш запрос в тех. поддержку успешно отправлен, мы скоро вам ответим!
            Ваш вопрос: """ + text + """
        </p>
        <p>Если вы не отпраляли запрос, то просто проигнорируйте это письмо.</p>
        <p>С уважением,<br>
        Команда Jimbl.</p>
        <p>­­© 2018. Jimbl. Все права защищены.</p>
    </div>
    """
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()

def index(request):

    if request.is_ajax():

        if(request.POST.get('render_comments') == '1'):
            post = Post.objects.get(id = request.POST.get('post_id'))

            template = loader.get_template('App/include/comments/simple_comment.html')

            context = {
                'comments_list': post.comments.all().order_by('-date')[:int(request.POST.get('now_cnt_comments'))],
                'now_post': post,
            }

            resp = template.render(context, request)
            return HttpResponse(resp)

        if(request.POST.get('load_more_comments') == '1'):
            post = Post.objects.get(id = request.POST.get('post_id'))

            template = loader.get_template('App/include/comments/simple_comment.html')

            if(int(request.POST.get('now_cnt_comments')) != post.comments.count()):
                context = {
                    'comments_list': post.comments.all().order_by('-date')[:int(request.POST.get('now_cnt_comments')) + 22],
                    'now_post': post,
                }
            else:
                context = {
                    'comments_list': post.comments.all().order_by('-date')[:int(request.POST.get('now_cnt_comments'))],
                    'now_post': post,
                    'full_comments_list': 1,
                }
            resp = template.render(context, request)
            return HttpResponse(resp)

        if(request.POST.get('hide_more_comments') == '1'):
            post = Post.objects.get(id = request.POST.get('post_id'))

            template = loader.get_template('App/include/comments/simple_comment.html')
            context = {
                'comments_list': post.comments.all().order_by('-date')[:3],
                'now_post': post,
                'hide_more_comments_status': 1,
            }
            resp = template.render(context, request)
            return HttpResponse(resp)


        if(request.POST.get('create_post_status') == '1'):
            title = request.POST.get('title')
            discription = request.POST.get('discription')
            cateregory =  request.POST.get('cateregory_id')
            html_data = request.POST.get('html_data')

            try:
                poster = request.FILES['image']
            except Exception:
                poster = False
            if(poster):
                if(discription != ''):
                    post = Post.objects.create(user = request.user, title = title, cateregory_id = cateregory, discription = discription, image = poster, html_data = html_data)
                else:
                    post = Post.objects.create(user = request.user, title = title, cateregory_id = cateregory, image = poster, html_data = html_data)
                post.save()
                template = loader.get_template('App/include/post_card.html')
                context = {
                    'news_list': Post.objects.all().order_by('-date')[:22]
                }
                resp = template.render(context, request)
                return HttpResponse(resp)
            else:
                template = loader.get_template('App/include/post_card.html')
                context = {
                    'news_list': Post.objects.all().order_by('-date')[:22]
                }
                resp = template.render(context, request)
                return HttpResponse(resp)

        if(request.POST.get('feedback') == '1'):
            if(auth.get_user(request).is_authenticated):
                send_feedback('Получен feedback!', 'Jimbl', 'jimbl.service@gmail.com', request.user.username, request.POST.get('feedback_email'), request.POST.get('feedback_area'))
                send_feedback_success('Запрос в тех. поддержку', 'Jimbl', request.POST.get('feedback_email'), request.POST.get('feedback_area'))

        if(request.POST.get('load_content') == '1'):
            template = loader.get_template('App/include/post_card.html')
            context = {
                'news_list': Post.objects.all().order_by('-rate')[:int(request.POST.get('count_post')) + 22]
            }
            resp = template.render(context, request)
            return HttpResponse(resp)


        if(request.POST.get('share_post') == '1'):
            post_id = request.POST.get('post_id')
            post = Post.objects.get(id = post_id)
            user = request.user

            if not user in post.share.all():
                repost = Repost.objects.create(user = user, post = post)
                post.share.add(user)
                repost.save()

            return HttpResponse('')

        if(request.POST.get('add_search_user') == '1'):
            search_user_username = str(request.POST.get('search_user'))
            search_user = User.objects.get(username = search_user_username)
            user = request.user

            user.profile.last_search_user.add(search_user)
            user.save()

            return HttpResponse('ok')

        if(request.POST.get('is_search') == '1'):
            user = request.user
            last_search_users = user.profile.last_search_user.all()[:5]
            search = request.POST.get('search')
            search_blogers = []
            search_posts = []
            users = Profile.objects.all().order_by('-views')
            posts = Post.objects.all().order_by('-views')

            for user in users:
                if(str(search) in str(user.user.username) and search != ''):
                    search_blogers.append(user)

            for post in posts:
                if(str(search) in str(post.title) and search != ''):
                    search_posts.append(post)

            if(request.POST.get('is_mobile') == '1'):
                template = loader.get_template('Search/mobile/search-result.html')
            else:
                template = loader.get_template('App/include/search.html')

            context = {
                'search': search,
                'search_blogers': search_blogers[:5],
                'last_search_users': last_search_users,
            }

            resp = template.render(context, request)
            return HttpResponse(resp)

        if(request.POST.get('write_comment') == '1'):

            post = Post.objects.get(id = request.POST.get('post_id'))

            if(not request.POST.get('main_data') or request.POST.get('main_data') == ''):
                return False

            comment_now = Comment.objects.create(user = request.user, data = request.POST.get('main_data'))
            post.comments.add(comment_now)
            post.save()

            update_month_activites_rate(post.user)

            template = loader.get_template('App/include/comments/simple_comment.html')

            if int(request.POST.get('now_cnt_comments')) < 3:
                to = 3
            else:
                to = int(request.POST.get('now_cnt_comments'))

            c = {
                'comments_list': post.comments.all().order_by('-date')[:to],
                'now_post': post,
            }

            resp = template.render(c, request)
            return HttpResponse(resp)

    if request.method == 'POST':
        post_form = PostForm(request.POST, request.FILES)
        if post_form.is_valid():
            post_form.save()
            #may_like
            user = request.user
            all_posts_user = Post.objects.filter(user = user).all()
            rate = []
            for i in range(1, 10):
                rate.append(Post.objects.filter(user = user, cateregory_id = i).all().count())
            user.profile.more_post_cat_id = rate.index(max(rate)) + 1
            may_like = ''
            for i in range(0, 3):
                cateregory = rate.index(max(rate))
                may_like += str(cateregory + 1) + ','
                rate[cateregory] = 0
            user.profile.may_like = may_like
            user.save()


    posts = Post.objects.all().order_by('-rate')[:22]
    best_bloggers = Profile.objects.all().order_by('-month_rate')[:3]

    best_blogs = Blog.objects.all().order_by('-rate')[:10]

    context = {
        'search': '',
        'news_list': posts,
        'best_blogs': best_blogs,
        'now_date': datetime.date.today(),
        'best_bloggers': best_bloggers,
        'page': '1',
    }
    return render(request, 'App/index.html', context)

def welcome(request):
    return render(request, 'welcome/index.html')

def blogs(request, username):
    try:
        view_user = User.objects.filter(username = username).get()
    except Exception:
        return render(request, 'Errors/404.html')

    if request.is_ajax():
        if(request.POST.get('delete_blog') == '1'):
            blog = Blog.objects.get(id = request.POST.get('blog_id'))
            if(request.user == blog.user):
                blog.delete()

                template = loader.get_template('blogs/include/blog_template.html')
                context = {
                    'blogs': Blog.objects.filter(user = view_user).all()[:int(request.POST.get('count_blogs')) + 1],
                    'page': 'blogs',
                }
                resp = template.render(context, request)
                return HttpResponse(resp)

        if(request.POST.get('create_blog') == '1'):
            if(request.user == view_user):
                title = request.POST.get('title_blog')
                discription = request.POST.get('discription')
                user = request.user

                try:
                    image = request.FILES['image_blog']
                except Exception:
                    image = False

                if(image):
                    blog = Blog.objects.create(user = user, name = title, discription = discription, image = image)
                    blog.save()
                    template = loader.get_template('blogs/include/blog_template.html')
                    context = {
                        'blogs': Blog.objects.filter(user = user).all()[:int(request.POST.get('count_blogs')) + 1],
                        'page': 'blogs',
                    }
                    resp = template.render(context, request)
                    return HttpResponse(resp)

        if(request.POST.get('load_content') == '1'):
            template = loader.get_template('blogs/include/blog_template.html')
            context = {
                'blogs': Blog.objects.filter(user = view_user).all()[:int(request.POST.get('count_post')) + 12],
                'page': 'blogs',
                'user': request.user,
                'profile_view': view_user,
            }
            resp = template.render(context, request)
            return HttpResponse(resp)

    media_list = []

    for media in Image_upload.objects.filter(user = view_user).all():
        media_list.append(media)

    for media in Video_upload.objects.filter(user = view_user).all():
        media_list.append(media)

    media_list = sorted(media_list, key=lambda x: x.date, reverse=True)

    blogs = Blog.objects.filter(user = view_user).all()[:12]

    recomended_profiles = Profile.objects.filter(more_post_cat_id = view_user.profile.more_post_cat_id).all().order_by('-month_rate')[:6]
    recomended_profiles_list = []

    for recomended_profile in recomended_profiles:
        if(recomended_profile.user != request.user):
            recomended_profiles_list.append(recomended_profile)

    if(len(recomended_profiles_list) == 0):
        recomended_profiles = Profile.objects.all().order_by('-month_rate')[:6]
        for recomended_profile in recomended_profiles:
            recomended_profiles_list.append(recomended_profile)

    context = {
        'profile_view': view_user,
        'blogs_cnt': Blog.objects.filter(user = view_user).count(),
        'posts_cnt': Post.objects.filter(user = view_user).count(),
        'media_list': media_list[:9],
        'recomended_profiles': recomended_profiles_list,
        'blogs': blogs,
        'page': 'blogs',
        'user': request.user,
    }

    return render(request, 'blogs/blogs.html', context)

def blogs_view(request, username, id):
    try:
        view_user = User.objects.filter(username = username).get()
        blog = Blog.objects.filter(id = id).get()
    except Exception:
        return render(request, 'Errors/404.html')

    if request.is_ajax():
        if(request.POST.get('delete_post_from_blog') == '1'):
            blog = Blog.objects.filter(id = request.POST.get('blog_id')).get()
            if(request.user == view_user and request.user == blog.user):
                post = Post.objects.filter(id = request.POST.get('post_id')).get()

                if(post in blog.posts.all()):
                    blog.posts.remove(post)
                    blog.save()
                    template = loader.get_template('blogs/include/post_view.html')
                    context = {
                        'blog_posts': blog.posts.all()[:int(request.POST.get('count_post'))],
                        'blog': Blog.objects.filter(id = request.POST.get('blog_id')).get(),
                        'page': 'blogs',
                    }
                    resp = template.render(context, request)
                    return HttpResponse(resp)

        if(request.POST.get('add_post_to_blog') == '1'):
            if(request.user == view_user):
                post = Post.objects.filter(id = request.POST.get('post_id')).get()
                blog = Blog.objects.filter(id = request.POST.get('blog_id')).get()

                if(not post in blog.posts.all()):
                    blog.posts.add(post)
                    blog.save()
                    template = loader.get_template('blogs/include/post_view.html')
                    context = {
                        'blog_posts': blog.posts.all()[:int(request.POST.get('count_post')) + 1],
                        'blog': Blog.objects.filter(id = request.POST.get('blog_id')).get(),
                        'page': 'blogs',
                    }
                    resp = template.render(context, request)
                    return HttpResponse(resp)
                else:
                    template = loader.get_template('blogs/include/post_view.html')
                    context = {
                        'blog_posts': blog.posts.all()[:int(request.POST.get('count_post'))],
                        'blog': Blog.objects.filter(id = request.POST.get('blog_id')).get(),
                        'page': 'blogs',
                    }
                    resp = template.render(context, request)
                    return HttpResponse(resp)


        if(request.POST.get('load_content') == '1'):
            template = loader.get_template('blogs/include/post_view.html')
            context = {
                'blog_posts': blog.posts.all()[:int(request.POST.get('count_post')) + 22],
                'blog': Blog.objects.filter(id = id).get(),
                'page': 'blogs',
            }
            resp = template.render(context, request)
            return HttpResponse(resp)

    posts = blog.posts.all()[:22]

    update_rating_blog(blog.id)
    media_list = []

    recomended_profiles = Profile.objects.filter(more_post_cat_id = view_user.profile.more_post_cat_id).all().order_by('-month_rate')[:6]
    recomended_profiles_list = []

    for recomended_profile in recomended_profiles:
        if(recomended_profile.user != request.user):
            recomended_profiles_list.append(recomended_profile)

    if(len(recomended_profiles_list) == 0):
        recomended_profiles = Profile.objects.all().order_by('-month_rate')[:6]
        for recomended_profile in recomended_profiles:
            recomended_profiles_list.append(recomended_profile)

    for media in Image_upload.objects.filter(user = view_user).all():
        media_list.append(media)

    for media in Video_upload.objects.filter(user = view_user).all():
        media_list.append(media)

    media_list = sorted(media_list, key=lambda x: x.date, reverse=True)

    context = {
        'profile_view': view_user,
        'blogs_cnt': Blog.objects.filter(user = view_user).count(),
        'user_posts': Post.objects.filter(user = view_user).all(),
        'posts_cnt': Post.objects.filter(user = view_user).count(),
        'media_list':  media_list[:9],
        'recomended_profiles': recomended_profiles_list,
        'blog_posts': posts,
        'blog': blog,
        'page': 'blogs',
    }

    return render(request, 'blogs/include/blog_view.html', context)

def get_media_user(user):
    media_list = []

    for media in Image_upload.objects.filter(user = user).all():
        media_list.append(media)

    for media in Video_upload.objects.filter(user = user).all():
        media_list.append(media)

    media_list = sorted(media_list, key=lambda x: x.date, reverse=True)
    return media_list

def media_profile(request, username):

    try:
        view_user = User.objects.filter(username = username).get()
    except Exception:
        return render(request, 'Errors/404.html')

    recomended_profiles = Profile.objects.filter(more_post_cat_id = view_user.profile.more_post_cat_id).all().order_by('-month_rate')[:6]
    recomended_profiles_list = []

    for recomended_profile in recomended_profiles:
        if(recomended_profile.user != request.user):
            recomended_profiles_list.append(recomended_profile)

    if(len(recomended_profiles_list) == 0):
        recomended_profiles = Profile.objects.all().order_by('-month_rate')[:6]
        for recomended_profile in recomended_profiles:
            recomended_profiles_list.append(recomended_profile)

    if request.is_ajax():
        if(request.POST.get('like') == '1'):
            if(request.POST.get('this_image') == '1'):
                post = Image_upload.objects.get(id = request.POST.get('post_id'))
            else:
                post = Video_upload.objects.get(id = request.POST.get('post_id'))
            user = request.user

            if (user in post.likes.all()):
                post.likes.remove(user)
                user.profile.liked_posts.remove(post)
            else:
                post.likes.add(user)
                user.profile.liked_posts.add(post)

            post.save()
            user.save()


        if(request.POST.get('load_content') == '1'):
            template = loader.get_template('profile/media/include/media_template.html')
            context = {
                'media_list': get_media_user(view_user)[:int(request.POST.get('count_post')) + 22],
                'profile_view': view_user,
                'user': request.user,
                'page': 'media',
            }
            resp = template.render(context, request)
            return HttpResponse(resp)

    context = {
        'user': request.user,
        'profile_view': view_user,
        'blogs_cnt': Blog.objects.filter(user = view_user).count(),
        'posts_cnt': Post.objects.filter(user = view_user).count(),
        'media_list': get_media_user(view_user),
        'media_count': len(get_media_user(view_user)),
        'recomended_profiles': recomended_profiles_list,
        'page': 'media',
    }
    return render(request, 'profile/media/index.html', context)

def profile_news(request, view_user, n):
    posts = []
    reposts = []

    for post in Post.objects.filter(user = view_user).all().order_by('-date'):
        posts.append(post)

    for repost in Repost.objects.filter(user = view_user).all().order_by('-date'):
        reposts.append(repost)

    posts.extend(reposts)

    posts = sorted(posts, key=lambda x: x.date, reverse=True)
    return posts[:n]

def profile_view(request, username):

    load_post = 20

    try:
        view_user = User.objects.get(username=username)
    except User.DoesNotExist:
        return render(request, 'Errors/404.html')

    media_list = []

    if(request.user.is_authenticated):
        if(request.user != view_user):
            if(request.user not in view_user.profile.related_views.all()):
                view_user.profile.related_views.add(request.user)

    if(request.user == view_user):
        posts = Post.objects.filter(user = view_user).all()

        cateregory_max_list = []

        for post in posts:
            cateregory_max_list.append(int(post.cateregory_id))

        if len(cateregory_max_list) == 0:
            cateregory_max_list.append(randint(1, 10))

        view_user.profile.more_post_cat_id = max(cateregory_max_list,key=cateregory_max_list.count)
        view_user.save()

    recomended_profiles = Profile.objects.filter(more_post_cat_id = view_user.profile.more_post_cat_id).all().order_by('-month_rate')[:6]
    recomended_profiles_list = []

    for recomended_profile in recomended_profiles:
        if(recomended_profile.user != request.user):
            recomended_profiles_list.append(recomended_profile)

    if(len(recomended_profiles_list) == 0):
        recomended_profiles = Profile.objects.all().order_by('-month_rate')[:6]
        for recomended_profile in recomended_profiles:
            recomended_profiles_list.append(recomended_profile)

    for media in Image_upload.objects.filter(user = view_user).all():
        media_list.append(media)

    for media in Video_upload.objects.filter(user = view_user).all():
        media_list.append(media)

    media_list = sorted(media_list, key=lambda x: x.date, reverse=True)

    cnt_view = view_user.profile.related_views.count()

    if request.is_ajax():
        if(request.POST.get('delete_repost') == '1'):
            repost = Repost.objects.get(id = request.POST.get('repost_id'))

            if repost.user == request.user:
                repost.delete()
            else:
                return False

            template = loader.get_template('profile/include/post_card.html')
            context = {
                'posts':  profile_news(request, view_user , int(request.POST.get('count_post'))),
                'page': 'lenta',
            }

            resp = template.render(context, request)
            return HttpResponse(resp)



        if(request.POST.get('set_avatar_by_url') == '1'):
            user = request.user
            url = request.POST.get('image_url')

            if(url):
                user.profile.image_profile = url
                user.save()

            return HttpResponse('')

        if(request.POST.get('render_comments') == '1'):
            post = Post.objects.get(id = request.POST.get('post_id'))

            template = loader.get_template('profile/include/comments/simple_comment.html')

            context = {
                'comments_list': post.comments.all().order_by('-date')[:int(request.POST.get('now_cnt_comments'))],
                'now_post': post,
            }

            resp = template.render(context, request)
            return HttpResponse(resp)

        if(request.POST.get('load_more_comments') == '1'):
            post = Post.objects.get(id = request.POST.get('post_id'))

            template = loader.get_template('profile/include/comments/simple_comment.html')

            if(int(request.POST.get('now_cnt_comments')) / 2 < post.comments.count()):
                context = {
                    'comments_list': post.comments.all().order_by('-date')[:int(request.POST.get('now_cnt_comments')) + 22],
                    'now_post': post,
                }
            else:
                context = {
                    'comments_list': post.comments.all().order_by('-date')[:int(request.POST.get('now_cnt_comments'))],
                    'now_post': post,
                    'full_comments_list': 1,
                }
            resp = template.render(context, request)
            return HttpResponse(resp)

        if(request.POST.get('hide_more_comments') == '1'):
            post = Post.objects.get(id = request.POST.get('post_id'))

            template = loader.get_template('profile/include/comments/simple_comment.html')
            context = {
                'comments_list': post.comments.all().order_by('-date')[:3],
                'now_post': post,
                'hide_more_comments_status': 1,
            }
            resp = template.render(context, request)
            return HttpResponse(resp)

        if(request.POST.get('report_post') == '1'):
            post = Post.objects.get(id = request.POST.get('report_post_id'))

            report_reason = request.POST.get('report_data')

            report = Report.objects.create(user = request.user, post = post, reason = report_reason)
            report.save()

            return HttpResponse('')

        if(request.POST.get('report_blog') == '1'):
            blog = Blog.objects.get(id = request.POST.get('report_blog_id'))

            report_reason = request.POST.get('report_data')

            report = Report.objects.create(user = request.user, blog = blog, reason = report_reason)
            report.save()

            return HttpResponse('')


        if(request.POST.get('set_avatar') == '1'):
            avatar = request.FILES['load_avatar']

            if (avatar):
                user = request.user
                user.profile.image_profile = avatar
                user.save()
                image = Image_upload.objects.create(user = user, image = avatar)
                image.save()
                template = loader.get_template('profile/include/avatar.html')
                context = {
                    'profile_view': user,
                    'page': 'lenta',
                }
                resp = template.render(context, request)
                return HttpResponse(resp)

        if(request.POST.get('share_post') == '1'):
            post_id = request.POST.get('post_id')
            post = Post.objects.get(id = post_id)
            user = request.user

            if(user in post.share.all):
                return False

            repost = Repost.objects.create(user = user, post = post)

            repost.save()

            template = loader.get_template('profile/include/post_card.html')
            context = {
                'user': user,
                'posts':  profile_news(request, view_user , int(request.POST.get('count_post'))),
                'page': 'lenta',
            }

            resp = template.render(context, request)
            return HttpResponse(resp)

        if(request.POST.get('load_content') == '1'):
            template = loader.get_template('profile/include/post_card.html')
            context = {
                'user': request.user,
                'profile_view': view_user,
                'posts': profile_news(request, view_user , int(request.POST.get('count_post')) + 22),
                'page': 'lenta',
            }
            resp = template.render(context, request)
            return HttpResponse(resp)

    context = {
        'user': request.user,
        'profile_view': view_user,
        'posts':profile_news(request, view_user , 22),
        'blogs_cnt': Blog.objects.filter(user = view_user).count(),
        'posts_cnt': Post.objects.filter(user = view_user).count(),
        'read': view_user.profile.read.all(),
        'readers': view_user.profile.readers.all(),
        'media_list': media_list[:9],
        'media_count': len(media_list),
        'cnt_view': cnt_view,
        'recomended_profiles': recomended_profiles_list,
        'page': 'lenta',
    }

    if request.is_ajax():

        if(request.POST.get('delete_post') == '1'):
            post_id = request.POST.get('post_id')
            post = Post.objects.get(id = post_id)

            if post.user != request.user:
                return False

            post.delete()

            template = loader.get_template('profile/include/post_card.html')
            context = {
                'posts':  profile_news(request, view_user , int(request.POST.get('count_post'))),
                'page': 'lenta',
            }

            resp = template.render(context, request)
            return HttpResponse(resp)


        if(request.POST.get('add_comment')):

            post_id = request.POST.get('post_id')
            post = Post.objects.get(id = post_id)
            user = request.user
            data = request.POST.get('comment_data_' + post_id)

            comment_now = Comment.objects.create(user = user, data = data)
            post.comments.add(comment_now)
            if user not in post.views.all():
                post.views.add(user)
                post.save()
            post.save()
            update_rating_post(request.POST.get('post_id'))
            update_month_rate(post.user.id)

            template = loader.get_template('profile/include/comments/simple_comment.html')

            c = {
                'user': user,
                'post_id': post_id,
                'comments' : post.comments.all().order_by('-date'),
                'page': 'lenta',
            }

            resp = template.render(c, request)
            return HttpResponse(resp)


        if(request.POST.get('liking')):
            post = Post.objects.get(id = request.POST.get('post_id'))
            user = request.user

            if (user in post.likes.all()):
                post.likes.remove(user)
                user.profile.liked_posts.remove(post)
            else:
                post.likes.add(user)
                user.profile.liked_posts.add(post)

            if user not in post.views.all():
                post.views.add(user)

            post.save()
            user.save()

            update_rating_post(request.POST.get('post_id'))
            update_month_rate(post.user.id)
            update_month_activites_rate(post.user)

        if(request.POST.get('share_post')):
            post_id = request.POST.get('post_id')
            post = Post.objects.get(id = post_id)
            user = request.user
            if user not in post.views.all():
                post.views.add(user)
                post.save()

            if user not in post.share.all():
                post.share.add(user)
                repost = Repost.objects.create(user = user, post = post)
                repost.save()
                post.save()
            else:
                template = loader.get_template('profile/include/media_card.html')

                c = {
                    'user': user,
                    'posts': Post.objects.filter(user = view_user).all().order_by('-date')[:load_post],
                    'page': 'lenta',
                }

                resp = template.render(c, request)
                return HttpResponse(resp)

            update_rating_post(request.POST.get('post_id'))
            update_month_rate(post.user.id)
            update_month_activites_rate(post.user)



            template = loader.get_template('profile/include/media_card.html')

            c = {
                'user': user,
                'posts': Post.objects.filter(user = view_user).all().order_by('-date')[:load_post],
                'page': 'lenta',
            }

            resp = template.render(c, request)
            return HttpResponse(resp)

        if(request.POST.get('liking_comment')):
            comment = Comment.objects.get(id = request.POST.get('comment_id'))
            user = request.user

            if (user in comment.likes.all()):
                comment.likes.remove(user)
                user.profile.liked_comments.remove(comment)
            else:
                comment.likes.add(user)
                user.profile.liked_comments.add(comment)

            comment.save()
            user.save()

        if(request.POST.get('common_follow') == '1'):
            now_user = request.user
            follow_user = User.objects.get(id = request.POST.get('user_id'))

            if now_user in follow_user.profile.readers.all():
                follow_user.profile.readers.remove(now_user)
                now_user.profile.read.remove(follow_user)
            else:
                follow_user.profile.readers.add(now_user)
                now_user.profile.read.add(follow_user)

                now_user.marker.add_follower = True
                now_user.marker.save()

            now_user.save()
            follow_user.save()
            print(2)


        if(request.POST.get('follow')):
            now_user = request.user
            readers_list = view_user.profile.readers.all()

            if now_user in readers_list:
                view_user.profile.readers.remove(now_user)
                now_user.profile.read.remove(view_user)
            else:
                view_user.profile.readers.add(now_user)
                now_user.profile.read.add(view_user)

                now_user.marker.add_follower = True
                now_user.marker.save()

            view_user.save()
            now_user.save()

    return render(request, 'profile/index.html', context)

def newest(request):
    if request.is_ajax():
        if(request.POST.get('load_content') == '1'):
            template = loader.get_template('App/include/post_card.html')
            context = {
                'news_list': Post.objects.all().order_by('-date')[:int(request.POST.get('count_post')) + 22]
            }
            resp = template.render(context, request)
            return HttpResponse(resp)

    posts = Post.objects.all().order_by('-date')[:20]

    best_bloggers = Profile.objects.all().order_by('-views')[:3]
    best_blogs = Blog.objects.all().order_by('-rate')[:10]

    context = {
        'news_list': posts,
        'best_blogs': best_blogs,
        'best_bloggers': best_bloggers,
    }

    return render(request, 'Newest/index.html', context)


def update_month_activites_rate(user):
    profile = user.profile
    posts = Post.objects.filter(user = user).all()

    now_month = int(datetime.date.today().strftime('%m'))
    now_year = int(datetime.date.today().strftime('%Y'))

    cnt_view = 0
    like_cnt = 0
    comments_cnt = 0
    reposts_cnt = 0
    rate_cnt = 0
    for post in posts:
        cnt_view += post.views.count()
        like_cnt += post.likes.count()
        comments_cnt += post.comments.count()
        reposts_cnt += post.share.count()
        rate_cnt += post.rate

    try:
        rate_cnt = int(rate_cnt / posts.count())
    except ZeroDivisionError:
        rate_cnt = 0

    try:
        month_activites_rating = month_activites_rate.objects.get(user = user, month = now_month, year = now_year)
    except Exception:
        month_activites_rating = month_activites_rate.objects.create(user = user, month = now_month, year = now_year)

    try:
        month_activites_views = month_activites_rating.views.split(',')
    except Exception:
        month_activites_views = []
        for i in range(0, calendar.mdays[datetime.date.today().month]):
            month_activites_views.append(0)

    try:
        month_activites_likes = month_activites_rating.likes.split(',')
    except Exception:
        month_activites_likes = []
        for i in range(0, calendar.mdays[datetime.date.today().month]):
            month_activites_likes.append(0)

    try:
        month_activites_comments = month_activites_rating.comments.split(',')
    except Exception:
        month_activites_comments = []
        for i in range(0, calendar.mdays[datetime.date.today().month]):
            month_activites_comments.append(0)

    try:
        month_activites_reposts = month_activites_rating.reposts.split(',')
    except Exception:
        month_activites_reposts = []
        for i in range(0, calendar.mdays[datetime.date.today().month]):
            month_activites_reposts.append(0)

    try:
        month_activites_epr = month_activites_rating.rating.split(',')
    except Exception:
        month_activites_epr = []
        for i in range(0, calendar.mdays[datetime.date.today().month]):
            month_activites_epr.append(0)

    month_activites_views.insert(int(datetime.date.today().strftime('%d')), cnt_view)
    month_activites_views.pop(int(datetime.date.today().strftime('%d')) - 1)

    month_activites_likes.insert(int(datetime.date.today().strftime('%d')), like_cnt)
    month_activites_likes.pop(int(datetime.date.today().strftime('%d')) - 1)

    month_activites_comments.insert(int(datetime.date.today().strftime('%d')), comments_cnt)
    month_activites_comments.pop(int(datetime.date.today().strftime('%d')) - 1)

    month_activites_reposts.insert(int(datetime.date.today().strftime('%d')), reposts_cnt)
    month_activites_reposts.pop(int(datetime.date.today().strftime('%d')) - 1)

    month_activites_epr.insert(int(datetime.date.today().strftime('%d')), rate_cnt)
    month_activites_epr.pop(int(datetime.date.today().strftime('%d')) - 1)

    month_activites_views_string = ''
    month_activites_likes_string = ''
    month_activites_comments_string = ''
    month_activites_reposts_string = ''
    month_activites_epr_string = ''

    for i in month_activites_views:
        month_activites_views_string += str(i) + ','

    for i in month_activites_likes:
        month_activites_likes_string += str(i) + ','

    for i in month_activites_comments:
        month_activites_comments_string += str(i) + ','

    for i in month_activites_reposts:
        month_activites_reposts_string += str(i) + ','

    for i in month_activites_epr:
        month_activites_epr_string += str(i) + ','

    month_activites_rating.views = month_activites_views_string[:len(month_activites_views_string) - 1]
    month_activites_rating.likes = month_activites_likes_string [:len(month_activites_likes_string ) - 1]
    month_activites_rating.comments = month_activites_comments_string[:len(month_activites_comments_string) - 1]
    month_activites_rating.reposts = month_activites_reposts_string[:len(month_activites_reposts_string) - 1]
    month_activites_rating.rating = month_activites_epr_string[:len(month_activites_epr_string) - 1]

    month_activites_rating.save()

def update_month_egagement_rate(user):
    profile = user.profile
    posts = Post.objects.filter(user = user).all()
    cnt_view = 0
    cnt_readers = profile.readers.count()

    for post in posts:
        cnt_view += post.views.count()

    now_month = int(datetime.date.today().strftime('%m'))
    now_year = int(datetime.date.today().strftime('%Y'))

    try:
        month_egegament_rating = month_egegament_rate.objects.get(user = user, month = now_month, year = now_year)
    except Exception:
        month_egegament_rating = month_egegament_rate.objects.create(user = user, month = now_month, year = now_year)

    try:
        month_egegament_views = month_egegament_rating.views.split(',')
    except Exception:
        month_egegament_views = []
        for i in range(0, calendar.mdays[datetime.date.today().month]):
            month_egegament_views.append(0)

    try:
        month_egegament_readers = month_egegament_rating.readers.split(',')
    except Exception:
        month_egegament_readers = []
        for i in range(0, calendar.mdays[datetime.date.today().month]):
            month_egegament_readers.append(0)

    month_egegament_views.insert(int(datetime.date.today().strftime('%d')), cnt_view)
    month_egegament_views.pop(int(datetime.date.today().strftime('%d')) - 1)

    month_egegament_readers.insert(int(datetime.date.today().strftime('%d')), cnt_readers)
    month_egegament_readers.pop(int(datetime.date.today().strftime('%d')) - 1)

    month_egegament_views_string = ''
    month_egegament_readers_string = ''
    for i in month_egegament_views:
        month_egegament_views_string += str(i) + ','
    for i in month_egegament_readers:
        month_egegament_readers_string += str(i) + ','

    month_egegament_rating.views = month_egegament_views_string[:len(month_egegament_views_string) - 1]
    month_egegament_rating.readers = month_egegament_readers_string[:len(month_egegament_readers_string) - 1]

    month_egegament_rating.save()


def post_view(request, username, id):
    try:
        view_user = User.objects.get(username = username)
    except Exception:
        return render(request, 'Errors/404.html')

    if auth.get_user(request).is_authenticated:
        user = request.user
    post = Post.objects.get(user = view_user, id = id)

    if auth.get_user(request).is_authenticated:
        if (user not in post.views.all()):
            post.views.add(user)
            post.save()
            update_month_egagement_rate(post.user)
            update_month_activites_rate(post.user)


    context = {
        'post': post
    }

    return render(request, 'post/post.html', context)

def logout_view(request):
    logout(request)
    return redirect('/accounts/login/')
