from django.urls import path, include
from django.conf.urls import url
from . import views
from welcome import views as welcome_views

urlpatterns = [
    path('', views.index, name="index"),  #main news page
    path('welcome/', welcome_views.index, name="welcome"),
    path('logout/', views.logout_view),
    path('creator/', include('creator.urls'), name="creator"),
    path('newest/', views.newest, name="newest"),
    path('random/', views.random, name="random"),
    path('featured/', views.featured, name="featured"),
    path('interesting/', views.interesting, name="interesting"),
    path('edit-profile/', include('edit_profile.urls')),
    path('registration/', views.register, name="register"),
    path('terms/', views.terms, name="terms"),
    path('licence/', views.licence, name="licence"),
    path('feed/', views.feed, name="feed"),
    #path('search/', views.mobile_search, name="mobile_search"),
    path('chat/', include('Messenger.urls'), name="chat"),
    path('help/', include('Help.urls'), name="help"),
    path('statistic/', include('statistic.urls'), name="statistic"),
    url(r'^(?P<cateregory>\w+)/$', views.show_cateregory, name='show_cateregory'),
]
