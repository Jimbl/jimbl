from django import forms
from .models import Blog, Post, Profile

class BlogForm(forms.ModelForm):
    class Meta:
        model = Blog
        fields = ('name', 'user', 'discription', 'image')

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('user', 'title', 'image', 'discription', 'simple_data', 'html_data', 'cateregory_id')

class Avatar_Upload_Form(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('image_profile',)
