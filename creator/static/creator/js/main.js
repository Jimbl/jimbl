
$.ajaxSetup({
 beforeSend: function(xhr, settings) {
     function getCookie(name) {
         var cookieValue = null;
         if (document.cookie && document.cookie != '') {
             var cookies = document.cookie.split(';');
             for (var i = 0; i < cookies.length; i++) {
                 var cookie = jQuery.trim(cookies[i]);
                 // Does this cookie string begin with the name we want?
             if (cookie.substring(0, name.length + 1) == (name + '=')) {
                 cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                 break;
             }
         }
     }
     return cookieValue;
     }
     if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
         // Only send the token to relative URLs i.e. locally.
         xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
     }
 }
});

function load(){
  if($(window).width() < 1200){
    $('#post-mobile').html('<main class="pt-5 mx-lg-5 w-100 h-fill"><div class="container-fluid mt-5 w-100 h-fill"><table class="w-100 h-fill"><tr><td class="align-middle" align="center">'+
              '<div class="d-flex justify-content-center"><div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status"><span class="sr-only">Loading...</span></div>'+
              '</div></td></tr></table></div></main>')
  }
  else{
    $('#desktop').html('<main class="pt-5 mx-lg-5 w-100 h-fill"><div class="container-fluid mt-5 w-100 h-fill"><table class="w-100 h-fill"><tr><td class="align-middle" align="center">'+
              '<div class="d-flex justify-content-center"><div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status"><span class="sr-only">Loading...</span></div>'+
              '</div></td></tr></table></div></main>')
  }

  $('#mobile-navbar').addClass('hidden')
  $('#mobile-adder').addClass('hidden')
  $('#bottom-menu').addClass('hidden')
}

function get_started(){
  add_main_header();
  add_paragraph('Начните писать ваш пост здесь...');
  $('#main_header').focus()
}

$(document).ready(function(){
  sort_table();

  $('.dropdown-menu').removeClass('show');
  if(sessionStorage.getItem('post') === '' || sessionStorage.getItem('post') === null){
    get_started();
  }
  if($(window).width() < 1200){
    $('.main-body-post').css('min-height', $(window).height() - 168 + 'px');
  }
  if($('#id_video_preview').attr('src') === ''){
    $('#id_video_preview').parent().addClass('hidden')
  }
  if($(window).width() < 600){
    width = $(window).width()
    $('#preview-title').css('max-width', width - 100)
  }
});

$('#id_title').on('input', function(){
  title = $('#id_title').val()
  $('#preview-title').text(title);
  $('#main_header').val(title);
  document.title = title;
})

function get_html_post(){
  return $('#post').html();
}

$('.media').on('click', function(){
  if($(this).hasClass('select')){
      $(this).removeClass('select');
  }
  else{
    $('.media').removeClass('select');
    $(this).addClass('select');
  }
});

function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#user_media').addClass('hidden');
      $('#img-preview').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

function readURLposter(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#poster_preview').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#id_image").change(function() {
  readURLposter(this);
});

function create_post(){
  if($('#id_image').val() === ''){
    $('#empty_poster_modal').modal('show');
    return false;
  }
  else if($('#id_title').val() === ''){
    $('#empty_title_modal').modal('show');
    return false;
  }
  else{
    $('textarea.input').each(function() {
        $(this).height($(this).prop('scrollHeight'));
    });
    save_post()

    var data = new FormData($('#create_post_form').get(0));

    load()
    $('#create_post').addClass('slideOutRight')

    $.ajax({
        url: '/',
        type: 'POST',
        data: data,
        cache: false,
        processData: false,
        contentType: false,
        success: function(data) {
          sessionStorage.removeItem('post');
          document.location.href='/feed';
        },
        error: function(data){
          console.log('error');
        }
    });
    return false;
  }
}

$(document).on("change", "#id_load_video", function(evt) {
  var $source = $('#id_video_preview');
  $source[0].src = URL.createObjectURL(this.files[0]);
  $source.parent()[0].load();
  $('#id_html_video').addClass('hidden');
  $('#devider').addClass('hidden');
  $('#id_video_preview').parent().removeClass('hidden')
});

$("#id_load_image").change(function() {
  readURL(this);
});

function update_inner_post(){
  sort_table();
  jQuery.each(jQuery('textarea[data-autoresize]'), function() {
      var offset = this.offsetHeight - this.clientHeight;

      var resizeTextarea = function(el) {
          jQuery(el).css('height', 'auto').css('height', el.scrollHeight + offset);
      };
      jQuery(this).on('keyup input active focus load', function() { resizeTextarea(this); });
      jQuery(this).on('keyup input', function() {
        if($(this).attr('id') === 'main_header'){
          var header = $(this).val();
          if(header === ''){
            document.title = 'Новая статья | Jimbl'
            $('#id_title').val('')
            $('label[for=id_title]').removeClass('active');
          }
          else{
            document.title = header;
            $('#id_title').val(header);
            $('label[for=id_title]').addClass('active');
            $('#preview-title').text(header);
          }
        }
      });
  });
}

function render_post(){
  if($(window).width() < 1200){
    $('#post-mobile').html(sessionStorage.getItem('post'));
  }
  else{
    $('#post').html(sessionStorage.getItem('post'));
  }
  update_inner_post();
}

function clear_post(){
  sessionStorage.removeItem('post');
  render_post();
  get_started();
  $('#clear_post_modal').modal('show')
  return;
}

function save_post(){
  if($(window).width() < 1200){
    sessionStorage.setItem('post', $('#post-mobile').html());
    $('#id_html_data').val($('#post-mobile').html());
  }
  else{
    sessionStorage.setItem('post', $('#post').html());
    $('#id_html_data').val($('#post').html());
  }
  return;
}

render_post();
update_inner_post();
save_post();

$(document).on('change', 'textarea.input', function(){
  element_id = sessionStorage.getItem('focus_element_id');
  var input = $('.element#'+ element_id + ' > td > div > textarea');
  input.text(this.value);
  save_post();
  update_inner_post()
  return false;
});

$(document).on('focus', 'textarea.input', function(){
  var element = $(this);
  var element_id = element.parent().parent().parent().attr('id');
  $('.btn-add').addClass('hidden-add-btn');
  $('#collapse_'+element_id).removeClass('hidden-add-btn');
  sessionStorage.setItem('focus_element_id', element_id);

  $('.mobile-main-tools-bar').addClass('hidden')
  $('#mobile-tools-bar-'+element_id).removeClass('hidden')

  $('.action-bar-toggler').addClass('hidden')
  $('.action-bar-toggler').removeClass('rotate')

  $('.action-bar-wrapper').addClass('hidden')
  $('.action-bar-toggler-'+element_id).removeClass('hidden')

  if(element_id === '1'){
    $('#trash_icon').addClass('text-muted');
    $('[onclick="removeItem()"]').attr('onclick', '');
  }
  else{
    $('#trash_icon').removeClass('text-muted');
    $('[onclick=""]').attr('onclick', 'removeItem()');
  }

  if(element.parent().parent().parent().parent().hasClass('tbody')) {
    $('.table-settings').removeClass('hidden');
  }
  else{
    $('.table-settings').addClass('hidden');
  }

  if(element.hasClass('bold')){
    $('li.set-bold').addClass('active');
  }
  else{
    $('li.set-bold').removeClass('active');
  }

  if(element.hasClass('italic')){
    $('li.set-italic').addClass('active');
  }
  else{
    $('li.set-italic').removeClass('active');
  }

  if(element.hasClass('underline')){
    $('li.set-underline').addClass('active');
  }
  else{
    $('li.set-underline').removeClass('active');
  }

  $("select[id=mobile-tool-height] > option[value='"+parseInt(element.css('font-size'))+"']").prop('selected', true)
  $('select.font-family-select > option[value=' + element.css('font-family') +']').prop('selected', true);
  $("select.font-size-select > option[value='"+parseInt(element.css('font-size'))+"']").prop('selected', true)
  $('select.font-align-select > option[value=' + element.css('text-align') +']').prop('selected', true);
  //$('select.font-size-select > option[value=' + element.css('font-size') * 10 +']').prop('selected', true);
  save_post();
  update_inner_post();
  return false;
});

$('li.set-bold').on('click', function(){
  $('.element#' + sessionStorage.getItem('focus_element_id') + '> td > div > textarea.input').toggleClass('bold');
  $(this).toggleClass('active');
  save_post();
  update_inner_post();
  return false;
});

function set_bold(){
  $('.element#' + sessionStorage.getItem('focus_element_id') + '> td > div > textarea.input').toggleClass('bold');
  $(this).toggleClass('active');
  save_post();
  update_inner_post();
  return false;
}

$('li.set-italic').on('click', function(){
  $('.element#' + sessionStorage.getItem('focus_element_id') + '> td > div > textarea.input').toggleClass('italic');
  $(this).toggleClass('active');
  save_post();
  update_inner_post();
  return false;
});

function set_italic(){
  $('.element#' + sessionStorage.getItem('focus_element_id') + '> td > div > textarea.input').toggleClass('italic');
  $(this).toggleClass('active');
  save_post();
  update_inner_post();
  return false;
}

$('li.set-underline').on('click', function(){
  $('.element#' + sessionStorage.getItem('focus_element_id') + '> td > div > textarea.input').toggleClass('underline');
  $(this).toggleClass('active');
  save_post();
  update_inner_post();
  return false;
});

function set_underline(){
  $('.element#' + sessionStorage.getItem('focus_element_id') + '> td > div > textarea.input').toggleClass('underline');
  $(this).toggleClass('active');
  save_post();
  update_inner_post();
  return false;
}

function set_height(value){
  element_id = sessionStorage.getItem('focus_element_id');
  var input = $('.element#'+ element_id + ' > td > div > textarea');
  input.css('font-size', value + 'px' );
  var offset = input.offsetHeight - input.clientHeight;
  input.css('height', 'auto').css('height', input.scrollHeight + offset);
  save_post();
  update_inner_post();
  return false;
}

function align(value) {
  var element_id = sessionStorage.getItem('focus_element_id');
  var input = $('.element#'+ element_id + ' > td > div > textarea');
  input.removeClass('text-left');
  input.removeClass('text-center');
  input.removeClass('text-right');
  input.addClass('text-' + value);
  update_inner_post();
  save_post()
  return false;
}

function cut_element(){
  element_id = sessionStorage.getItem('focus_element_id');
  var input = $('.element#'+ element_id)
  sessionStorage.setItem('cut_element', "<tr class='element' id=" + element_id + " align='center'>" + input.html() + "</tr>")
  sessionStorage.setItem('cut_element_id', element_id)
  input.remove()
  $('#cut_element_modal').modal('show');
}

function copy_element(){
  element_id = sessionStorage.getItem('focus_element_id');
  var input = $('.element#'+ element_id)

  var action = "$('.action-bar-wrapper-"+ (parseInt(get_max_id()) + 1) +"').toggleClass('hidden');"+"$(this).toggleClass('rotate')";
  var textarea = $('tr.element#'+ element_id + ' > td > div > textarea').parent().children()[1].outerHTML

  if($(window).width() < 1200){
  var new_element = "<tr class='element' id=" + (parseInt(get_max_id()) + 1) + " align='center'>"+"<td class='drag-handler'><div class='input-group'><div class='input-group-prepend'><span class='input-group-text m-0 p-0 bg-white border-0'>"+
  "<div class='dropdown'><a class='btn-add hidden-add-btn' style='position: relative; top: -9px;' href='' id='collapse_" + (parseInt(get_max_id()) + 1) + "' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><span class='adder-elements' style='font-size: 2.5rem; top: 11px;'><span>&#43;</span></span></a></span><div class='dropdown-menu' style='border: none; background: transparent;' aria-labelledby='collapse_" + (parseInt(get_max_id()) + 1) + "'>"+
  $('#mobile_tools').html()+"</div></div></div>"+
  textarea+"</div>"+
  "<div class='action-bar-toggler action-bar-toggler-"+(parseInt(get_max_id()) + 1)+" hidden' onclick="+action+"><img src='/static/App/def_image/down-arrow.svg' height='20px' width='20px'></div>"+
  "<div class='hidden animated fadeIn action-bar-wrapper action-bar-wrapper-"+(parseInt(get_max_id()) + 1)+"'>"+$('#mobile_action_bar').html()+"</div></td></tr>"
  }
  else{
    var new_element = "<tr class='element' id=" + (parseInt(get_max_id()) + 1) + " align='center'>"+"<td class='drag-handler'><div class='input-group'><div class='input-group-prepend'><span class='input-group-text m-0 p-0 bg-white border-0'>"+
    "<div class='dropdown'><a class='btn-add hidden-add-btn' style='position: relative; top: -9px;' href='' id='collapse_" + (parseInt(get_max_id()) + 1) + "' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><span class='adder-elements' style='font-size: 2.5rem;'><span>&#43;</span></span></a></span><div class='dropdown-menu' style='border: none; background: transparent;' aria-labelledby='collapse_" + (parseInt(get_max_id()) + 1) + "'>"+$('#mobile_tools').html()+"</div></div></div>"+
    textarea+"</div></td></tr>"
  }

  sessionStorage.setItem('cut_element', new_element)
  sessionStorage.setItem('cut_element_id', (parseInt(get_max_id()) + 1))
  $('#copy_element_modal').modal('show');
}

function get_max_id(){
  var array = $('tr.element').toArray()
  var cnt = 0
  for (i = 0; i < array.length; i++){
    if(cnt < array[i].id){
      cnt = array[i].id
    }
  }
  return cnt
}

function paste_element(){
  var input = sessionStorage.getItem('cut_element');
  $(input).insertAfter('tr.element#'+sessionStorage.getItem('focus_element_id'));
  $('tr.element#'+sessionStorage.getItem('cut_element_id') + ' > td > div > textarea').focus()
  sessionStorage.removeItem('cut_element')
  sessionStorage.removeItem('cut_element_id')
}

function set_font_family(value){
  element_id = sessionStorage.getItem('focus_element_id');
  var input = $('.element#'+ element_id + ' > td > div > textarea');
  input.css('font-family', value);
  update_inner_post();
  save_post()
  return false;
}

function removeItem() {
  element_id = sessionStorage.getItem('focus_element_id');
  if(element_id === '1') return false;
  var element = document.getElementById(element_id);
  element.parentNode.removeChild(element);
  update_inner_post();
  save_post();
  $('#remove_element_modal').modal('show');
  return false;
}

function add_image(event){

  event.preventDefault();

  if($('.media.select').length === 0 && $('#id_load_image').val() === ''){
    $('#empty_photo_modal').modal('show');
    return false;
  }

  $('.action-bar-wrapper').addClass('hidden')

  if($('.media.select').length === 1){
    var url = $('.media.select').attr('src');
    var element = document.createElement('tr');
    last_element_id = $('tr.element').last().attr('id');
    if(!last_element_id) last_element_id = 0;

    element.className += 'element';
    element.setAttribute('id', parseInt(last_element_id) + 1);
    element.setAttribute('align', 'center');

    element.innerHTML = "<td class='drag-handler' ondrag='console.log(2)'><img src=" + url + " width='100%'' alt='image'></td>";

    if($(window).width() < 1200){
      document.getElementById('post-mobile').appendChild(element);
    }
    else{
      document.getElementById('post').appendChild(element);
    }

    $('#add_image_modal').modal('hide');
    $('#id_load_image').val('');
    $('[id=img-preview]').attr('src', '');
    $('[id=user_media]').removeClass('hidden');
    $('.media').removeClass('select')

    save_post();
    update_inner_post();
    return false;
  }

  var data = new FormData($('#image_form').get(0));

  $.ajax({
      url: '/creator/',
      type: 'POST',
      data: data,
      cache: false,
      processData: false,
      contentType: false,
      success: function(data) {

          var element = document.createElement('tr');
          last_element_id = $('tr.element').last().attr('id');
          if(!last_element_id) last_element_id = 0;

          element.className += 'element';
          element.setAttribute('id', parseInt(last_element_id) + 1);
          element.setAttribute('align', 'center');

          $(element).html(data)

          if($(window).width() < 1200){
            document.getElementById('post-mobile').appendChild(element);
          }
          else{
            document.getElementById('post').appendChild(element);
          }

          $('#add_image_modal').modal('hide');
          $('#id_load_image').val('');
          $('[id=img-preview]').attr('src', '');
          $('[id=user_media]').removeClass('hidden');
          $('.media').removeClass('select')

          save_post();
          update_inner_post();
      }
  });
  return false;
}

function add_video(event){

  if($('#id_html_video').val() === '' && $('#id_load_video').val() === ''){
    $('#empty_video_modal').modal('show');
    return false;
  }

  if($('#id_html_video').val() != ''){
    var element = document.createElement('tr');
    last_element_id = $('tr.element').last().attr('id');
    if(!last_element_id) last_element_id = 0;

    element.className += 'element';
    element.setAttribute('id', parseInt(last_element_id) + 1);
    element.setAttribute('align', 'center');

    element.innerHTML = "<td align='center'>" + $('#id_html_video').val() + "</td>";

    if($(window).width() < 1200){
      document.getElementById('post-mobile').appendChild(element);
    }
    else{
      document.getElementById('post').appendChild(element);
    }

    $('#add_video_modal').modal('hide');

    save_post();
    update_inner_post();
    return false;
  }

  event.preventDefault();

  if($('.media.select').length === 1){
    var url = $('.media.select').attr('src');
    var element = document.createElement('tr');
    last_element_id = $('tr.element').last().attr('id');
    if(!last_element_id) last_element_id = 0;

    element.className += 'element';
    element.setAttribute('id', parseInt(last_element_id) + 1);
    element.setAttribute('align', 'center');

    element.innerHTML = "<td><img src=" + url + " width='100%'' alt='image'></td>";

    if($(window).width() < 1200){
      document.getElementById('post-mobile').appendChild(element);
    }
    else{
      document.getElementById('post').appendChild(element);
    }

    $('#add_image_modal').modal('hide');

    save_post();
    update_inner_post();
    return false;
  }

  var data = new FormData($('#video_form').get(0));

  $.ajax({
      url: '/creator/',
      type: 'POST',
      data: data,
      cache: false,
      processData: false,
      contentType: false,
      success: function(data) {

          var element = document.createElement('tr');
          last_element_id = $('tr.element').last().attr('id');
          if(!last_element_id) last_element_id = 0;

          element.className += 'element';
          element.setAttribute('id', parseInt(last_element_id) + 1);
          element.setAttribute('align', 'center');

          $(element).html(data)

          if($(window).width() < 1200){
            document.getElementById('post-mobile').appendChild(element);
          }
          else{
            document.getElementById('post').appendChild(element);
          }

          $('#add_video_modal').modal('hide');

          save_post();
          update_inner_post();
      }
  });
  return false;
}

function add_main_header(){
  var element = document.createElement('tr');
  last_element_id = $('tr.element').last().attr('id');
  if(!last_element_id) last_element_id = 0;

  element.className += 'element';
  element.setAttribute('id', parseInt(last_element_id) + 1)
  element.setAttribute('align', 'center');

  element.innerHTML = "<td><div><textarea autofocus data-autoresize rows='1' class='input display-4 header text-left form-control' id='main_header' placeholder='Начните с заголовка...'></textarea></div></td>"

  if($(window).width() < 1200){
    document.getElementById('post-mobile').appendChild(element);
  }
  else{
    document.getElementById('post').appendChild(element);
  }
  save_post();
  sort_table();
  update_inner_post();
}

function add_header(text){
  var element = document.createElement('tr');
  last_element_id = $('tr.element').last().attr('id');
  if(!last_element_id) last_element_id = 0;

  element.className += 'element';
  element.setAttribute('id', parseInt(last_element_id) + 1)
  element.setAttribute('align', 'center');

  var action = "$('.action-bar-wrapper-"+ (parseInt(last_element_id) + 1) +"').toggleClass('hidden');"+"$(this).toggleClass('rotate')"

  element.innerHTML = "<td class='drag-handler'><div class='input-group'>"+
  "<textarea spellcheck='false' data-autoresize rows='1' class='input display-4 header pb-0 text-left form-control' style='font-size: 28px;' placeholder='"+text+"'></textarea></div>"+
  "<div class='mobile-main-tools-bar hidden' id='mobile-tools-bar-" + (parseInt(last_element_id) + 1) + "'><span class='input-group-text m-0 p-0 bg-white border-0' style='width: fit-content;'>"+
  "<div class='action-bar-toggler action-bar-toggler-"+(parseInt(last_element_id) + 1)+" hidden' onclick="+action+"><img src='/static/App/def_image/down-arrow.svg' height='20px' width='20px'></div></div>"+
  "<div class='hidden animated fadeIn action-bar-wrapper action-bar-wrapper-"+(parseInt(last_element_id) + 1)+"'>"+$('#mobile_action_bar').html()+"</div></td>"

  if($(window).width() < 1200){
    document.getElementById('post-mobile').appendChild(element);
    $('tr.element#'+(parseInt(last_element_id) + 1)+'> td > div > textarea').focus()
  }
  else{
    document.getElementById('post').appendChild(element);
    $('tr.element#'+(parseInt(last_element_id) + 1)+'> td > div > textarea').focus()
  }
  save_post();
  sort_table();
  update_inner_post();
}

$(document).on('click', function(event){
  if($(window).width() < 1200){
    if($(event.target).hasClass('main-body-post')){
      $('.action-bar-wrapper').addClass('hidden');
      $('.action-bar-toggler').removeClass('rotate');
    }
  }
})

$('.dropdown.mr-4[role=button]').on('click', function(){
  $('.action-bar-wrapper').addClass('hidden');
})

function add_paragraph(text){
  var element = document.createElement('tr');
  last_element_id = $('tr.element').last().attr('id');
  if(!last_element_id) last_element_id = 0;

  element.className += 'element';
  element.setAttribute('id', parseInt(last_element_id) + 1)
  element.setAttribute('align', 'center');

  var action = "$('.action-bar-wrapper-"+ (parseInt(last_element_id) + 1) +"').toggleClass('hidden');"+"$(this).toggleClass('rotate')"

  element.innerHTML = "<td class='drag-handler'><div class='input-group'>"+
  "<textarea spellcheck='false' data-autoresize rows='1' class='input lead paragraph pb-0 text-left form-control' style='font-size: 16px;' placeholder='"+text+"'></textarea></div>"+
  "<div class='mobile-main-tools-bar hidden' id='mobile-tools-bar-" + (parseInt(last_element_id) + 1) + "'><span class='input-group-text m-0 p-0 bg-white border-0' style='width: fit-content;'>"+
  "<div class='action-bar-toggler action-bar-toggler-"+(parseInt(last_element_id) + 1)+" hidden' onclick="+action+"><img src='/static/App/def_image/down-arrow.svg' height='20px' width='20px'></div></div>"+
  "<div class='hidden animated fadeIn action-bar-wrapper action-bar-wrapper-"+(parseInt(last_element_id) + 1)+"'>"+$('#mobile_action_bar').html()+"</div></td>"

  if($(window).width() < 1200){
    document.getElementById('post-mobile').appendChild(element);
    $('tr.element#'+(parseInt(last_element_id) + 1)+'> td > div > textarea').focus()
  }
  else{
    document.getElementById('post').appendChild(element);
    $('tr.element#'+(parseInt(last_element_id) + 1)+'> td > div > textarea').focus()
  }
  save_post();
  sort_table();
  update_inner_post();
}

$('.timeout').on('show.bs.modal', function (event) {
  setTimeout(function(){
    $('.modal-backdrop.show').last().css('opacity', '0');
    $('body').removeClass('modal-open')
    $('body').css('padding-right', '0')
    $('.navbar').css('padding-right', '0')
  }, 10)
})

$('.modal.timeout').on('show.bs.modal', function (event) {
  setTimeout(function(){$('.modal.timeout').modal('hide')}, 1500)
})

function sort_table(){
  Sortable.create(
      $('#post')[0], {
          animation: 150,
          scroll: true,
          handle: '.drag-handler',
      }
  );
  Sortable.create(
      $('#post-mobile')[0], {
          animation: 150,
          scroll: true,
          handle: '.drag-handler',
      }
  );
}

function add_hr(){
  var element = document.createElement('tr');

  element.setAttribute('align', 'center');

  element.innerHTML = "<td class='drag-handler'><div><hr></div></td>";

  if($(window).width() < 1200){
    document.getElementById('post-mobile').appendChild(element);
  }
  else{
    document.getElementById('post').appendChild(element);
  }
  save_post();
  sort_table();
  update_inner_post();
}
