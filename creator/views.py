from django.shortcuts import render
from .models import Image_upload, Video_upload
from App.models import Post
from .forms import Image_Upload_Form, Video_Upload_Form
from django.http import HttpResponse
from django.template import loader
from django.contrib.auth.decorators import login_required

@login_required(login_url='/accounts/login/')
def post_edit(request, post_id):
    if request.is_ajax():
        if request.POST.get('save_post'):
            post = Post.objects.get(id = request.POST.get('post_id'))

            new_html = request.POST.get('post_new_html')

            post.html_data = new_html
            post.save()

    post = Post.objects.filter(id = post_id).get()
    context = {
        'post': post,
    }
    return render(request, 'edit_post/index.html', context)

@login_required(login_url='/accounts/login/')
def index(request):
    user = request.user

    if request.is_ajax():
        if (request.POST.get('edit_post_status') == "1"):
            edit_post = Post.objects.filter(id = request.POST.get('id_post')).get()
            html_data = request.POST.get('html_data')
            edit_post.html_data = html_data
            edit_post.save()
            context = {
                'post': edit_post,
            }
            return render(request, 'edit_post/index.html', context)
        try:
            image_file = request.FILES['load_image']
        except Exception:
            image_file = False

        try:
            video_file = request.FILES['load_video']
        except Exception:
            video_file = False

        if(image_file):
            image = Image_upload.objects.create(user = request.user, image = image_file)

            image.save()

            template = loader.get_template('creator/include/image.html')
            media_list = Image_upload.objects.filter(user = user).all()

            context = {
                "image_url": image.image.url,
                "media_list": media_list,
            }

            resp = template.render(context, request)
            image_file = False
            return HttpResponse(resp)
        elif(video_file):
            video = Video_upload.objects.create(user = request.user, video = video_file)

            video.save()

            template = loader.get_template('creator/include/video.html')
            video_media_list = Video_upload.objects.filter(user = user).all()

            context = {
                "video_url": video.video.url,
                "media_list": video_media_list,
            }

            resp = template.render(context, request)
            video_file = False
            return HttpResponse(resp)

    media_list = Image_upload.objects.filter(user = user).all()
    context = {
        "media_list": media_list,
    }

    return render(request, 'creator/index.html', context)
