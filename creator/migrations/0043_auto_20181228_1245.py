# Generated by Django 2.1 on 2018-12-28 09:45

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('creator', '0042_auto_20181228_1244'),
    ]

    operations = [
        migrations.AlterField(
            model_name='image_upload',
            name='date',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2018, 12, 28, 9, 45, 21, 618866, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='video_upload',
            name='date',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2018, 12, 28, 9, 45, 21, 624533, tzinfo=utc)),
        ),
    ]
