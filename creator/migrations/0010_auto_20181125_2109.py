# Generated by Django 2.1 on 2018-11-25 18:09

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('creator', '0009_auto_20181124_1758'),
    ]

    operations = [
        migrations.AlterField(
            model_name='image_upload',
            name='date',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2018, 11, 25, 18, 9, 7, 527275, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='video_upload',
            name='date',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2018, 11, 25, 18, 9, 7, 530288, tzinfo=utc)),
        ),
    ]
