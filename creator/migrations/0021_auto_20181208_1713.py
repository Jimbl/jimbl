# Generated by Django 2.1 on 2018-12-08 14:13

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('creator', '0020_auto_20181208_0742'),
    ]

    operations = [
        migrations.AlterField(
            model_name='image_upload',
            name='date',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2018, 12, 8, 14, 13, 13, 863251, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='video_upload',
            name='date',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2018, 12, 8, 14, 13, 13, 865253, tzinfo=utc)),
        ),
    ]
