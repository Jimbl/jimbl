from django import forms
from .models import Image_upload, Video_upload

class Image_Upload_Form(forms.ModelForm):
    class Meta:
        model = Image_upload
        fields = ('user', 'image')

class Video_Upload_Form(forms.ModelForm):
    class Meta:
        model = Video_upload
        fields = ('user', 'video')
