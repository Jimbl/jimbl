from django.db import models
from django.contrib.auth.models import User
from App.models import Post, Comment

import PIL
from PIL import Image
from imagekit.models.fields import ImageSpecField
from imagekit.processors import ResizeToFit, Adjust, ResizeToFill
from django.utils import timezone


def upload_to(instance, filename):
    return 'App/static/uploads/%s/media/%s' % (instance.user.username, filename)

class Image_upload(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, null=True, blank='true')
    image = models.FileField(upload_to=upload_to, blank=True)
    image_media = ImageSpecField([Adjust(contrast=1.2, sharpness=1.1),
                            ResizeToFill(400, 300)], source='image',
                            format='JPEG', options={'quality': 90})
    image_preview = ImageSpecField([Adjust(contrast=1.2, sharpness=1.1),
                            ResizeToFill(650, 450)], source='image',
                            format='JPEG', options={'quality': 90})
    date =  models.DateTimeField(default=timezone.now(),blank=True)
    likes = models.ManyToManyField(User, blank=True, related_name='media_likes')
    comments = models.ManyToManyField(Comment, blank=True, related_name='media_comment')
    share = models.ManyToManyField(User, blank=True, related_name='media_share')

class Video_upload(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, null=True, blank='true')
    video = models.FileField(upload_to=upload_to, blank=True)
    date =  models.DateTimeField(default=timezone.now(),blank=True)
    likes = models.ManyToManyField(User, blank=True, related_name='media_video_likes')
    comments = models.ManyToManyField(Comment, blank=True, related_name='media_video_comment')
    share = models.ManyToManyField(User, blank=True, related_name='media_video_share')
