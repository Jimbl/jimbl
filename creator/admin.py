from django.contrib import admin
from .models import Image_upload, Video_upload
# Register your models here.

admin.site.register(Image_upload)
admin.site.register(Video_upload)
