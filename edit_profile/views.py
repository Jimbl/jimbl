from django.shortcuts import render, redirect
from django.http import HttpResponse
from . import forms
from App.models import Profile
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth import update_session_auth_hash
from django.template import loader
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import make_password, check_password
from django.contrib.auth.forms import PasswordChangeForm
from django.core.mail import EmailMultiAlternatives
from django.contrib.auth.hashers import make_password
import requests
import json
from urllib import request as r
from urllib import parse
import ast
import string
import tempfile
from django.core import files

def send_mail(subject, from_email, to, email_confirm_key):
    text_content = 'Добро пожаловать в Jimbl!Мы очень рады, что вы присоединились к нашей дружной и большой компании блогеров. Вместе мы совершим революцию в мире блоггинга! Но для начала введите код, расположенный снизу, для подтверждении email.'
    html_content = """
    <div>
        <h1>Добро пожаловать в Jimbl!</h1>
    </div>
    <div>
        <p>
            Мы очень рады, что вы присоединились к нашей дружной и большой компании блогеров.<br>
            Вместе мы совершим революцию в мире блоггинга!<br>
            Но для начала введите код, расположенный снизу, для подтверждении email.
        </p>
        <h1>""" + str(email_confirm_key) + """</h1>
        <p>Если вы не указывали email <b>""" + to + """</b>, то просто проигнорируйте это письмо.</p>
        <p>С уважением,<br>
        Команда Jimbl.</p>
    </div>
    """
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()

@login_required(login_url='/accounts/login/')
def change_mail(request, username):
    if username != request.user.username:
        return render(request, 'errors/404.html')

    if request.is_ajax():
        if request.POST.get('confirm_code') == '1':
            user_object = request.user
            user_email_key = request.POST.get('code')
            email_confirm_key = request.user.profile.email_confirm_key
            if str(email_confirm_key) == str(user_email_key):
                user_object.profile.email_confirm_status = True
                user_object.profile.email = request.POST.get('user_email')
                user_object.email = request.POST.get('user_email')

                user_object.save()
                return HttpResponse('')
            else:
                template = loader.get_template('profiles/include/input-code.html')
                context = {
                    'error': 'Код введён не верно!',
                }
                resp = template.render(context, request)
                return HttpResponse(resp)

        if request.POST.get('send_code_cm') == '1':
            user = request.user

            email = request.POST.get('email')
            password = request.POST.get('password')
            email_confirm_key = user.profile.email_confirm_key

            if user.check_password(password):
                if( Profile.objects.filter(email = email).all().count() == 0 ):
                    try:
                        send_mail('Подтверждение Email', 'Jimbl', email, email_confirm_key)
                    except Exception:
                        template = loader.get_template('profiles/include/change-mail-inputs.html')
                        context = {
                            'error_mail': 'Не удалось отправить код!',
                        }
                        resp = template.render(context, request)
                        return HttpResponse(resp)

                    template = loader.get_template('profiles/include/change-mail-inputs.html')
                    context = {
                        'error_mail': '',
                        'error_password': '',
                    }
                    resp = template.render(context, request)
                    return HttpResponse(resp)
                else:
                    template = loader.get_template('profiles/include/change-mail-inputs.html')
                    context = {
                        'error_mail': 'Такой Email уже используется!',
                    }
                    resp = template.render(context, request)
                    return HttpResponse(resp)
            else:
                template = loader.get_template('profiles/include/change-mail-inputs.html')
                context = {
                    'error_password': 'Неправильный пароль!',
                }
                resp = template.render(context, request)
                return HttpResponse(resp)

    return render(request, 'profiles/include/email-change.html')


@login_required(login_url='/accounts/login/')
def email(request, username):
    if username != request.user.username:
        return render(request, 'errors/404.html')

    if request.is_ajax():
        if request.POST.get('send_code') == '1':
            user = request.user
            email = request.POST.get('email')
            email_confirm_key = user.profile.email_confirm_key
            if( Profile.objects.filter(email = email).all().count() == 0 ):
                send_mail('Подтверждение Email', 'Jimbl', request.POST.get('email'), email_confirm_key)
                template = loader.get_template('profiles/include/input-mail.html')
                context = {
                    'error': '',
                }
                resp = template.render(context, request)
                return HttpResponse(resp)
            else:
                template = loader.get_template('profiles/include/input-mail.html')
                context = {
                    'error': 'Такой Email уже используется!',
                }
                resp = template.render(context, request)
                return HttpResponse(resp)

        if request.POST.get('confirm_code') == '1':
            user_object = request.user
            user_email_key = request.POST.get('code')
            email_confirm_key = request.user.profile.email_confirm_key
            if str(email_confirm_key) == str(user_email_key):
                user_object.profile.email_confirm_status = True
                user_object.profile.email = request.POST.get('user_email')
                user_object.email = request.POST.get('user_email')

                user_object.marker.add_email = True
                user_object.marker.save()

                user_object.save()
                return HttpResponse('')
            else:
                template = loader.get_template('profiles/include/input-code.html')
                context = {
                    'error': 'Код введён не верно!',
                }
                resp = template.render(context, request)
                return HttpResponse(resp)

    return render(request, 'profiles/include/email.html')

@login_required(login_url='/accounts/login/')
def privacy(request, username):
    if username != request.user.username:
        return render(request, 'errors/404.html')

    return render(request, 'profiles/include/privacy.html')

@login_required(login_url='/accounts/login/')
def social_networks(request, username):
    if request.is_ajax():
        if(request.POST.get('vk_clear') == '1'):
            user = request.user
            user.profile.vk_id = ''
            user.profile.vk_firstname = ''
            user.profile.vk_lastname = ''
            user.profile.vk_image = ''
            user.save()
            return redirect('/'+request.user.username+'/edit/social-networks')

    if request.method == 'GET':
        if request.GET.get('code'):
            code = request.GET.get('code')
            data_dict = {
                'client_id': '6789266',
                'client_secret':'QQZeKIBWXUdt1dw3th4i',
                'redirect_uri':'https://jimbl.ru/'+request.user.username+'/edit/social-networks',
                'code':code
            }
            r = requests.get("https://oauth.vk.com/access_token",  params=data_dict) #send request
            url = r.url #get response
            tmp = r.content.decode('utf-8')
            resp = ast.literal_eval(tmp)
            user_id = resp.get('user_id')
            access_token = resp.get('access_token')

            if Profile.objects.filter(vk_id = user_id).all().count() == 0:
                user = request.user

                data_dict_2 = {
                    'user_ids': str(user_id),
                    'fields': 'photo_400_orig',
                    'access_token': access_token,
                    'v': '5.92'
                }

                r_2 = requests.get("https://api.vk.com/method/users.get",  params=data_dict_2)
                tmp_2 = r_2.content.decode('utf-8')
                resp_2 = json.loads(tmp_2)
                resp_2_list = resp_2.get('response')

                name = resp_2_list[0].get('first_name')
                last_name = resp_2_list[0].get('last_name')
                photo = resp_2_list[0].get('photo_400_orig')

                request_photo = requests.get(photo, stream=True)

                file_name = photo.split('/')[-1]
                lf = tempfile.NamedTemporaryFile()

                for block in request_photo.iter_content(1024 * 8):
                     if not block:
                        break
                     lf.write(block)

                user.profile.vk_id = user_id
                user.profile.vk_firstname = name
                user.profile.vk_lastname = last_name
                user.profile.vk_image.save(file_name, files.File(lf))
                user.profile.save()
                return redirect('/'+request.user.username+'/edit/social-networks')
            else:
                return False

    if username != request.user.username:
        return render(request, 'errors/404.html')

    return render(request, 'profiles/include/social_networks.html')

@login_required(login_url='/accounts/login/')
def change_password(request, username):
    if username != request.user.username:
        return render(request, 'errors/404.html')

    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            user_new_logged = authenticate(username = str(request.user.username), password = form.cleaned_data['new_password1'])
            login(request, user_new_logged)
            return redirect('/' + auth.get_user(request).username + '/edit/change-password')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'profiles/include/change_password.html', {
        'form': form
    })

def send_del_user(subject, from_email, to, username, email, text):
    text_content = ''
    html_content = """
    <div>
        <h1>"""+username+""" удалил аккаунт!</h1>
    </div>
    <div>
        <h1>Email: """ + email + """</h1>
        <h1>Причина: </h1><p>""" + text + """</p>
    </div>
    """
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()

@login_required(login_url='/accounts/login/')
def update_profile(request, username):
    if username != request.user.username:
        return render(request, 'errors/404.html')

    if request.is_ajax():
        if(request.POST.get('delete_account') == '1'):
            user = User.objects.get(username = username)
            send_del_user('Удален аккаунт!', 'Jimbl', 'jimbl.service@gmail.com', request.user.username, request.user.profile.email, request.POST.get('reason_delete'))
            user.delete()
            logout(request)
            return redirect('/accounts/login/')

        user = request.user

        if(int(request.POST.get('window_width')) < 1200):
            try:
                image_profile = request.FILES['image_profile_mobile']
            except Exception:
                image_profile = False
        else:
            try:
                image_profile = request.FILES['image_profile']
            except Exception:
                image_profile = False

        if(int(request.POST.get('window_width')) < 1200):
            try:
                image_header = request.FILES['image_header_mobile']
            except Exception:
                image_header = False
        else:
            try:
                image_header = request.FILES['image_header']
            except Exception:
                image_header = False

        if(image_profile):
           print(3)
           user.profile.image_profile = image_profile
           user.save()

        if(image_header):
            user.profile.image_header = image_header
            user.save()

        user_form = forms.UserForm(request.POST, instance=request.user)
        profile_form = forms.ProfileForm(request.POST, request.FILES, instance=request.user.profile)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request, 'Ваш профиль был успешно обновлен!')

            user = request.user
            if(not user.marker.add_name and user.get_full_name != ''):
                user.marker.add_name = True
                user.marker.save()

            if(not user.marker.add_image and (user.profile.image_header != '' or user.profile.image_profile != '')):
                user.marker.add_image = True
                user.marker.save()

            if(not user.marker.add_discription and user.profile.discription_profile != ''):
                user.marker.add_discription = True
                user.marker.save()

            return HttpResponse(request, 'profiles/profile.html')
        else:
            messages.error(request, 'ошибка: ' + str(profile_form.errors) + str(user_form.errors))

        user.save()

        return HttpResponse('OK')
    else:
        user_form = forms.UserForm(instance=request.user)
        profile_form = forms.ProfileForm(instance=request.user.profile)
    return render(request, 'profiles/profile.html', {
        'user_form': user_form,
        'profile_form': profile_form,
    })
