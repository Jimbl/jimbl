from django import forms
from django.contrib.auth.models import User
from App.models import Profile

class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'username')

class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('phone', 'email', 'discription_profile', 'hide_list_readers', 'hide_list_read', 'hide_date_of_registration', 'accept_comment_posts', 'accept_send_messages', 'status_profile', 'accept_send_notifications', 'accept_send_news_of_update', 'instagram', 'vk', 'facebook', 'twitter', 'site', 'image_profile', 'image_header')
