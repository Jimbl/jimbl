$.ajaxSetup({
 beforeSend: function(xhr, settings) {
     function getCookie(name) {
         var cookieValue = null;
         if (document.cookie && document.cookie != '') {
             var cookies = document.cookie.split(';');
             for (var i = 0; i < cookies.length; i++) {
                 var cookie = jQuery.trim(cookies[i]);
                 // Does this cookie string begin with the name we want?
             if (cookie.substring(0, name.length + 1) == (name + '=')) {
                 cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                 break;
             }
         }
     }
     return cookieValue;
     }
     if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
         // Only send the token to relative URLs i.e. locally.
         xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
     }
 }
});

$('#search-desktop-input').on("change paste keyup focus", function() {
  if(!$('#search-desktop-list').hasClass('show')){
    $('#search-desktop-list').collapse('show');
  }
  search = $(this).val();
  $.ajax({
     url : '/',
     type : 'POST',
     data : {
       is_search: '1',
       search: search,
     },
     success : function (response) {
      $('#search-desktop-list').html(response)
     },
 });
});

function vk_clear(){
  $.ajax({
    url: '/' + sessionStorage.getItem('logged_user') + '/edit/social-networks',
    type: 'post',
    data: {
      vk_clear: '1'
    }
  })
}

function send_ver_code_cm(){
  var code = $('#ver_code').val();
  if(code === ''){return false}

  $('.load-wrapp').removeClass('hidden')
  $.ajax({
    url: '/' + sessionStorage.getItem('logged_user') + '/edit/email/change',
    type: 'post',
    data:{
      confirm_code: '1',
      code: code,
      user_email: sessionStorage.getItem('tmp_email')
    },
    success: function(response){
      $('#confirm-code-input').html(response)
      $('.load-wrapp').addClass('hidden')
      if($('.invalid-feedback').length == 0){
        document.location.location('../')
      }
    }
  })
}

function send_code_cm(){
  var email = $('#user_email').val();
  var password = $('#password').val();

  $('.load-wrapp').removeClass('hidden')
  $.ajax({
    url: '/' + sessionStorage.getItem('logged_user') + '/edit/email/change',
    type: 'post',
    data:{
      send_code_cm: '1',
      email: email,
      password: password,
    },
    success: function(response){
      $('[id=main-inputs]').html(response)
      $('.load-wrapp').addClass('hidden')
      $('#user_email').val(email)
      if($('.invalid-feedback').length == 0){
        $('#confirm-mail-code').removeClass('hidden');
        sessionStorage.setItem('tmp_email', email)
      }
    }
  })
}

function send_code(){
  var email = $('#user_email').val();
  if(email === ''){return false}
  $('.load-wrapp').removeClass('hidden')
  $.ajax({
    url: '/' + sessionStorage.getItem('logged_user') + '/edit/email',
    type: 'post',
    data:{
      send_code: '1',
      email: email,
    },
    success: function(response){
      $('#confirm-mail-input').html(response)
      $('#user_email').val(email)
      $('.load-wrapp').addClass('hidden')
      if($('.invalid-feedback').length == 0){
        $('#confirm-mail-code').removeClass('hidden');
        sessionStorage.setItem('tmp_email', email)
      }
    }
  })
}

function send_verify_code(){
  var code = $('#ver_code').val();
  if(code === ''){return false}
  $('.load-wrapp').removeClass('hidden')
  $.ajax({
    url: '/' + sessionStorage.getItem('logged_user') + '/edit/email',
    type: 'post',
    data:{
      confirm_code: '1',
      code: code,
      user_email: sessionStorage.getItem('tmp_email')
    },
    success: function(response){
      $('#confirm-code-input').html(response)
      $('.load-wrapp').addClass('hidden')
      if($('.invalid-feedback').length == 0){
        document.location.reload()
      }
    }
  })
}

function Ajaxrequest(formMain) {
   jQuery.ajax({
           url:     "/{{user.username}}/edit",
           type:     "POST",
           dataType: "html",
           data: jQuery("#"+formMain).serialize(),
           success: function(response) {
             $('#success').modal("show");
       },
       error: function(response) {
           alert("2");
       }
    });
}

$(document).ready(function(){
  if($(window).width() < 1200){
    $('#id_accept_send_notifications').first().parent().html('') //delete desktop form
  }
  $('[name=window_width]').val($(window).width());
})
$('input[name=window_width]').val($(window).width());
$('.timeout').on('show.bs.modal', function (event) {
  setTimeout(function(){
    $('.modal-backdrop.show').css('opacity', '0');
    $('body').removeClass('modal-open')
    $('body').css('padding-right', '0')
    $('.navbar').css('padding-right', '0')
  }, 10)
})

$('.modal.timeout').on('show.bs.modal', function (event) {
  setTimeout(function(){$('.modal.timeout').modal('hide')}, 1500)
})

$('#id_reason_delete').keyup(function(){
  $('#reason_delete_cnt').text($('#id_reason_delete').val().length)
});

$('div.col-9').css('min-height', $(window).height() - 238);

function delete_account(username){
  jQuery.ajax({
      url:     "/" + username +"/edit",
      type:     "POST",
      data: {
        delete_account: 1,
        reason_delete: $('#id_reason_delete').val(),
      },
      success: function(response) {
        document.location.href="/accounts/login";
        sessionStorage.setItem('logged_user', '');
      },
      error: function(response) {
          alert("2");
      }
   });
}

function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('[id=image_profile_preview]').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

function readURLHeader(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('[id=image_header_preview]').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#id_image_profile_mobile").change(function() {
  readURL(this);
});

$("#id_image_profile").change(function() {
  readURL(this);
});

$("#id_image_header").change(function() {
  readURLHeader(this);
});

$("#id_image_header_mobile").change(function() {
  readURLHeader(this);
});

function save_profile(event, username){
  event.preventDefault();

  if($(window).width() < 1200){
    var data_img = new FormData($('#form-edit-mobile').get(0));
  }
  else{
    var data_img = new FormData($('#form-edit').get(0));
  }

  jQuery.ajax({
      url:     "/" + username +"/edit",
      type:     "POST",
      data: data_img,
      cache: false,
      processData: false,
      contentType: false,
      success: function(response) {
        $('#success').modal("show");
      },
      error: function(response) {
          alert("2");
      }
   });
}
