"""Jimbl URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from App import views
from edit_profile import views as edit_profile_views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('logout/', views.logout_view, name='logout'),
    url('^', include('django.contrib.auth.urls')),
    path('admin/', admin.site.urls),
    path('API/', include('API.urls')),
    path('search/', include('search.urls')),
    #path('admin/', include('admin.urls')),
    path('', include('App.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('creator/edit-post/', include('creator.urls'), name="edit-post"),
    path('confirm-email/', include('confirm_email.urls'), name='submit_email'),
    url(r'^(?P<username>\w+)/$', views.profile_view, name='profile_view'),
    #url(r'^(?P<username>\w+)/media$', views.media_profile, name='media_profile'),
    url(r'^(?P<username>\w+)/blogs$', views.blogs, name='blogs'),
    url(r'^(?P<username>\w+)/blogs/(?P<id>\d+)$', views.blogs_view, name='blogs'),
    url(r'^(?P<username>\w+)/edit$', include('edit_profile.urls'), name='edit_profile_view'),
    url(r'^(?P<username>\w+)/edit/change-password$', edit_profile_views.change_password, name='change_password'),
    url(r'^(?P<username>\w+)/edit/social-networks$', edit_profile_views.social_networks, name='social_networks'),
    url(r'^(?P<username>\w+)/edit/privacy$', edit_profile_views.privacy, name='privacy'),
    url(r'^(?P<username>\w+)/edit/email$', edit_profile_views.email, name='email'),
    url(r'^(?P<username>\w+)/edit/email/change$', edit_profile_views.change_mail, name='change_email'),
    url(r'^(?P<username>\w+)/post/(?P<id>\d+)$', views.post_view, name='post_view'),
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
